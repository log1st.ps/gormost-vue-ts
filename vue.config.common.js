module.exports = {
    css: {
        loaderOptions: {
            css: {
                localIdentName: '[local]__[hash:6]'
            }
        }
    },
    chainWebpack: (config) => {
        config
            .module
            .rule("svg")
            .exclude
            .add(/\.inline/);

        config
            .module
            .rule("inline-svg")
            .test(/\.inline\.svg/)
            .use("svg-to-vue-loader")
            .loader("svg-to-vue-component/loader")
            .end()
            .use("svg-loader")
            .before("svg-to-vue-loader")
            .loader("vue-loader")
            .end();
    },
}