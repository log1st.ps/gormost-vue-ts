// @ts-ignore-file

import {VueComponent} from 'vue-tsx-helper';
import {TransitionGroupProps, TransitionProps} from 'vue-tsx-support/types/builtin-components';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import {Component, Prop} from 'vue-property-decorator';

@Component
class TransitionComponent extends VueComponent<TransitionProps | TransitionGroupProps> {

    @Prop()
    public name?: string;
    @Prop()
    public appear?: boolean;
    @Prop()
    public css?: boolean;
    @Prop()
    public type?: string;
    @Prop()
    public enterClass?: string;
    @Prop()
    public leaveClass?: string;
    @Prop()
    public enterActiveClass?: string;
    @Prop()
    public leaveActiveClass?: string;
    @Prop()
    public appearClass?: string;
    @Prop()
    public appearActiveClass?: string;

    @Prop()
    public mode?: string;
    @Prop()
    public tag?: string;
    @Prop()
    public moveClass?: string;

    public render() {
        const Tag: any = (this.tag || this.moveClass) ? 'transition-group' : 'transition';
        return (
            <Tag
                {...{
                    attrs: {
                        ...this.$props,
                    },
                    on: {
                        ...this.$listeners,
                    },
                }}
            >
                {renderSlot.bind(this)('default')}
            </Tag>
        );
    }
}

export default TransitionComponent;
