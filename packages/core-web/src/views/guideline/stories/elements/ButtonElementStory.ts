import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import {availableButtonSizesList, BUTTON_SIZE_XS} from '@cip/core/src/constants/button/buttonSizesConstants';
import {IStoryComponent, IComponentParam} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import {availableButtonStatesList, BUTTON_STATE_PRIMARY} from '@cip/core/src/constants/button/buttonStatesConstants';
import {availableExtendedIconTypesList} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import {availableButtonTypesList, BUTTON_TYPE_BUTTON} from '@cip/core/src/constants/button/buttonTypesConstants';

export default {
    component: ButtonElement,
    params: {
        text: 'Some text',
        type: {
            type: 'list',
            value: BUTTON_TYPE_BUTTON,
            options: {
                options: availableButtonTypesList,
            },
        },
        size: {
            type: 'list',
            value: BUTTON_SIZE_XS,
            options: {
                options: availableButtonSizesList,
            },
        },
        state: {
            type: 'list',
            value: BUTTON_STATE_PRIMARY,
            options: {
                options: availableButtonStatesList,
            },
        },
        isDisabled: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isBlock: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isRounded: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isSquare: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isLoading: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        loadingProgress: {
            type: 'number',
            value: 0,
        },
        ...['leftIcon', 'rightIcon'].reduce((currentValue, item) => ({
            ...currentValue,
            [item]: {
                type: 'list',
                value: null,
                options: {
                    options: availableExtendedIconTypesList,
                    allowEmpty: true,
                },
            } as IComponentParam,
        }), {}),
        withRouterClasses: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        routerClasses: {
            type: 'object',
            value: {
                active: 'activeClass',
                exactActive: 'exactActiveClass',
            },
        },
    },
    events: [
        'click',
    ],
} as IStoryComponent;
