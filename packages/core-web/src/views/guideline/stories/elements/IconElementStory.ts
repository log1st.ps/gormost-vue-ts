import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {
    availableExtendedIconTypesList,
    ICON_TYPE_MAGINIFIER,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';

export default {
    component: IconElement,
    params: {
        type: {
            type: 'list',
            value: ICON_TYPE_MAGINIFIER,
            options: {
                options: availableExtendedIconTypesList,
            },
        },
        height: {
            type: 'number',
            value: 20,
        },
        width: {
            type: 'number',
            value: 20,
        },
    },
} as IStoryComponent;
