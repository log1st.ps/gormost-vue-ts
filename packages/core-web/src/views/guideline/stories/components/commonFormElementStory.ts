import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';

export default {
    model: {
        prop: 'value',
        event: 'input',
    },
    params: {
        placeholder: 'Some label',
        isDisabled: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isReadonly: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isValid: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isInvalid: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isClearable: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        clearValue: 'emptyValue',
        hasAppearance: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        before: '',
        after: '',
    },
    events: [
        'input',
    ],
} as {
    params?: IStoryComponent['params'],
    events?: IStoryComponent['events'],
    model?: {
        prop: string,
        event: string,
    },
};
