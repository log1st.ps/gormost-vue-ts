import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import TabPanelComponent from '@cip/gormost-core-web/src/components/tabs/TabPanelComponent';

export default {
    component: TabPanelComponent,
    defaultSlot: 'Something in default slot',
    params: {
        name: 'some name',
    },
} as IStoryComponent;
