import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import TabComponent from '@cip/gormost-core-web/src/components/tabs/TabComponent';
import {
    availableExtendedIconTypesList,
    ICON_TYPE_CLOSE,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import {availableTabTypesList, TAB_TYPE_DEFAULT} from '@cip/core/src/constants/tabs/tabTypesConstants';

export default {
    component: TabComponent,
    params: {
        name: 'one',
        icon: {
            type: 'list',
            value: ICON_TYPE_CLOSE,
            options: {
                options: availableExtendedIconTypesList,
            },
        },
        type: {
            type: 'list',
            value: TAB_TYPE_DEFAULT,
            options: {
                options: availableTabTypesList,
            },
        },
        label: 'Some label',
        isDisabled: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
    },
} as IStoryComponent;
