import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import TabPanelsComponent from '@cip/gormost-core-web/src/components/tabs/TabPanelsComponent';
import {VueComponent} from 'vue-tsx-helper';
import {Component, Emit, Prop} from 'vue-property-decorator';

@Component
class TabPanelsStoryRenderTab extends VueComponent<{name: string, text: string}> {
    @Prop()
    public name: any;

    @Prop()
    public text: any;

    public render() {
        return (
            <div
                style={{
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    right: 0,
                    bottom: 0,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                {this.text}
            </div>
        );
    }
}

// tslint:disable-next-line:max-classes-per-file
@Component
class TabPanelsStoryRender extends VueComponent<{}> {
    @Prop()
    public isSwipeable: any;
    @Prop()
    public value: any;

    public render() {
        return (
            <TabPanelsComponent
                style={{
                    position: 'relative',
                    height: '100%',
                }}
                isSwipeable={this.isSwipeable}
                onShowNext={this.onShowNext}
                onShowPrevious={this.onShowPrevious}
            >
                <TabPanelsStoryRenderTab
                    key={this.value}
                    name={this.value}
                    text={`Slide ${this.value}`}
                />
            </TabPanelsComponent>
        );
    }

    @Emit('showNext')
    protected onShowNext() {
        // something
    }

    @Emit('showPrevious')
    protected onShowPrevious() {
        // something
    }
}

const showSlide = (story: IStoryComponent, direction: 'left' | 'right') => {
    if (!story.params) {
        return;
    }
    if (
        typeof story.params.value === 'object'
        && story.params.value.options
        && 'options' in story.params.value.options
    ) {
        const slides = story.params.value.options.options;
        const oldIndex = slides.indexOf(story.params.value.value);
        const newIndex = oldIndex + (direction === 'left' ? -1 : 1);

        story.params.value.value = slides[
            newIndex < 0 ? slides.length - 1 : (newIndex > slides.length - 1 ? 0 : newIndex)
        ];
    }
};

export default {
    component: TabPanelsStoryRender,
    params: {
        isSwipeable: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        value: {
            type: 'list',
            value: 'one',
            options: {
                options: ['one', 'two'],
            },
        },
    },
    events: ['input', 'showPrevious', 'showNext'],
    handlers: {
        showPrevious: (story) => {
            showSlide(story, 'left');
        },
        showNext: (story) => {
            showSlide(story, 'right');
        },
    },
} as IStoryComponent;
