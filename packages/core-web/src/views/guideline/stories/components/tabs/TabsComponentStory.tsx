import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import TabsComponent from '@cip/gormost-core-web/src/components/tabs/TabsComponent';
import {VueComponent} from 'vue-tsx-helper';
import {Component, Emit, Prop} from 'vue-property-decorator';

@Component
class TabsStoryRenderTab extends VueComponent<{name: string, type: any}> {
    @Prop()
    public name: any;
    @Prop()
    public type: any;

    protected isActive = false;

    public setIsActive(value) {
        this.isActive = value;
    }

    public render() {
        return (
            <div
                onClick={this.onInput}
                style={{
                    marginRight: '10px',
                    fontWeight: this.isActive ? 'bold' : '400',
                    cursor: 'pointer',
                }}
            >
                {this.name}
            </div>
        );
    }

    @Emit('click')
    protected onInput() {
        return this.name;
    }
}

// tslint:disable-next-line:max-classes-per-file
@Component
class TabsStoryRender extends VueComponent<{}> {
    @Prop()
    public value: any;

    public render() {
        return (
            <TabsComponent value={this.value} onInput={this.onInput}>
                <TabsStoryRenderTab name={'one'} type={'default'}/>
                <TabsStoryRenderTab name={'two'} type={'default'}/>
                <TabsStoryRenderTab name={'three'} type={'default'}/>
                <TabsStoryRenderTab name={'four'} type={'default'}/>
            </TabsComponent>
        );
    }

    @Emit('input')
    protected onInput(value) {
        return value;
    }
}

export default {
    component: TabsStoryRender,
    model: {
        prop: 'value',
        event: 'input',
    },
    params: {
        value: {
            type: 'list',
            value: 'one',
            options: {
                options: ['one', 'two', 'three'],
            },
        },
    },
    events: ['input'],
} as IStoryComponent;
