import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import DialogComponent from '@cip/gormost-core-web/src/components/dialog/DialogComponent';
import {BUTTON_STATE_PRIMARY, BUTTON_STATE_SECONDARY} from '@cip/core/src/constants/button/buttonStatesConstants';
import {BUTTON_SIZE_SM} from '@cip/core/src/constants/button/buttonSizesConstants';
import {ACTIONS_LIST_ALIGN_END} from '@cip/core/src/constants/actionsList/actionsListAlignsConstants';

export default {
    component: DialogComponent,
    defaultSlot: 'Some dialog content',
    params: {
        title: 'Some title',
        actions: {
            type: 'object',
            value: {
                actions: [
                    {
                        text: 'Some button',
                        state: BUTTON_STATE_PRIMARY,
                        size: BUTTON_SIZE_SM,
                    },
                    {
                        text: 'Yet another button',
                        state: BUTTON_STATE_SECONDARY,
                        size: BUTTON_SIZE_SM,
                    },
                ],
                align: ACTIONS_LIST_ALIGN_END,
            },
        },
    },
    events: [
        'close',
    ],
} as IStoryComponent;
