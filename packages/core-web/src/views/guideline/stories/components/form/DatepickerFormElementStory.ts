import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import DatepickerFormElement from
        '@cip/gormost-core-web/src/components/form/elements/datepicker/DatepickerFormElement';
import commonFormElementStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/commonFormElementStory';

export default {
    component: DatepickerFormElement,
    model: commonFormElementStory.model,
    params: {
        value: '21/06/2018',
        format: 'd/m/Y',
        ...(commonFormElementStory.params || {}),
    },
    events: [
        ...(commonFormElementStory.events || []),
    ],
} as IStoryComponent;
