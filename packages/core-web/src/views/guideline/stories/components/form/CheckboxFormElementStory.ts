import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import commonFormElementStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/commonFormElementStory';
import CheckboxFormElement from '@cip/gormost-core-web/src/components/form/elements/checkbox/CheckboxFormElement';

export default {
    component: CheckboxFormElement,
    params: {
        ...(commonFormElementStory.params || {}),
        value: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
    },
    events: [
        ...(commonFormElementStory.events || []),
    ],
    model: commonFormElementStory.model,
} as IStoryComponent;
