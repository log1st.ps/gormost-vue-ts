import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import commonFormElementStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/commonFormElementStory';
import InputFormElement from '@cip/gormost-core-web/src/components/form/elements/input/InputFormElement';
import {availableFormInputTypesList, INPUT_TYPE_TEXT} from '@cip/core/src/constants/form/input/formInputTypesConstants';

export default {
    component: InputFormElement,
    model: commonFormElementStory.model,
    params: {
        value: 'Some value',
        type: {
            type: 'list',
            value: INPUT_TYPE_TEXT,
            options: {
                options: availableFormInputTypesList,
            },
        },
        min: {
            type: 'number',
            value: 0,
        },
        max: {
            type: 'number',
            value: 1000,
        },
        step: {
            type: 'number',
            value: 1,
        },
        rows: {
            type: 'number',
            value: 5,
        },
        ...(commonFormElementStory.params || {}),
    },
    events: [
        ...(commonFormElementStory.events || []),
    ],
} as IStoryComponent;
