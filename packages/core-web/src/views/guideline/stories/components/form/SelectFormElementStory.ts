import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import DatepickerFormElement from
        '@cip/gormost-core-web/src/components/form/elements/datepicker/DatepickerFormElement';
import commonFormElementStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/commonFormElementStory';
import SelectFormElement from '@cip/gormost-core-web/src/components/form/elements/select/SelectFormElement';
import {PORTAL_SELECT} from '@cip/core/src/constants/portal/portalNames';

export default {
    component: SelectFormElement,
    model: commonFormElementStory.model,
    params: {
        value: {
            type: 'list',
            value: 'one',
            options: {
                options: [
                    {text: 'Uno', key: 'one'},
                    'two',
                    'three',
                    {text: '4', key: 'four'},
                ],
            },
        },
        options: {
            type: 'object',
            value: [
                {text: 'Uno', key: 'one'},
                'two',
                'three',
                {text: '4', key: 'four'},
            ],
        },
        isMultiple: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        withQuery: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        queryPlaceholder: 'Type query...',
        queryTimeout: {
            type: 'number',
            value: 250,
        },
        portalName: PORTAL_SELECT,
        ...(commonFormElementStory.params || {}),
    },
    events: [
        ...(commonFormElementStory.events || []),
        'query',
    ],
} as IStoryComponent;
