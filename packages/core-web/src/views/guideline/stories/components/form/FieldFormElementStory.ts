import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import FieldFormElement from '@cip/gormost-core-web/src/components/form/elements/field/FieldFormElement';
import commonFormElementStory
    from '@cip/gormost-core-web/src/views/guideline/stories/components/commonFormElementStory';

export default {
    component: FieldFormElement,
    defaultSlot: 'Default Slot content',
    params: {
        title: 'Title string',
        preHint: 'Title string',
        hint: 'Title string',
        error: 'Error string',
    },
    model: commonFormElementStory.model,
} as IStoryComponent;
