import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import HeaderComponent from '@cip/gormost-core-web/src/components/header/HeaderComponent';

export default {
    component: HeaderComponent,
    params: {
        logoPath: 'https://static.thenounproject.com/png/129830-200.png',
        withNotifications: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        hasNotifications: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isNotificationsListActive: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isUserControlActive: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        user: {
            type: 'object',
            value: {
                firstName: 'John',
                lastName: 'Doe',
                middleName: 'William',
                avatarPath: 'https://www.theuiaa.org/wp-content/uploads/2017/12/2018_banner.jpg',
            },
        },
    },
    events: [
        'notificationTriggerClick',
        'userClick',
        'logoClick',
    ],
} as IStoryComponent;
