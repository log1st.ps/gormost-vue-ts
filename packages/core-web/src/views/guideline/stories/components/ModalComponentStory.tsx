import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import ModalComponent from '@cip/gormost-core-web/src/components/modal/ModalComponent';
import {PORTAL_MODAL} from '@cip/core/src/constants/portal/portalNames';
import {VueComponent} from 'vue-tsx-helper';
import {Component, Emit, Prop} from 'vue-property-decorator';

@Component
class ModalStoryRender extends VueComponent<{}> {

    @Prop()
    public value: any;
    @Prop()
    public isBackdropClosable: any;
    @Prop()
    public portalName: any;

    public render() {
        return (
            <div>
                Select value to <code>true</code>
                <ModalComponent
                    value={this.value}
                    isBackdropClosable={this.isBackdropClosable}
                    portalName={this.portalName}
                    onInput={this.onCloseClick}
                >
                    <div
                        onClick={this.onCloseClick}
                        style={{
                            color: 'white',
                            height: '100%',
                            flexGrow: 1,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            cursor: 'pointer',
                        }}
                    >Close</div>
                </ModalComponent>
            </div>
        );
    }

    @Emit('input')
    protected onCloseClick() {
        return !this.value;
    }
}

export default {
    component: ModalStoryRender,
    model: {
        prop: 'value',
        event: 'input',
    },
    params: {
        value: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isBackdropClosable: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        portalName: PORTAL_MODAL,
    },
    events: [
        'input',
    ],
} as IStoryComponent;
