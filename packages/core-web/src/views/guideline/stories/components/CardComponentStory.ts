import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import CardComponent from '@cip/gormost-core-web/src/components/card/CardComponent';

export default {
    component: CardComponent,
    defaultSlot: 'Some card content',
    params: {
        hasShadow: {
            type: 'checkbox',
            value: true,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
    },
} as IStoryComponent;
