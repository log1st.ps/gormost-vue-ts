import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import ImageComponent from '@cip/gormost-core-web/src/components/image/ImageComponent';
import {availableImageTypesList, IMAGE_TYPE_BLOCK} from '@cip/core/src/constants/image/imageTypesConstants';
import {availableBgTypesList, IMAGE_BG_TYPE_COVER} from '@cip/core/src/constants/image/imageBackgroundTypesConstants';

export default {
    component: ImageComponent,
    params: {
        src: 'https://www.theuiaa.org/wp-content/uploads/2017/12/2018_banner.jpg',
        alt: 'Some alt',
        type: {
            type: 'list',
            value: IMAGE_TYPE_BLOCK,
            options: {
                options: availableImageTypesList,
            },
        },
        backgroundType: {
            type: 'list',
            value: IMAGE_BG_TYPE_COVER,
            options: {
                options: availableBgTypesList,
            },
        },
        height: {
            type: 'number',
            value: 200,
        },
        width: {
            type: 'number',
            value: 200,
        },
    },
} as IStoryComponent;
