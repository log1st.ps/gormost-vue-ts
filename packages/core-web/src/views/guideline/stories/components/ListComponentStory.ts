import ListComponent from '@cip/gormost-core-web/src/components/list/ListComponent';
import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import {IListElement} from '@cip/core/src/mixins/components/list/ListComponentMixin.d';

export default {
    component: ListComponent,
    params: {
        title: 'Some title',
        items: {
            type: 'object',
            value: [
                {
                    key: 'one',
                    title: 'One',
                    route: '#urlOne',
                },
                {
                    key: 'two',
                    title: 'Two',
                },
                {
                    key: 'three',
                    title: 'Three',
                    route: '#urlThree',
                },
            ] as IListElement[],
        },
    },
} as IStoryComponent;
