import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import {
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_SECONDARY,
    BUTTON_STATE_TERTIARY,
} from '@cip/core/src/constants/button/buttonStatesConstants';
import {ICON_TYPE_SPINNER} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import {
    ACTIONS_LIST_ALIGN_CENTER,
    availableActionsListAlignsList,
} from '@cip/core/src/constants/actionsList/actionsListAlignsConstants';
import {BUTTON_SIZE_MD} from '@cip/core/src/constants/button/buttonSizesConstants';
import {ACTION_LIST_DIRECTION_ROW} from '@cip/core/src/constants/actionsList/actionsListDirectionContstants';
import {availableRowDirectionsList} from '@cip/core/src/constants/grid/rowDirectionsConstants';

export default {
    component: ActionsListComponent,
    params: {
        actions: {
            type: 'object',
            value: [
                {
                    state: BUTTON_STATE_PRIMARY,
                    text: 'Primary',
                    isDisabled: true,
                    size: BUTTON_SIZE_MD,
                },
                {
                    state: BUTTON_STATE_SECONDARY,
                    text: 'Secondary',
                    size: BUTTON_SIZE_MD,
                },
                {
                    state: BUTTON_STATE_TERTIARY,
                    text: 'Tertiary',
                    leftIcon: ICON_TYPE_SPINNER,
                    size: BUTTON_SIZE_MD,
                },
            ],
        },
        align: {
            type: 'list',
            value: ACTIONS_LIST_ALIGN_CENTER,
            options: {
                options: availableActionsListAlignsList,
            },
        },
        direction: {
            type: 'list',
            value: ACTION_LIST_DIRECTION_ROW,
            options: {
                options: availableRowDirectionsList,
            },
        },
        gap: {
            type: 'number',
            value: 0,
        },
    },
} as IStoryComponent;
