import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import {VueComponent} from 'vue-tsx-helper';
import {Component, Prop} from 'vue-property-decorator';
import {
    CaptionTypographyComponent,
    HintTypographyComponent,
    PrimaryTypographyComponent, QuaternaryTypographyComponent, QuinaryTypographyComponent,
    SecondaryTypographyComponent, SenaryTypographyComponent, SeptenaryTypographyComponent,
    TertiaryTypographyComponent, TextTypographyComponent,
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';

@Component
class TypographyStoryRender extends VueComponent<{
    color?: string,
    text?: string,
}> {
    @Prop()
    public text?: string;

    @Prop()
    public color?: string;

    public render() {
        return (
            <div>
                {
                    [
                        [PrimaryTypographyComponent, 'primary'],
                        [SecondaryTypographyComponent, 'secondary'],
                        [TertiaryTypographyComponent, 'tertiary'],
                        [QuaternaryTypographyComponent, 'quaternary'],
                        [QuinaryTypographyComponent, 'quinary'],
                        [SenaryTypographyComponent, 'senary'],
                        [SeptenaryTypographyComponent, 'septenary'],
                        [TextTypographyComponent, 'text'],
                        [HintTypographyComponent, 'hint'],
                        [CaptionTypographyComponent, 'caption'],
                    ].map(([TypographyComponent, type]) => (
                        <div>
                            <TypographyComponent text={`${type}: ${this.text}`} color={this.color}/>
                            <br/>
                            <br/>
                        </div>
                    ))
                }
            </div>
        );
    }
}

export default {
    component: TypographyStoryRender,
    params: {
        text: 'Some text',
        color: '#efefef',
    },
} as IStoryComponent;
