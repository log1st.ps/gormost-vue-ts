import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import AccordionComponent from '../../../../components/accordion/AccordionComponent';

export default {
    component: AccordionComponent,
    defaultSlot: 'Some content',
    model: {
        prop: 'value',
        event: 'input',
    },
    params: {
        title: 'Some title',
        value: {
            type: 'checkbox',
            value: true,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
    },
    events: [
        'input',
    ],
} as IStoryComponent;
