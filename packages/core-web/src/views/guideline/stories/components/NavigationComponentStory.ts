import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import NavigationComponent from '@cip/gormost-core-web/src/components/navigation/NavigationComponent';
import {ICON_TYPE_MAGINIFIER, ICON_TYPE_SPINNER} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import {ICON_TYPE_PLUS} from '@cip/core/src/constants/icon/iconTypesConstants';

export default {
    component: NavigationComponent,
    model: {
        prop: 'value',
        event: 'input',
    },
    params: {
        items: {
            type: 'object',
            value: [
                {
                    key: 'one',
                    text: 'One',
                    route: undefined,
                    icon: ICON_TYPE_PLUS,
                    isDisabled: true,
                },
                {
                    key: 'two',
                    text: 'Two',
                    route: '#someUrl',
                    icon: ICON_TYPE_SPINNER,
                },
                {
                    key: 'three',
                    text: 'Three',
                    route: undefined,
                    icon: ICON_TYPE_MAGINIFIER,
                },
            ],
        },
        value: {
            type: 'list',
            value: 'one',
            options: {
                options: ['one', 'two', 'three'],
            },
        },
    },
    events: [
        'input',
    ],
} as IStoryComponent;
