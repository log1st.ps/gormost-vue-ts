import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import {VueComponent} from 'vue-tsx-helper';
import {Component, Prop} from 'vue-property-decorator';
import {availableRowAlignsList, ROW_ALIGN_START} from '@cip/core/src/constants/grid/rowAlignsConstants';
import {availableRowJustifiesList, ROW_JUSTIFY_START} from '@cip/core/src/constants/grid/rowJustifiesConstants';
import {
    availableRowDirectionsList, ROW_DIRECTION_COLUMN,
    ROW_DIRECTION_ROW,
    ROW_DIRECTION_ROW_REVERSE,
} from '@cip/core/src/constants/grid/rowDirectionsConstants';
import Row from '@cip/gormost-core-web/src/components/grid/Row';

@Component
class RowStoryRender extends VueComponent<{}> {
    @Prop()
    public align ?: any;
    @Prop()
    public justify ?: any;
    @Prop()
    public direction ?: any;
    @Prop()
    public isWrap ?: any;
    @Prop()
    public gap ?: any;

    public render() {
        return (
            <Row
                gap={this.gap}
                isWrap={this.isWrap}
                direction={this.direction}
                justify={this.justify}
                align={this.align}
            >
                {Array(12).fill(null).map((item, index) => (
                    <div
                        key={index}
                        style={{
                            background: 'red',
                            height: '40px',
                            width: '100px',
                            margin:
                                [ROW_DIRECTION_ROW, ROW_DIRECTION_ROW_REVERSE].indexOf(this.direction) > -1
                                    ? `0 ${this.gap / 2}px`
                                    : `${this.gap / 2}px 0`,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            color: 'white',
                            flexShrink: 0,
                        }}
                    >
                        {index + 1}
                    </div>
                ))}
            </Row>
        );
    }
}

export default {
    component: RowStoryRender,
    params: {
        gap: {
            type: 'number',
            value: 10,
        },
        align: {
            type: 'list',
            value: ROW_ALIGN_START,
            options: {
                options: availableRowAlignsList,
            },
        },
        justify: {
            type: 'list',
            value: ROW_JUSTIFY_START,
            options: {
                options: availableRowJustifiesList,
            },
        },
        direction: {
            type: 'list',
            value: ROW_DIRECTION_ROW,
            options: {
                options: availableRowDirectionsList,
            },
        },
        isWrap: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
    },
} as IStoryComponent;
