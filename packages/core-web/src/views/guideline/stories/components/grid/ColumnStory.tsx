import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import Column from '@cip/gormost-core-web/src/components/grid/Column';
import {availableRowAlignsList, ROW_ALIGN_CENTER} from '@cip/core/src/constants/grid/rowAlignsConstants';

export default {
    component: Column,
    defaultSlot: 'Something in slot',
    params: {
        size: '100px',
        isGrow: {
            type: 'checkbox',
            value: true,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isShrink: {
            type: 'checkbox',
            value: true,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        align: {
            type: 'list',
            value: ROW_ALIGN_CENTER,
            options: {
                options: availableRowAlignsList,
            },
        },
        gap: {
            type: 'number',
            value: 10,
        },
    },
} as IStoryComponent;
