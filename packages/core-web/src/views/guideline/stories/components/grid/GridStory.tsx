import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import Grid from '@cip/gormost-core-web/src/components/grid/Grid';
import {VueComponent} from 'vue-tsx-helper';
import {Component, Prop} from 'vue-property-decorator';

@Component
class GridStoryRender extends VueComponent<{}> {
    @Prop()
    public columns ?: number | number[];
    @Prop()
    public rows ?: number | number[];
    @Prop()
    public gap ?: number;

    public render() {
        return (
            <Grid
                rows={this.rows}
                gap={this.gap}
                columns={this.columns}
            >
                {Array(10).fill(null).map((item, index) => (
                    <div
                        key={index}
                        style={{
                            background: 'red',
                            height: '40px',
                        }}
                    />
                ))}
            </Grid>
        );
    }
}

export default {
    component: GridStoryRender,
    params: {
        gap: {
            type: 'number',
            value: 10,
        },
        columns: {
            type: 'number',
            value: 10,
        },
        rows: {
            type: 'number',
            value: 10,
        },
    },
} as IStoryComponent;
