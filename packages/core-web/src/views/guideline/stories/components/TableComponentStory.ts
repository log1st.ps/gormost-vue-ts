import {IStoryComponent} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import TableComponent from '@cip/gormost-core-web/src/components/table/TableComponent';

export default {
    component: TableComponent,
    params: {
        texts: {
            type: 'object',
            value: {
                noColumns: 'No provided columns',
            },
        },
        data: {
            type: 'object',
            value: [
                {
                    one: 'John',
                    two: 'Doe',
                    three: {
                        value: 'Russia',
                        hint: 'Russian Federation',
                    },
                    four: '+7 800 555 35 35',
                },
                {
                    one: 'John',
                    two: {
                        value: 'Doe',
                        hint: 'Jr.',
                    },
                    three: 'US',
                    four: '+1 5555 35 21',
                },
                {
                    one: 'John',
                    two: {
                        value: 'Doe',
                        hint: 'Jr.',
                    },
                    three: 'US',
                    four: '+1 5555 35 21',
                },
                {
                    one: 'John',
                    two: {
                        value: 'Doe',
                        hint: 'Jr.',
                    },
                    three: 'US',
                    four: '+1 5555 35 21',
                },
                {
                    one: 'John',
                    two: {
                        value: 'Doe',
                        hint: 'Jr.',
                    },
                    three: 'US',
                    four: '+1 5555 35 21',
                },
                {
                    one: 'John',
                    two: {
                        value: 'Doe',
                        hint: 'Jr.',
                    },
                    three: 'US',
                    four: '+1 5555 35 21',
                },
            ],
        },
        columns: {
            type: 'object',
            value: [
                {
                    key: 'one',
                    title: 'One',
                },
                {
                    key: 'two',
                    title: 'Two',
                    subTitle: 'SubTwo',
                },
                {
                    key: 'three',
                    subTitle: 'SubThree',
                },
                {
                    key: 'four',
                    title: 'Four',
                },
            ],
        },
        columnsOrder: {
            type: 'object',
            value: ['two', 'one', 'three'],
        },
        visibleKeys: {
            type: 'object',
            value: ['one', 'two', 'three'],
        },
        caption: 'Some caption',
        filteredColumnsKeys: {
            type: 'object',
            value: ['three'],
        },
        keyField: {
            type: 'list',
            value: 'one',
            options: {
                options: ['one', 'two', 'three', 'four'],
            },
        },
        isFixed: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isBordered: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isVerticallyStriped: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
        isHorizontallyStriped: {
            type: 'checkbox',
            value: false,
            options: {
                trueValue: true,
                falseValue: false,
            },
        },
    },
} as IStoryComponent;
