import {
    AbstractGuidelineApplication,
    IGuidelineStyles,
    IStoryComponent,
} from '@cip/core/src/views/helpers/AbstractGuidelineApplication';
import {Component} from 'vue-property-decorator';

import styles from './guidelineApplication.module.scss';
import ButtonElementStory from '@cip/gormost-core-web/src/views/guideline/stories/elements/ButtonElementStory';
import IconElementStory from '@cip/gormost-core-web/src/views/guideline/stories/elements/IconElementStory';
import HeaderComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/HeaderComponentStory';
import NavigationComponentStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/NavigationComponentStory';
import TypographyComponentStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/TypographyComponentStory';
import AccordionComponentStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/AccordionComponentStory';
import ActionsListComponentStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/ActionsListComponentStory';
import CardComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/CardComponentStory';
import DialogComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/DialogComponentStory';
import CheckboxFormElementStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/form/CheckboxFormElementStory';
import DatepickerFormElementStory from
        '@cip/gormost-core-web/src/views/guideline/stories/components/form/DatepickerFormElementStory';
import {PORTAL_DATEPICKER, PORTAL_MODAL, PORTAL_SELECT} from '@cip/core/src/constants/portal/portalNames';
import TableComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/TableComponentStory';
import GridStory from '@cip/gormost-core-web/src/views/guideline/stories/components/grid/GridStory';
import RowStory from '@cip/gormost-core-web/src/views/guideline/stories/components/grid/RowStory';
import ColumnStory from '@cip/gormost-core-web/src/views/guideline/stories/components/grid/ColumnStory';
import ImageComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/ImageComponentStory';
import ListComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/ListComponentStory';
import ModalComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/ModalComponentStory';
import TabsComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/tabs/TabsComponentStory';
import TabComponentStory from '@cip/gormost-core-web/src/views/guideline/stories/components/tabs/TabComponentStory';
import TabPanelsComponentStory
    from '@cip/gormost-core-web/src/views/guideline/stories/components/tabs/TabPanelsComponentStory';
import TabPanelComponentStory
    from '@cip/gormost-core-web/src/views/guideline/stories/components/tabs/TabPanelComponentStory';
import FieldFormElementStory
    from '@cip/gormost-core-web/src/views/guideline/stories/components/form/FieldFormElementStory';
import SelectFormElementStory
    from '@cip/gormost-core-web/src/views/guideline/stories/components/form/SelectFormElementStory';
import InputFormElementStory
    from '@cip/gormost-core-web/src/views/guideline/stories/components/form/InputFormElementStory';

@Component
class GuidelineApplication extends AbstractGuidelineApplication {
    protected styles: IGuidelineStyles = styles;

    protected activeComponent: string = 'icon';

    protected portals = [
        PORTAL_DATEPICKER,
        PORTAL_SELECT,
        PORTAL_MODAL,
    ];

    protected components: {
        [key: string]: IStoryComponent,
    } = {
        'icon': IconElementStory,
        'button': ButtonElementStory,
        'header': HeaderComponentStory,
        'navigation': NavigationComponentStory,
        'typography': TypographyComponentStory,
        'accordion': AccordionComponentStory,
        'actionsList': ActionsListComponentStory,
        'card': CardComponentStory,
        'dialog': DialogComponentStory,
        'table': TableComponentStory,
        'form/checkbox': CheckboxFormElementStory,
        'form/datepicker': DatepickerFormElementStory,
        'form/input': InputFormElementStory,
        'form/select': SelectFormElementStory,
        'form/field': FieldFormElementStory,
        'grid/grid': GridStory,
        'grid/row': RowStory,
        'grid/column': ColumnStory,
        'image': ImageComponentStory,
        'list': ListComponentStory,
        'modal': ModalComponentStory,
        'tabs/tabs': TabsComponentStory,
        'tabs/tab': TabComponentStory,
        'tabs/tabPanels': TabPanelsComponentStory,
        'tabs/tabPanel': TabPanelComponentStory,
    };

    protected tab: any = 'one';
}

export default GuidelineApplication;
