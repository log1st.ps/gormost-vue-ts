import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import './registerServiceWorker';

Vue.config.productionTip = false;

import { Plugin } from 'vue-fragment';
Vue.use(Plugin);

new Vue({
  router,
  store,
  render: (h) => h(App),
} as any).$mount('#app');
