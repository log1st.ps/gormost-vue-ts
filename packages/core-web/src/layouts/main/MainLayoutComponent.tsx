import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';

import styles from './mainLayoutComponent.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

@Component
class MainLayoutComponent extends VueComponent<{}> {
    public render() {
        const render = renderSlot.bind(this);
        return (
            <div class={styles.layout}>
                <div class={styles.header}>
                    <div class={styles.container}>
                        {render('header')}
                    </div>
                </div>
                <div class={styles.navigation}>
                    <div class={styles.container}>
                        {render('navigation')}
                    </div>
                </div>
                <div class={styles.content}>
                    <div class={styles.container}>
                        {render('default')}
                    </div>
                </div>
            </div>
        );
    }
}

export default MainLayoutComponent;

