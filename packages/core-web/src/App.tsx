import {Component} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';

import './App.scss';
import GuidelineApplication from '@cip/gormost-core-web/src/views/guideline/GuidelineApplication';

@Component
class App extends VueComponent<{}> {
    public render() {
        return (
            <div>
                <GuidelineApplication/>
            </div>
        );
    }
}

export default App;
