import ImageComponentMixin from '@cip/core/src/mixins/components/image/ImageComponentMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';
import {IMAGE_TYPE_BLOCK, IMAGE_TYPE_IMG} from '@cip/core/src/constants/image/imageTypesConstants';

import styles from './imageComponent.module.scss';

@Component
class ImageComponent extends ImageComponentMixin {
    get Tag(): string {
        return this.type === IMAGE_TYPE_IMG ? 'img' : 'div';
    }

    get computedWidth(): string | undefined {
        return typeof this.width === 'number' ? `${this.width}px` : (this.width || undefined);
    }

    get computedHeight(): string | undefined {
        return typeof this.height === 'number' ? `${this.height}px` : (this.height || undefined);
    }

    public render(): VNode {
        return (
            <this.Tag
                src={this.type === IMAGE_TYPE_IMG ? this.src : undefined}
                style={{
                    width: this.computedWidth,
                    height: this.computedHeight,
                    backgroundImage: this.type === IMAGE_TYPE_IMG ? undefined : `url(${this.src})`,
                }}
                class={{
                    [styles.image]: true,
                    [styles[`${this.type}Type`]]: true,
                    [styles[`${this.backgroundType}BackgroundType`]]: this.type === IMAGE_TYPE_BLOCK,
                }}
                alt={this.type === IMAGE_TYPE_IMG ? this.alt : undefined}
            />
        );
    }
}

export default ImageComponent;
