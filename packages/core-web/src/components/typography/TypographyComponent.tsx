import {
    BaseTypographyComponentMixin, CaptionTypographyComponentMixin, HintTypographyComponentMixin,
    PrimaryTypographyComponentMixin,
    QuaternaryTypographyComponentMixin, QuinaryTypographyComponentMixin,
    SecondaryTypographyComponentMixin, SenaryTypographyComponentMixin, SeptenaryTypographyComponentMixin,
    TertiaryTypographyComponentMixin, TextTypographyComponentMixin,
} from '@cip/core/src/mixins/components/typography/TypographyComponentMixin';
import {Component, Mixins} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './typographyComponent.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

@Component
class BaseTypographyComponent extends BaseTypographyComponentMixin {
    public render(): VNode {
        return (
            <div class={{
                [styles.typography]: true,
                [styles[`${this.type}Type`]]: true,
                [styles[`${this.color}Color`]]: !!this.color,
                [styles.transparent]: this.text === '&nbsp;',
            }}>
                {renderSlot.bind(this)('default', () => this.text)}
            </div>
        );
    }
}

// tslint:disable-next-line:max-classes-per-file
@Component
class PrimaryTypographyComponent
    extends Mixins(PrimaryTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class SecondaryTypographyComponent
    extends Mixins(SecondaryTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class TertiaryTypographyComponent
    extends Mixins(TertiaryTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class QuaternaryTypographyComponent
    extends Mixins(QuaternaryTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class QuinaryTypographyComponent
    extends Mixins(QuinaryTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class SenaryTypographyComponent
    extends Mixins(SenaryTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class SeptenaryTypographyComponent
    extends Mixins(SeptenaryTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class TextTypographyComponent
    extends Mixins(TextTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class CaptionTypographyComponent
    extends Mixins(CaptionTypographyComponentMixin, BaseTypographyComponent) {}

// tslint:disable-next-line:max-classes-per-file
@Component
class HintTypographyComponent
    extends Mixins(HintTypographyComponentMixin, BaseTypographyComponent) {}

export {
    PrimaryTypographyComponent,
    SecondaryTypographyComponent,
    TertiaryTypographyComponent,
    QuaternaryTypographyComponent,
    QuinaryTypographyComponent,
    SenaryTypographyComponent,
    SeptenaryTypographyComponent,
    TextTypographyComponent,
    HintTypographyComponent,
    CaptionTypographyComponent,
};

