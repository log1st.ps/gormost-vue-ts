import DialogComponentMixin from '@cip/core/src/mixins/components/dialog/DialogComponentMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './dialogComponent.module.scss';
import {isSlotExists, renderSlot} from '@cip/core/src/helpers/slotHelper';
import {SecondaryTypographyComponent} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {ICON_TYPE_CLOSE} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';

@Component
class DialogComponent extends DialogComponentMixin {
    public render(): VNode {
        return (
            <div
                class={{
                    [styles.dialog]: true,
                }}
            >
                <ButtonElement class={styles.closeButton} onClick={this.onCloseClick}>
                    <IconElement type={ICON_TYPE_CLOSE} height={11} width={11}/>
                </ButtonElement>
                <div class={styles.header}>
                    {(this.title || isSlotExists.bind(this)('header')) && (
                        <div class={styles.title}>
                            {renderSlot.bind(this)('header', () => (
                                <SecondaryTypographyComponent text={this.title}/>
                            ))}
                        </div>
                    )}
                </div>
                {isSlotExists.bind(this)('default') && (
                    <div class={styles.content}>
                        {renderSlot.bind(this)('default')}
                    </div>
                )}
                {(this.actions || isSlotExists.bind(this)('footer')) && (
                    <div class={styles.footer}>
                        {renderSlot.bind(this)('footer', () => (
                            <ActionsListComponent {...{attrs: this.actions} as any} gap={16}/>
                        ))}
                    </div>
                )}
            </div>
        );
    }
}

export default DialogComponent;
