import HeaderComponentMixin from '@cip/core/src/mixins/components/header/HeaderComponentMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './headerComponent.module.scss';
import {isSlotExists, renderSlot} from '@cip/core/src/helpers/slotHelper';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {ICON_TYPE_BELL} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import ImageComponent from '../image/ImageComponent';
import {IMAGE_TYPE_BLOCK} from '@cip/core/src/constants/image/imageTypesConstants';
import {IMAGE_BG_TYPE_CONTAIN, IMAGE_BG_TYPE_COVER} from '@cip/core/src/constants/image/imageBackgroundTypesConstants';

@Component
class HeaderComponent extends HeaderComponentMixin {

    public render(): VNode {
        const userName = this.user ? [
            this.user.lastName,
            this.user.firstName && `${this.user.firstName[0]}.`,
            this.user.middleName && `${this.user.middleName[0]}.`,
        ].filter(Boolean).join(' ') : undefined;

        return (
            <div class={styles.header}>
                {this.logoPath && (
                    <div class={styles.logo} onClick={this.onLogoClick}>
                        {renderSlot.bind(this)('logo', () => (
                            <ImageComponent
                                src={this.logoPath as string}
                                height={50}
                                width={120}
                                type={IMAGE_TYPE_BLOCK}
                                backgroundType={IMAGE_BG_TYPE_CONTAIN}
                            />
                        ))}
                    </div>
                )}
                {isSlotExists.bind(this)('default') && (
                    <div class={styles.content}>
                        {renderSlot.bind(this)('default')}
                    </div>
                )}
                {(
                    (this.withNotifications || this.user || isSlotExists.bind(this)('side'))
                ) && (
                    <div class={styles.side}>
                        {renderSlot.bind(this)('side', () => (
                            <div class={styles.sideContent}>
                                {this.withNotifications && (
                                    <div
                                        class={{
                                            [styles.notificationTrigger]: true,
                                            [styles.notificationTriggerActive]: this.isNotificationsListActive,
                                            [styles.notificationTriggerHasNotifications]: this.hasNotifications,
                                        }}
                                        onClick={this.onNotificationTriggerClick}
                                    >
                                        <IconElement height={'100%'} width={'100%'} type={ICON_TYPE_BELL}/>
                                    </div>
                                )}
                                {this.user && (
                                    <div
                                        class={{
                                            [styles.userInfo]: true,
                                            [styles.userInfoActive]: this.isUserControlActive,
                                        }}
                                        onClick={this.onUserClick}
                                    >
                                        {this.user.avatarPath && (
                                            <ImageComponent
                                                src={this.user.avatarPath}
                                                type={IMAGE_TYPE_BLOCK}
                                                height={30}
                                                width={30}
                                                backgroundType={IMAGE_BG_TYPE_COVER}
                                                alt={userName}
                                                class={styles.userImage}
                                            />
                                        )}
                                        <div class={styles.userName}>
                                            {userName}
                                        </div>
                                    </div>
                                )}
                            </div>
                        ))}
                    </div>
                )}
            </div>
        );
    }
}

export default HeaderComponent;
