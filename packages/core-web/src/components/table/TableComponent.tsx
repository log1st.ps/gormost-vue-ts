import TableComponentMixin from '@cip/core/src/mixins/components/table/TableComponentMixin';
import {ITableColumn, ITableDataRow} from '@cip/core/src/mixins/components/table/TableComponentMixin.d';
import {Component, Emit, Mixins, Prop} from 'vue-property-decorator';

import styles from './tableComponent.module.scss';
import {VueComponent} from 'vue-tsx-helper';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import Fragment from 'vue-fragment/src/component';
import {
    CaptionTypographyComponent,
    HintTypographyComponent,
    QuinaryTypographyComponent,
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';

interface ITableText {
    texts?: {
        noColumns?: string,
        noData?: string,
    };
}

@Component
class TableText
    extends VueComponent<ITableText>
    implements ITableText {
    @Prop() public texts ?: ITableText['texts'];
}

// tslint:disable-next-line:max-classes-per-file
@Component
class TableComponent
    extends Mixins(TableText, TableComponentMixin) {

    public render() {
        const texts = this.texts || {};
        return (
            this.columns ? (
                <div
                    class={{
                        [styles.table]: true,
                        [styles.isFixed]: this.isFixed,
                        [styles.isBordered]: this.isBordered,
                        [styles.isCondensed]: this.isCondensed,
                        [styles.isVerticallyStriped]: this.isVerticallyStriped && !this.isHorizontallyStriped,
                        [styles.isHorizontallyStriped]: this.isHorizontallyStriped && !this.isVerticallyStriped,
                        [styles.isTotallyStriped]: this.isVerticallyStriped && this.isHorizontallyStriped,
                    }}
                >
                    {this.caption && (
                        <caption class={styles.caption}>
                            {renderSlot.bind(this)('caption', () => (
                                <HintTypographyComponent text={this.caption}/>
                            ))}
                        </caption>
                    )}
                    {this.renderHeader()}
                    {this.renderBody()}
                </div>
            ) : (
                <div class={styles.noColumns}>
                    <HintTypographyComponent text={texts.noColumns}/>
                </div>
            )
        );
    }

    protected renderRow(row: ITableDataRow, index: number, key: string) {
        const render = renderSlot.bind(this);

        return render('row', () => (
            <div class={[styles.row, styles.bodyRow]} key={key}>
                {this.sortedColumns.map((column) => (
                    render(`r_${column.key}`, () => {
                        const cell = row[column.key];

                        let value: any = null;
                        let hint: any = null;

                        if (
                            typeof cell === 'object'
                            && cell !== null
                        ) {
                            value = cell.value || null;
                            hint = cell.hint || null;
                        } else {
                            value = cell;
                        }

                        return (
                            <div key={column.key} class={[styles.cell, styles.bodyCell]}>
                                {cell && (
                                    <div class={styles.value}>
                                        <CaptionTypographyComponent text={value}/>
                                    </div>
                                )}
                                {hint && (
                                    <div class={styles.hint}>
                                        <HintTypographyComponent text={hint}/>
                                    </div>
                                )}
                            </div>
                        );
                    }, {
                        column,
                        row,
                        index,
                    })
                ))}
            </div>
        ), {
            row,
            index,
        });
    }

    protected renderBody() {
        const texts = this.texts || {};
        return (
            <div class={styles.body}>
                {this.data ? (
                    this.data.map((row, index) => {

                        let keyField: any = this.keyField in row ? row[this.keyField] : index;

                        if (typeof keyField === 'object' && 'value' in keyField) {
                            keyField = keyField.value;
                        }

                        return (
                            this.renderRow(row, index, keyField)
                        );
                    })
                ) : (
                    <div class={styles.noDataRow}>
                        <div class={styles.noDataCell}>
                            {texts.noData}
                        </div>
                    </div>
                )}
            </div>
        );
    }

    protected renderColumn(column: ITableColumn, index: number) {
        const render = renderSlot.bind(this);

        return render(`h_${column.key}`, () => (
            <div class={[styles.cell, styles.headCell]}>
                {render('cell', () => ([
                    column.title && <div class={styles.cellTitle}>
                        <QuinaryTypographyComponent text={column.title}/>
                    </div>,
                    column.subTitle && <div class={styles.cellSubTitle}>
                        <HintTypographyComponent text={column.subTitle}/>
                    </div>,
                ]))}
            </div>
        ), {
            classes: {
                cell: [styles.cell, styles.headCell],
            },
            column,
            index,
        });
    }

    protected renderHeader() {
        return renderSlot.bind(this)('header', () => (
            <div class={styles.header}>
                <div class={[styles.row, styles.headRow]}>
                    {
                        this.sortedColumns.map((column, index) => (
                            <Fragment
                                key={column.key}
                            >
                                {this.renderColumn(column, index)}
                            </Fragment>
                        ))
                    }
                </div>
            </div>
        ), {
            classes: {
                head: styles.head,
                row: [styles.row, styles.headRow],
                cell: [styles.cell, styles.headCell],
            },
        });
    }
}

export default TableComponent;
