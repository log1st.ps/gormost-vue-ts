import TabPanelsComponentMixin from '@cip/core/src/mixins/components/tabs/TabPanelsComponentMixin';
import {Component} from 'vue-property-decorator';

import styles from './tabPanels.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import TransitionComponent from '@cip/gormost-core-web/helpers/TransitionComponent';

@Component
class TabPanelsComponent extends TabPanelsComponentMixin {

    protected x: number = 0;
    protected isSwiping: boolean = false;
    protected swipeDirection: 'Left' | 'Right' = 'Right';

    public render() {
        const type = this.isSwipeable ? `swipe${this.swipeDirection}` : 'fade';
        return (
            <div
                class={styles.tabs}
                onTouchstart={this.onTouchStart}
                onTouchmove={this.onTouchMove}
            >
                <TransitionComponent
                    name={styles[type]}
                    tag={'fragment'}
                    enterActiveClass={styles[`${type}EnterActive`]}
                    leaveActiveClass={styles[`${type}LeaveActive`]}
                    enterClass={styles[`${type}Enter`]}
                    leaveClass={styles[`${type}Leave`]}
                    onAfterLeave={this.onAfterLeave}
                >
                    {renderSlot.bind(this)('default')}
                </TransitionComponent>
            </div>
        );
    }

    protected onAfterLeave() {
        this.isSwiping = false;
    }

    protected onTouchStart(e) {
        this.x = e.touches[0].clientX;
    }

    protected onTouchMove(e) {
        if (
            !this.isSwipeable
            || this.isSwiping
        ) {
           return false;
        }
        this.isSwiping = true;

        this.swipeDirection = this.x < e.touches[0].clientX ? 'Left' : 'Right';

        if (this.x < e.touches[0].clientX) {
            this.showPrevious();
        }   else {
            this.showNext();
        }
    }
}

export default TabPanelsComponent;
