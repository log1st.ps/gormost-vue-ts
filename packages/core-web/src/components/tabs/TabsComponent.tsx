import TabsComponentMixin from '@cip/core/src/mixins/components/tabs/TabsComponentMixin';
import {Component, Watch} from 'vue-property-decorator';

import styles from './tabsComponent.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';
import TabComponent from '@cip/gormost-core-web/src/components/tabs/TabComponent';
import {TAB_TYPE_DEFAULT, TAB_TYPE_STEP} from '@cip/core/src/constants/tabs/tabTypesConstants';

@Component
class TabsComponent extends TabsComponentMixin {

    @Watch('value', {
        immediate: true,
    })
    public async updateValue() {
        await waitForTicks(this);

        const allTabs: string[] = this.$children.map((child: TabComponent | any) => child.name);
        const activeTabs: string[] = [];

        this.$children.forEach((child: TabComponent | any) => {
            if (child.type === TAB_TYPE_DEFAULT && this.value === child.name) {
                activeTabs.push(child.name);
            }
            if (child.type === TAB_TYPE_STEP && this.value) {
                if (allTabs.indexOf(child.name) <= allTabs.indexOf(this.value)) {
                    activeTabs.push(child.name);
                }
            }
        });

        this.$children.forEach((child: TabComponent | any) => {
            child.setIsActive(activeTabs.indexOf(child.name) > -1);

            child.$on('click', this.onTabClick);
        });
    }

    public render() {
        return (
            <div class={styles.tabs}>
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default TabsComponent;
