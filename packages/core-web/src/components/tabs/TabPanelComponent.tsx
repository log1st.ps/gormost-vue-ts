import TabPanelComponentMixin from '@cip/core/src/mixins/components/tabs/TabPanelComponentMixin';
import {Component} from 'vue-property-decorator';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

import styles from './tabPanelComponent.module.scss';

@Component
class TabPanelComponent extends TabPanelComponentMixin {
    public render() {
        return (
            <div class={styles.panel}>
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default TabPanelComponent;
