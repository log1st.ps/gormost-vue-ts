import TabComponentMixin from '@cip/core/src/mixins/components/tabs/TabComponentMixin';
import {Component} from 'vue-property-decorator';

import styles from './tabComponent.module.scss';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {TAB_TYPE_DEFAULT, TAB_TYPE_STEP} from '@cip/core/src/constants/tabs/tabTypesConstants';

@Component
class TabComponent extends TabComponentMixin {
    protected isActive: boolean = false;

    public setIsActive(isActive: boolean) {
        this.isActive = isActive;
    }

    public render() {
        const iconSize = this.type ? {
            [TAB_TYPE_STEP]: 32,
            [TAB_TYPE_DEFAULT]: 24,
        }[this.type] || 24 : 24;

        return (
            <div
                class={{
                    [styles.tab]: true,
                    [styles[`${this.type}Type`]]: true,
                    [styles.isDisabled]: this.isDisabled,
                    [styles.isActive]: this.isActive,
                }}
                onClick={this.onClick}
            >
                {this.icon && (
                    <div class={styles.icon}>
                        {typeof this.icon === 'number' ? (
                            <div class={styles.number}>{this.icon}</div>
                        ) : (
                            <IconElement
                                type={this.icon}
                                height={iconSize}
                                width={iconSize}
                            />
                        )}
                    </div>
                )}
                {this.label && (
                    <div class={styles.label}>
                        {this.label}
                    </div>
                )}
            </div>
        );
    }
}

export default TabComponent;
