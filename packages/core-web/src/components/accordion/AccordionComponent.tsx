import AccordionComponentMixin from '@cip/core/src/mixins/components/accordion/AccordionComponentMixin';
import {Component, Watch} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './accordionComponent.module.scss';
import {isSlotExists, renderSlot} from '@cip/core/src/helpers/slotHelper';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import {ICON_TYPE_ANGLE} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {QuaternaryTypographyComponent} from '../typography/TypographyComponent';
import anime from 'animejs';

@Component
class AccordionComponent extends AccordionComponentMixin {

    // @ts-ignore
    public $refs: {
        content: HTMLDivElement,
    };

    protected animation?: anime.AnimeInstance;

    public render(): VNode {
        return (
            <div class={styles.accordion}>
                {(this.title || isSlotExists.bind(this)('header')) && (
                    <div class={styles.header}>
                        <div class={styles.title}>
                            {renderSlot.bind(this)('header', () => (
                                <QuaternaryTypographyComponent text={this.title}/>
                            ))}
                        </div>
                        <ButtonElement
                            class={{
                                [styles.toggleTrigger]: true,
                                [styles.toggleTriggerActive]: this.value,
                            }}
                            onClick={this.onClick}
                        >
                            <IconElement type={ICON_TYPE_ANGLE} height={12} width={12}/>
                        </ButtonElement>
                    </div>
                )}
                <div
                    class={styles.content}
                    ref={'content'}
                >
                    <div class={styles.contentInner}>
                        {renderSlot.bind(this)('default')}
                    </div>
                </div>
            </div>
        );
    }

    public mounted(): void {
        if (!this.value) {
            this.$refs.content.style.height = '0px';
        }
    }

    @Watch('value')
    protected animateContent() {
        if (this.animation) {
            this.animation.pause();
        }

        this.animation = anime({
            targets: this.$refs.content,
            duration: 150,
            easing: 'easeInOutCubic',
            height: `${!this.value ? 0 : this.$refs.content.scrollHeight}px`,
        });
    }
}

export default AccordionComponent;

