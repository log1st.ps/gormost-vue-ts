import StylerComponentMixin from '@cip/core/src/mixins/components/styler/StylerComponentMixin';
import {Component} from 'vue-property-decorator';

import styles from './styler.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

@Component
class Styler extends StylerComponentMixin {
    public render() {
        return (
            <div
                class={{
                    [styles[`${this.background}Background`]]: !!this.background,
                    [styles[`${this.color}Color`]]: !!this.color,
                }}
                style={{
                    ...(this.height ? {height: `${this.height}px`} : {}),
                }}
            >
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default Styler;
