import ActionsListComponentMixin from '@cip/core/src/mixins/components/actionsList/ActionsListComponentMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './actionsListComponent.module.scss';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import {
    ACTION_LIST_DIRECTION_ROW,
    ACTION_LIST_DIRECTION_ROW_REVERSE,
} from '@cip/core/src/constants/actionsList/actionsListDirectionContstants';

@Component
class ActionsListComponent extends ActionsListComponentMixin {

    public render(): VNode {
        return (
            <div
                class={{
                    [styles.actionsList]: true,
                    [styles[`${this.align}Align`]]: true,
                    [styles[`${this.direction}Direction`]]: true,
                }}
                style={
                    this.gap ? {
                        margin:
                            [
                                ACTION_LIST_DIRECTION_ROW,
                                ACTION_LIST_DIRECTION_ROW_REVERSE,
                            ].indexOf(String(this.direction)) > -1
                                ? `0 -${this.gap / 2}px`
                                : `-${this.gap / 2}px 0`,
                    } : {}
                }
            >
                {this.actions && this.actions.map(({key, onClick: click, ...attrs}) => (
                    <ButtonElement
                        key={key}
                        class={{
                            [styles.action]: true,
                            [styles[`${this.direction}ActionGapped`]]: !this.gap,
                        }}
                        {...{
                            attrs,
                            ...(click ? {on: {click}} : {}),
                        }}
                        style={
                            this.gap ? {
                                margin:
                                    [
                                        ACTION_LIST_DIRECTION_ROW,
                                        ACTION_LIST_DIRECTION_ROW_REVERSE,
                                    ].indexOf(String(this.direction)) > -1
                                        ? `0 ${this.gap / 2}px`
                                        : `${this.gap / 2}px 0`,
                            } : {}
                        }
                    />
                ))}
            </div>
        );
    }
}

export default ActionsListComponent;
