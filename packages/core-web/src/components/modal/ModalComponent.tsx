import ModalComponentMixin from '@cip/core/src/mixins/components/modal/ModalComponentMixin';
import {Component, Prop, Watch} from 'vue-property-decorator';

import styles from './modalComponent.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import {Portal} from 'portal-vue';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';
import getRandomString from '@cip/core/src/helpers/getRandomString';

@Component
class ModalComponent extends ModalComponentMixin {

    // @ts-ignore
    public $refs: {
        modal: HTMLDivElement,
        content: HTMLDivElement,
    };

    get uniqueKey() {
        return getRandomString();
    }

    public render() {
        return (
            // @ts-ignore
            <Portal to={this.portalName} inline slim>
                <div
                    key={this.uniqueKey}
                    class={{
                        [styles.modal]: true,
                        [styles.isActive]: this.value,
                        [styles.isClosable]: this.isBackdropClosable,
                    }}
                    ref={'modal'}
                    onClick={this.handleOutsideClick}
                >
                    <div
                        class={styles.container}
                        style={{
                            ...(this.maxWidth ? {maxWidth: `${this.maxWidth}px`} : {}),
                        }}
                    >
                        <div
                            class={styles.content}
                            ref={'content'}
                        >
                            {renderSlot.bind(this)('default')}
                        </div>
                    </div>
                </div>
            </Portal>
        );
    }

    protected handleOutsideClick(e) {
        if (
            !this.isBackdropClosable
            || !e.target
            || !this.$refs.content
            || this.$refs.content.contains(e.target)
        ) {
            return;
        }

        this.setValue(false);
    }
}

export default ModalComponent;
