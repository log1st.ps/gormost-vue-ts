import CardComponentMixin from '@cip/core/src/mixins/components/card/CardComponentMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './cardComponent.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

@Component
class CardComponent extends CardComponentMixin {
    public render(): VNode {
        return (
            <div
                class={{
                    [styles.card]: true,
                    [styles.hasShadow]: this.hasShadow,
                }}
            >
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default CardComponent;
