import ListComponentMixin from '@cip/core/src/mixins/components/list/ListComponentMixin';
import {Component} from 'vue-property-decorator';

import styles from './listComponent.module.scss';
import {
    CaptionTypographyComponent,
    QuaternaryTypographyComponent,
    TertiaryTypographyComponent, TextTypographyComponent,
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';

@Component
class ListComponent extends ListComponentMixin {
    public render() {
        return (
            <div class={styles.list}>
                {this.title && (
                    <div class={styles.title}>
                        <TertiaryTypographyComponent text={this.title}/>
                    </div>
                )}
                {this.items && (
                    <div class={styles.items}>
                        {this.items.map((item) => (
                            <div
                                key={item.key}
                                class={{
                                    [styles.item]: true,
                                    [styles.itemClickable]: item.onClick || item.route,
                                }}
                            >
                                {(item.onClick || item.route) ? (
                                    <ButtonElement
                                        url={item.route}
                                        text={item.title}
                                    />
                                ) : (
                                    <CaptionTypographyComponent
                                        text={item.title}
                                    />
                                )}
                            </div>
                        ))}
                    </div>
                )}
            </div>
        );
    }
}

export default ListComponent;
