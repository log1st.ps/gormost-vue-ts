import FormComponentMixin from '@cip/core/src/mixins/components/form/FormComponentMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

import styles from './formComponent.module.scss';

@Component
class FormComponent extends FormComponentMixin {
    public render(): VNode {
        return (
            <form id={this.id} class={styles.form}>
                {renderSlot.bind(this)('default')}
            </form>
        );
    }
}

export default FormComponent;
