import DatepickerFormElementMixin from '@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin';
import {Component, Mixins, Watch} from 'vue-property-decorator';
import {Portal} from 'portal-vue';

import styles from './datepickerFormElement.module.scss';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import {
    availableExtendedIconTypesList,
    ICON_TYPE_CALENDAR,
    ICON_TYPE_CLOSE,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import flatpickr from 'flatpickr';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';
import positionObserver, {IPositionObserver} from '@cip/core/src/helpers/positionObserver';
import CommonFormComponent from '@cip/gormost-core-web/src/components/form/elements/CommonFormComponent';
import {Instance} from 'flatpickr/dist/types/instance';
import Fragment from 'vue-fragment/src/component';

@Component
class DatepickerFormElement
    extends Mixins(DatepickerFormElementMixin, CommonFormComponent) {

    // @ts-ignore
    public $refs: {
        target: HTMLDivElement,
        input: HTMLInputElement,
        popup: HTMLDivElement,
        picker: HTMLDivElement,
    };

    protected observer?: IPositionObserver;

    protected pickerInstance?: Instance;

    public onPortalBlur({relatedTarget: target}: { relatedTarget: HTMLElement } | any) {
        if (
            this.pickerInstance
            && this.pickerInstance.calendarContainer.contains(target)
        ) {
            return;
        }
        this.onBlur();
    }

    public render() {
        const withClearer =
            this.isClearable
            && !this.isDisabled
            && !this.isReadonly;

        return (
            <Fragment>
                <label
                    class={{
                        [styles.input]: true,
                        [styles.value]: true,
                        [styles.isDisabled]: this.isDisabled,
                        [styles.isReadonly]: this.isReadonly,
                        [styles.isValid]: this.isValid,
                        [styles.isInvalid]: this.isInvalid,
                        [styles.hasAppearance]: this.hasAppearance,
                        [styles.isFocused]: this.isFocused,
                    }}
                    ref={'target'}
                >
                    {this.before && (
                        <div class={styles.before}>
                            {availableExtendedIconTypesList.indexOf(this.before) > -1 ? (
                                <IconElement type={this.before} height={16} width={16}/>
                            ) : this.before}
                        </div>
                    )}
                    <input
                        type='text'
                        value={this.value}
                        onInput={this.onInput}
                        placeholder={this.placeholder}
                        disabled={this.isDisabled}
                        ref={'input'}
                        class={styles.field}
                        onFocus={this.onFocus}
                        onBlur={this.onPortalBlur}
                    />
                    {this.after && (
                        <div class={styles.after}>
                            {availableExtendedIconTypesList.indexOf(this.after) > -1 ? (
                                <IconElement type={this.after} height={16} width={16}/>
                            ) : this.after}
                        </div>
                    )}
                    <ButtonElement
                        class={styles.sideButton}
                        isSquare
                        onClick={this.onSideButtonClick}
                        scopedSlots={{
                            before: () => (
                                <IconElement
                                    type={
                                        (withClearer && this.value)
                                            ? ICON_TYPE_CLOSE
                                            : ICON_TYPE_CALENDAR
                                    }
                                    height={(withClearer && this.value) ? 9 : 16}
                                    width={(withClearer && this.value) ? 9 : 16}
                                />
                            ),
                        }}
                    />
                </label>
                {(
                    // @ts-ignore
                    <Portal to={this.portalName} inline>
                        <div ref={'popup'} class={{
                            [styles.popup]: true,
                            [styles.popupIsFocused]: this.isFocused,
                        }}>
                            <div ref={'picker'}/>
                        </div>
                    </Portal>
                )}
            </Fragment>
        );
    }

    public async mounted() {
        await waitForTicks(this);

        this.pickerInstance = flatpickr(this.$refs.picker, {
            dateFormat: this.format,
            inline: true,
            onChange: (selectedDates, dateStr) => {
                this.updateValue(dateStr);
                window.requestAnimationFrame(() => {
                    // (document.activeElement as HTMLElement).blur();
                });
            },
            onMonthChange: () => {
                requestAnimationFrame(() => {
                    this.$refs.input.focus();
                });
            },
            defaultDate: this.value,
        });

        document.addEventListener('click', this.onPopupClick);

        this.observer = positionObserver(this.$refs as any).observe();

        this.setValidity();
    }

    public beforeDestroy() {
        if (!this.pickerInstance) {
            return;
        }
        document.removeEventListener('click', this.onPopupClick);
    }

    @Watch('isFocused')
    protected focusInput(value) {
        if (value) {
            this.$refs.input.focus();
        }
    }

    protected onPopupClick({target}: { target: EventTarget | null }) {
        if (
            this.pickerInstance
            && target
            && !this.pickerInstance.calendarContainer.contains(target as HTMLElement)
        ) {
            return;
        }
        this.$refs.input.focus();
    }

    @Watch('format')
    protected async onFormatUpdate() {
        await waitForTicks(this);

        if (!this.pickerInstance) {
            return;
        }
        this.pickerInstance.set('dateFormat', this.format);
    }

    @Watch('value', {
        immediate: true,
    })
    protected async onValueUpdate() {
        await waitForTicks(this);

        if (!this.pickerInstance) {
            return;
        }

        this.pickerInstance.setDate(this.value);
    }

    protected onInput(e: Event | any) {
        if (!e.target) {
            return;
        }
        this.updateValue(e.target.value);
    }

    @Watch('isValid')
    @Watch('isInvalid')
    protected async setValidity() {
        await waitForTicks(this);

        if (!this.$refs.input) {
            return;
        }
        this.$refs.input.setCustomValidity((this.isValid || this.isInvalid) ? 'invalid' : '');
    }
}

export default DatepickerFormElement;
