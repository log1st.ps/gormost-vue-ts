import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';

@Component
class CommonFormComponent extends VueComponent<{}> {

    protected isFocused = false;

    public onSideButtonClick() {
        const withClearer =
            this.isClearable
            && !this.isDisabled
            && !this.isReadonly;

        if (
            withClearer && this.value
        ) {
            this.updateValue(this.clearValue);
        }   else {
            this.isFocused = true;
        }
    }

    protected async onFocus() {
        this.isFocused = true;
    }

    protected onBlur() {
        this.isFocused = false;
    }
}

export default CommonFormComponent;
