import FieldFormElementMixin from '@cip/core/src/mixins/components/form/elements/field/FieldFormElementMixin';
import {Component} from 'vue-property-decorator';

import styles from './fieldFormElement.module.scss';
import {
    CaptionTypographyComponent,
    HintTypographyComponent,
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import {isSlotExists, renderSlot} from '@cip/core/src/helpers/slotHelper';

@Component
class FieldFormElement extends FieldFormElementMixin {
    public render() {
        return (
            <div
                class={{
                    [styles.field]: true,
                }}
            >
                {this.title && (
                    <div class={styles.title}>
                        <CaptionTypographyComponent text={this.title}/>
                    </div>
                )}
                {this.preHint && (
                    <div class={styles.preHint}>
                        <HintTypographyComponent text={this.preHint}/>
                    </div>
                )}
                {isSlotExists.bind(this)('default') && (
                    <div class={styles.content}>
                        {renderSlot.bind(this)('default')}
                    </div>
                )}
                {this.hint && (
                    <div class={styles.hint}>
                        <HintTypographyComponent text={this.hint}/>
                    </div>
                )}
                {this.error && (
                    <div class={styles.error}>
                        {
                            (Array.isArray(this.error) ? this.error : [this.error]).map((err) => (
                                <HintTypographyComponent text={err}/>
                            ))
                        }
                    </div>
                )}
            </div>
        );
    }
}

export default FieldFormElement;
