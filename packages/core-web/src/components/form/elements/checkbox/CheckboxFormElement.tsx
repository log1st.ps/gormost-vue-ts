import CheckboxFormElementMixin from '@cip/core/src/mixins/components/form/elements/checkbox/CheckboxFormElementMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './checkboxFormElement.module.scss';
import {CaptionTypographyComponent} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import {CHECKBOX_TYPE_CHECKBOX} from '@cip/core/src/constants/form/checkbox/formCheckboxTypesConstants';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {ICON_TYPE_CHECK} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import getRandomString from '@cip/core/src/helpers/getRandomString';

@Component
class CheckboxFormElement extends CheckboxFormElementMixin {

    protected id?: string;
    public onClick() {
        if (this.isDisabled || this.isReadonly) {
            return;
        }

        this.updateValue(
            this.value === this.trueValue ? this.falseValue : this.trueValue,
        );
    }

    public created(): void {
        this.id = getRandomString(6);
    }

    public render(): VNode {
        return (
            <div
                class={{
                    [styles.container]: true,
                    [styles.isDisabled]: this.isDisabled,
                    [styles.isReadonly]: this.isReadonly,
                    [styles.isValid]: this.isValid,
                    [styles.isInvalid]: this.isInvalid,
                    [styles.hasAppearance]: this.hasAppearance,
                    [styles.isChecked]: this.value === this.trueValue,
                }}
                onClick={this.onClick}
            >
                <div
                    class={{
                        [styles.icon]: true,
                        [styles[`${this.type}Type`]]: true,
                        [styles.isChecked]: this.value === this.trueValue,
                    }}
                    id={this.id}
                >
                    {this.type === CHECKBOX_TYPE_CHECKBOX && (
                        <IconElement type={ICON_TYPE_CHECK} height={10} width={10}/>
                    )}
                </div>
                <label htmlfor={this.id} class={styles.label}>
                    <CaptionTypographyComponent text={this.placeholder}/>
                </label>
            </div>
        );
    }
}

export default CheckboxFormElement;
