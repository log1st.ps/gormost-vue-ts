import {Component, Emit, Mixins, Prop, Watch} from 'vue-property-decorator';
import Fragment from 'vue-fragment/src/component';
import {
    availableExtendedIconTypesList, ICON_TYPE_ANGLE, ICON_TYPE_CHECK,
    ICON_TYPE_CLOSE,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import positionObserver, {IPositionObserver} from '@cip/core/src/helpers/positionObserver';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';
import SelectFormElementMixin from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin';
import CommonFormComponent from '@cip/gormost-core-web/src/components/form/elements/CommonFormComponent';
import {Portal} from 'portal-vue';

import styles from './selectFormElement.module.scss';
import {HintTypographyComponent} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import {VueComponent} from 'vue-tsx-helper';

interface ISelectFormElementText {
    texts?: {
        noOptions?: string,
    };
}

@Component
class SelectFormElementText extends VueComponent<ISelectFormElementText> {
    @Prop()
    public texts ?: ISelectFormElementText['texts'];
}

// tslint:disable-next-line:max-classes-per-file
@Component
class SelectFormElement
    extends Mixins(SelectFormElementMixin, CommonFormComponent, SelectFormElementText)
    implements ISelectFormElementText {

    // @ts-ignore
    public $refs: {
        target: HTMLDivElement,
        popup: HTMLDivElement,
        query: HTMLDivElement,
    };

    protected observer?: IPositionObserver;

    public onPortalBlur({relatedTarget: target}: { relatedTarget: HTMLElement } | any) {
        if (
            this.$refs.popup.contains(target)
        ) {
            return;
        }
        this.onBlur();
    }

    public render() {
        const withClearer =
            this.isClearable
            && !this.isDisabled
            && !this.isReadonly;

        const texts = this.texts || {};

        return (
            <Fragment>
                <div
                    class={{
                        [styles.input]: true,
                        [styles.value]: true,
                        [styles.isDisabled]: this.isDisabled,
                        [styles.isReadonly]: this.isReadonly,
                        [styles.isValid]: this.isValid,
                        [styles.isInvalid]: this.isInvalid,
                        [styles.hasAppearance]: this.hasAppearance,
                        [styles.isFocused]: this.isFocused,
                    }}
                    ref={'target'}
                    tabindex={0}
                    onFocus={this.onFocus}
                    onBlur={this.onPortalBlur}
                >
                    {this.before && (
                        <div class={styles.before}>
                            {availableExtendedIconTypesList.indexOf(this.before) > -1 ? (
                                <IconElement type={this.before} height={16} width={16}/>
                            ) : this.before}
                        </div>
                    )}
                    {!this.computedValue.length && (
                        <div class={styles.placeholder}>
                            <HintTypographyComponent text={this.placeholder}/>
                        </div>
                    )}
                    {!this.isMultiple && (
                        <div class={styles.singleValue}>
                            {!!this.computedValueOptions.length && (
                                this.computedValueOptions[0].text
                            )}
                        </div>
                    )}
                    {this.isMultiple && (
                        <div class={styles.center}>
                            {(!!this.computedValueOptions.length) && (
                                <div class={styles.tags}>
                                    {this.computedValueOptions.map((value) => (
                                        <div
                                            key={value.key}
                                            class={styles.tag}
                                            onClick={
                                                this.onTagClick(value.key)
                                            }
                                        >
                                            {value.text}
                                        </div>
                                    ))}
                                </div>
                            )}
                            {(!this.isDisabled && !this.isReadonly && this.withQuery) && (
                                <input
                                    type='text'
                                    value={this.query}
                                    onInput={this.onQueryInput}
                                    placeholder={this.queryPlaceholder}
                                    disabled={this.isDisabled}
                                    ref={'query'}
                                    class={styles.query}
                                />
                            )}
                        </div>
                    )}
                    {this.after && (
                        <div class={styles.after}>
                            {availableExtendedIconTypesList.indexOf(this.after) > -1 ? (
                                <IconElement type={this.after} height={16} width={16}/>
                            ) : this.after}
                        </div>
                    )}
                    <ButtonElement
                        class={{
                            [styles.sideButton]: true,
                            [styles.sideButtonActive]: !(withClearer && this.value) && this.isFocused,
                        }}
                        isSquare
                        onClick={this.onSideButtonClick}
                        scopedSlots={{
                            before: () => (
                                <IconElement
                                    type={
                                        (withClearer && this.value)
                                            ? ICON_TYPE_CLOSE
                                            : ICON_TYPE_ANGLE
                                    }
                                    height={9}
                                    width={9}
                                />
                            ),
                        }}
                    />
                </div>
                {(
                    // @ts-ignore
                    <Portal to={this.portalName} inline>
                        <div
                            ref={'popup'}
                            tabindex={0}
                            class={{
                                [styles.popup]: true,
                                [styles.popupIsFocused]: this.isFocused,
                            }}
                        >
                            {!!this.computedOptions.length && (
                                <div class={styles.options}>
                                    {this.computedOptions.map((option) => (
                                        <div
                                            key={option.key}
                                            class={styles.option}
                                            onClick={this.onTagClick(option.key)}
                                            title={option.text || undefined}
                                        >
                                            {renderSlot.bind(this)('default', () => (
                                                <Fragment>
                                                    {option.isSelected && (
                                                        <div class={styles.optionSelectedIcon}>
                                                            <IconElement
                                                                type={ICON_TYPE_CHECK}
                                                                height={10}
                                                                width={13}
                                                            />
                                                        </div>
                                                    )}
                                                    {option.text}
                                                </Fragment>
                                            ))}
                                        </div>
                                    ))}
                                </div>
                            )}
                            {!this.computedOptions.length && (
                                renderSlot.bind(this)('empty', () => (
                                    <div class={styles.noOptions}>
                                        {texts.noOptions}
                                    </div>
                                ))
                            )}
                        </div>
                    </Portal>
                )}
            </Fragment>
        );
    }

    public async mounted() {
        await waitForTicks(this);

        this.$refs.popup.addEventListener('click', this.onPopupClick);

        this.observer = positionObserver(this.$refs as any).observe();
    }

    public beforeDestroy() {
        if (!this.$refs.popup) {
            return;
        }
        this.$refs.popup.removeEventListener('click', this.onPopupClick);
    }

    protected onTagClick(index) {
        return async () => {
            this.selectTag(index);

            if (
                !this.isMultiple
                && document.activeElement
            ) {
                window.requestAnimationFrame(() => {
                    (document.activeElement as HTMLElement).blur();
                });
            }
        };
    }

    protected handleClick({target}: { target: HTMLElement } | any) {
        if (this.$refs.target.contains(target)) {
            this.onFocus();
        }
    }

    @Watch('isFocused')
    protected focusInput(value) {
        if (value) {
            this.$refs.target.focus();
        }
    }

    protected onPopupClick() {
        this.$refs.target.focus();
    }

    protected onInput(e: Event | any) {
        if (!e.target) {
            return;
        }
        this.updateValue(e.target.value);
    }
}

export default SelectFormElement;
