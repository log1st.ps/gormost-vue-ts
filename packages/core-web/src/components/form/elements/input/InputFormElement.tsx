import InputFormElementMixin from '@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin';
import {Component, Mixins, Prop, Watch} from 'vue-property-decorator';
import {
    availableExtendedIconTypesList,
    ICON_TYPE_CLOSE,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import CommonFormComponent from '@cip/gormost-core-web/src/components/form/elements/CommonFormComponent';

import styles from './inputFormElement.module.scss';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';
import {
    INPUT_TYPE_NUMBER,
    INPUT_TYPE_TEXT,
    INPUT_TYPE_TEXTAREA,
} from '@cip/core/src/constants/form/input/formInputTypesConstants';

@Component
class InputFormElement
    extends Mixins(InputFormElementMixin, CommonFormComponent) {

    get Tag() {
        return this.type === INPUT_TYPE_TEXTAREA ? 'textarea' : 'input';
    }

    // @ts-ignore
    public $refs: {
        input: HTMLInputElement,
    };

    public render() {
        const withClearer =
            this.isClearable
            && !this.isDisabled
            && !this.isReadonly;

        return (
            <label
                class={{
                    [styles.input]: true,
                    [styles.value]: true,
                    [styles.isDisabled]: this.isDisabled,
                    [styles.isReadonly]: this.isReadonly,
                    [styles.isValid]: this.isValid,
                    [styles.isInvalid]: this.isInvalid,
                    [styles.hasAppearance]: this.hasAppearance,
                    [styles.isFocused]: this.isFocused,
                }}
            >
                {this.before && (
                    <div class={styles.before}>
                        {availableExtendedIconTypesList.indexOf(this.before) > -1 ? (
                            <IconElement type={this.before} height={16} width={16}/>
                        ) : this.before}
                    </div>
                )}
                <this.Tag
                    {...{
                        attrs: {
                            ...(this.type
                                ? (
                                    ([INPUT_TYPE_TEXT, INPUT_TYPE_NUMBER].indexOf(this.type)) ? {type: this.type} : {}
                                )
                                : {type: 'text'}),
                            ...(this.type === INPUT_TYPE_NUMBER ? {
                                step: this.step,
                            } : {}),
                            ...(this.type === INPUT_TYPE_TEXTAREA ? {
                                rows: this.rows,
                            } : {}),
                        },
                    }}
                    type={this.type}
                    onInput={this.onInput}
                    placeholder={this.placeholder}
                    disabled={this.isDisabled}
                    ref={'input'}
                    class={styles.field}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                >
                    {this.type === INPUT_TYPE_TEXTAREA ? this.value : ''}
                </this.Tag>
                {this.after && (
                    <div class={styles.after}>
                        {availableExtendedIconTypesList.indexOf(this.after) > -1 ? (
                            <IconElement type={this.after} height={16} width={16}/>
                        ) : this.after}
                    </div>
                )}
                {(withClearer && ([undefined, this.clearValue].indexOf(this.value) === -1)) && (
                    <ButtonElement
                        class={styles.sideButton}
                        isSquare
                        onClick={this.onSideButtonClick}
                        scopedSlots={{
                            before: () => (
                                <IconElement
                                    type={ICON_TYPE_CLOSE}
                                    height={9}
                                    width={9}
                                />
                            ),
                        }}
                    />
                )}
            </label>
        );
    }

    public async mounted() {
        await waitForTicks(this);

        this.setValidity();
        this.handleTypeChange();
    }

    @Watch('value')
    protected watchValue(val) {
        this.$refs.input.value = val;
    }

    @Watch('type')
    protected async handleTypeChange() {
        await waitForTicks(this);

        if (this.type === INPUT_TYPE_NUMBER) {
            this.attachNumberInputEvents();
        } else {
            this.deAttachNumberInputEvents();
        }
    }

    protected attachNumberInputEvents() {
        this.$refs.input.addEventListener('mousewheel', this.onMouseWheel);
    }

    protected deAttachNumberInputEvents() {
        this.$refs.input.removeEventListener('mousewheel', this.onMouseWheel);
    }

    protected onMouseWheel(e: WheelEvent | any) {
        e.preventDefault();
        let multiplier = this.step || 1;

        if (e.ctrlKey) {
            multiplier *= 2;
        }
        if (e.shiftKey) {
            multiplier *= 10;
        }

        this.onInput({
            target: {
                value: +(this.value || 0) + (e.deltaY < 0 ? 1 : -1) * multiplier,
            },
        });
    }

    protected async onInput(e: Event | any) {
        if (!e.target) {
            return;
        }

        let value = e.target.value;

        if (this.type === INPUT_TYPE_NUMBER) {
            value = +value;
            if (typeof this.max === 'number' && this.max < value) {
                value = this.max;
            }
            if (typeof this.min === 'number' && this.min > value) {
                value = this.min;
            }
        }


        this.updateValue(value);
    }

    @Watch('isValid')
    @Watch('isInvalid')
    protected async setValidity() {
        await waitForTicks(this);

        if (!this.$refs.input) {
            return;
        }
        this.$refs.input.setCustomValidity((this.isValid || this.isInvalid) ? 'invalid' : '');
    }
}

export default InputFormElement;

