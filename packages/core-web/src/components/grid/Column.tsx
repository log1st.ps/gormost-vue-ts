import ColumnComponentMixin from '@cip/core/src/mixins/components/grid/ColumnComponentMixin';
import {Component} from 'vue-property-decorator';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

import styles from './column.module.scss';

@Component
class Column extends ColumnComponentMixin {

    get computedStyles() {
        return {
            ...(this.size ? {width : this.size} : {}),
            ...(this.gap ? {margin: `0 ${this.gap / 2}px`} : {}),
        };
    }

    public render() {
        return (
            <div
                class={{
                    [styles.column]: true,
                    [styles[`grow${this.isGrow ? 1 : 0}`]]: typeof this.isGrow === 'boolean',
                    [styles[`shrink${this.isShrink ? 1 : 0}`]]: typeof this.isShrink === 'boolean',
                    [styles[`${this.align}Align`]]: this.align,
                }}
                style={this.computedStyles}
            >
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default Column;

