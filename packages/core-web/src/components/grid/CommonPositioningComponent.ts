import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';

@Component
class CommonPositioningComponent extends VueComponent<{}> {
    get computedStyles() {
        const offset = Array.isArray(this.offset)
            ? this.offset.map((item) => String(item))
            : this.offset;
        return {
            ...(
                Object.entries(
                    offset
                        ? (
                            typeof this.offset === 'number'
                                ? ['top', 'left', 'right', 'bottom'].reduce((currentValue, attribute) => ({
                                    ...currentValue,
                                    [attribute]: offset,
                                }), {})
                                : {
                                    top: offset[0],
                                    right: offset[1],
                                    bottom: offset[2] || offset[0],
                                    left: offset[3] || offset[1],
                                }
                        ) : {},
                ).reduce((currentValue, [attribute, value]) => ({
                    ...currentValue,
                    [attribute]: `${value}px`,
                }), {})
            ),
        };
    }
}

export default CommonPositioningComponent;
