import AbsoluteComponentMixin from '@cip/core/src/mixins/components/grid/AbsoluteComponentMixin';
import {Component, Mixins} from 'vue-property-decorator';

import styles from './absolute.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import CommonPositioningComponent from '@cip/gormost-core-web/src/components/grid/CommonPositioningComponent';
import { Portal } from 'portal-vue';
import {PORTAL_FIXED, PORTAL_SELECT} from '@cip/core/src/constants/portal/portalNames';

@Component
class Absolute
    extends Mixins(AbsoluteComponentMixin, CommonPositioningComponent) {

    get computedAbsoluteStyles() {
        return {
            ...this.computedStyles,
            ...(typeof this.align === 'number' ? {top: `${this.align}px`} : {}),
            ...(typeof this.position === 'number' ? {left: `${this.position}px`} : {}),
            ...(
                (Array.isArray(this.align) && this.align.length === 2)
                    ? {left: `${this.align[1]}px`}
                    : {}
            ),
            ...(
                (Array.isArray(this.position) && this.position.length === 2)
                    ? {left: `${this.position[1]}px`}
                    : {}
            ),
            ...(typeof this.zIndex === 'number' ? {
                zIndex: this.zIndex,
            } : {}),
        };
    }

    public render() {
        return (
            this.isFixed ? (
                // @ts-ignore
                <Portal to={PORTAL_FIXED}>
                    {this.renderContent()}
                </Portal>
            ) : (
                this.renderContent()
            )
        );
    }

    protected renderContent() {
        const type =
            (this.isFixed && 'fixed')
            || (this.isSticky && 'sticky')
            || 'absolute';

        return (
            <div
                class={{
                    [styles[type]]: true,
                    [styles[`${this.align}Align`]]: Array.isArray(this.align) && this.align.length === 2,
                    [styles[`${this.position}Position`]]: Array.isArray(this.position) && this.position.length === 2,
                }}
                style={this.computedAbsoluteStyles}
            >
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default Absolute;
