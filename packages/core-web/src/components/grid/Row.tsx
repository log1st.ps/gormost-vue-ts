import RowComponentMixin from '@cip/core/src/mixins/components/grid/RowComponentMixin';
import {Component} from 'vue-property-decorator';
import styles from './row.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import {
    ROW_DIRECTION_ROW,
    ROW_DIRECTION_ROW_REVERSE,
} from '@cip/core/src/constants/grid/rowDirectionsConstants';

@Component
class Row extends RowComponentMixin {
    public render() {
        return (
            <div
                class={{
                    [styles.row]: true,
                    [styles[`${this.align}Align`]]: this.align,
                    [styles[`${this.justify}Justify`]]: this.justify,
                    [styles[`${this.direction}Direction`]]: this.direction,
                    [styles.isWrap]: this.isWrap,
                }}
                style={{
                    ...(this.gap ? {
                        margin:
                            [ROW_DIRECTION_ROW, ROW_DIRECTION_ROW_REVERSE].indexOf(String(this.direction)) > -1
                            ? `0 -${this.gap / 2}px`
                            : `-${this.gap / 2}px 0`,
                    } : {}),
                }}
            >
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default Row;
