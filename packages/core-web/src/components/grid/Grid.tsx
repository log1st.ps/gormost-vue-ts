import GridComponentMixin from '@cip/core/src/mixins/components/grid/GridComponentMixin';

import styles from './grid.module.scss';
import {Component} from 'vue-property-decorator';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';

const computeCells = (cells: number | number[], gap: number): string => {

    if (Array.isArray(cells)) {
        const sum = cells.reduce((value, item) => value + item, 0);
        return cells.map((cell) => `calc(${cell / sum * 100}% - ${gap}px)`).join(' ');
    }   else {
        return `repeat(${cells}, calc(${1 / cells * 100}% - ${gap}px))`;
    }
};

@Component
class Grid extends GridComponentMixin {
    get computedStyles() {
        return {
            ...(this.gap ? {gridGap: `${this.gap}px`} : {}),
            ...(this.rows ? {gridTemplateRows:  computeCells(this.rows, this.gap || 0) } : {}),
            ...(this.columns ? {gridTemplateColumns:  computeCells(this.columns, this.gap || 0) } : {}),
        };
    }

    public render() {
        return (
            <div
                class={{
                    [styles.grid]: true,
                }}
                style={this.computedStyles}
            >
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default Grid;
