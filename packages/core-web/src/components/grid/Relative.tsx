import RelativeComponentMixin from '@cip/core/src/mixins/components/grid/RelativeComponentMixin';
import {Component, Mixins} from 'vue-property-decorator';

import styles from './relative.module.scss';
import {renderSlot} from '@cip/core/src/helpers/slotHelper';
import CommonPositioningComponent from '@cip/gormost-core-web/src/components/grid/CommonPositioningComponent';

@Component
class Relative
    extends Mixins(RelativeComponentMixin, CommonPositioningComponent) {

    public render() {
        return (
            <div
                class={styles.relative}
                style={this.computedStyles}
            >
                {renderSlot.bind(this)('default')}
            </div>
        );
    }
}

export default Relative;
