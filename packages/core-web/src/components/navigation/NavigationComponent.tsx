import NavigationComponentMixin from '@cip/core/src/mixins/components/navigation/NavigationComponentMixin';
import {Component} from 'vue-property-decorator';
import {VNode} from 'vue';

import styles from './navigationComponent.module.scss';
import ButtonElement from '@cip/gormost-core-web/src/elements/button/ButtonElement';
import {BUTTON_TYPE_BUTTON, BUTTON_TYPE_LINK} from '@cip/core/src/constants/button/buttonTypesConstants';
import {isSlotExists, renderSlot} from '@cip/core/src/helpers/slotHelper';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {BUTTON_STATE_FONT_REGULAR, BUTTON_STATE_SECONDARY} from '@cip/core/src/constants/button/buttonStatesConstants';

@Component
class NavigationComponent extends NavigationComponentMixin {

    public render(): VNode {
        return (
            <div class={{
                [styles.navigation]: true,
            }}>
                {renderSlot.bind(this)('default', () => (
                    this.items && this.items.map((item) => (
                        <ButtonElement
                            key={item.key}
                            text={item.text}
                            type={item.route ? BUTTON_TYPE_LINK : BUTTON_TYPE_BUTTON}
                            url={item.route}
                            isDisabled={item.isDisabled}
                            state={[BUTTON_STATE_SECONDARY, BUTTON_STATE_FONT_REGULAR]}
                            withRouterClasses
                            routerClasses={{
                                active: styles.activeItem,
                            }}
                            class={styles.item}
                            onClick={this.onRenderedItemClick(item.key)}
                            scopedSlots={{
                                ...(
                                    isSlotExists.bind(this)('item') ? {
                                        default: renderSlot.bind(this)('item', () => item.text, item),
                                    } : {}
                                ),
                                ...(
                                    item.icon ? {
                                        before: () => (
                                            <IconElement
                                                type={item.icon as string}
                                                height={20}
                                                width={18}
                                            />
                                        ),
                                    } : {}
                                ),
                            }}
                        />
                    ))
                ))}
            </div>
        );
    }

    protected onRenderedItemClick(key: string) {
        return () => {
            this.onItemClick(key);
        };
    }
}

export default NavigationComponent;
