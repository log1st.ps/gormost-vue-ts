import {Component, Prop} from 'vue-property-decorator';
import {BUTTON_TYPE_BUTTON} from '@cip/core/src/constants/button/buttonTypesConstants';
import {VNode} from 'vue';
import styles from './buttonElement.module.scss';
import ButtonElementMixin from '@cip/core/src/mixins/elements/button/ButtonElementMixin';
import {isSlotExists, renderSlot} from '@cip/core/src/helpers/slotHelper';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {
    availableButtonSizes,
    BUTTON_SIZE_LG,
    BUTTON_SIZE_MD,
    BUTTON_SIZE_SM,
    BUTTON_SIZE_XL,
    BUTTON_SIZE_XLG,
    BUTTON_SIZE_XS,
} from '@cip/core/src/constants/button/buttonSizesConstants';
import {
    availableExtendedIconTypes,
    ICON_TYPE_SPINNER,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import {modifiers as m} from 'vue-tsx-support';

const getIconSize = (size?: availableButtonSizes): number => (size ? {
    [BUTTON_SIZE_XS]: 14,
    [BUTTON_SIZE_SM]: 16,
    [BUTTON_SIZE_MD]: 18,
    [BUTTON_SIZE_LG]: 22,
    [BUTTON_SIZE_XL]: 26,
    [BUTTON_SIZE_XLG]: 28,
}[size] : 0);

@Component
class ButtonElement
    extends ButtonElementMixin {

    get Tag(): string {
        return (
            this.type === BUTTON_TYPE_BUTTON
            || this.isDisabled
        )
            ? 'button'
            : (this.url ? 'router-link' : 'button');
    }

    @Prop()
    public leftIcon ?: availableExtendedIconTypes | any;

    @Prop()
    public rightIcon ?: availableExtendedIconTypes | any;

    public render(): VNode {
        return (
            <this.Tag
                onClick={this.onButtonClick}
                nativeOnClick={this.onButtonClick}
                tabindex={this.isDisabled ? -1 : (this.tabIndex || -1)}
                activeClass={
                    this.withRouterClasses
                    && this.routerClasses
                    && this.routerClasses.active
                }
                exactActiveClass={
                    this.withRouterClasses
                    && this.routerClasses
                    && this.routerClasses.exactActive
                }
                class={[
                    {
                        [styles.button]: true,
                        [styles[`${this.size}Size`]]: !!this.size,
                        [styles[`${this.type}Type`]]: true,
                        [styles.isDisabled]: this.isDisabled,
                        [styles.isBlock]: this.isBlock,
                        [styles.isRounded]: this.isRounded,
                        [styles.isSquare]: this.isSquare,
                        [styles.isLoading]: this.isLoading,
                        [styles.withLoadingProgress]: !!this.loadingProgress,
                    },
                    ...(
                        Array.isArray(this.state) ? this.state : [this.state]
                        ).filter(
                            (state) => state && (`${state}State` in styles),
                        ).map((state) => styles[`${state}State`]),
                    ]
                }
                disabled={this.isDisabled}
                to={this.computedRoute}
            >
                {this.isLoading && (
                    <div class={styles.loader}>
                        <IconElement
                            class={styles.loaderIcon}
                            type={ICON_TYPE_SPINNER}
                            height={getIconSize(this.size)}
                            width={getIconSize(this.size)}
                        />
                    </div>
                )}
                {!!this.loadingProgress && (
                    <div
                        class={styles.loadingProgress}
                        style={{width: `${this.loadingProgress > 100 ? 100 : this.loadingProgress}%`}}
                    />
                )}
                <div class={styles.container}>
                    {
                        (isSlotExists.bind(this)('before') || this.leftIcon)
                        && (
                            <div class={styles.before}>
                                {renderSlot.bind(this)(
                                    'before',
                                    typeof this.leftIcon === 'function'
                                        ? this.leftIcon()
                                        : (
                                            <IconElement
                                                type={this.leftIcon as any}
                                                height={getIconSize(this.size)}
                                                width={getIconSize(this.size)}
                                            />
                                        ),
                                )}
                            </div>
                        )
                    }
                    {
                        (isSlotExists.bind(this)('default') || this.text)
                        && renderSlot.bind(this)('default', (
                            <div class={styles.content}>{this.text}</div>
                        ))
                    }
                    {
                        (isSlotExists.bind(this)('after') || this.rightIcon)
                        && renderSlot.bind(this)(
                            'after', typeof this.rightIcon === 'function'
                                ? this.rightIcon()
                                : (
                                    <div class={styles.after}>
                                        <IconElement
                                            type={this.rightIcon as any}
                                            height={getIconSize(this.size)}
                                            width={getIconSize(this.size)}
                                        />
                                    </div>
                                ),
                        )
                    }
                </div>
            </this.Tag>
        );
    }

    protected onButtonClick() {
        if (this.isDisabled) {
           return;
        }

        this.onClick();
    }
}

export default ButtonElement;
