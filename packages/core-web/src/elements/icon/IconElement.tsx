import IconElementMixin from '@cip/core/src/mixins/elements/icon/IconElementMixin';
import {Component, Mixins, Prop} from 'vue-property-decorator';
import {VNode} from 'vue';
import styles from './iconElement.module.scss';
import {availableExtendedIconTypes} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';

@Component
class IconElement extends IconElementMixin {

    @Prop()
    public type !: availableExtendedIconTypes;

    get Tag(): string {
        return require(`./assets/${this.type}.inline.svg`).default;
    }

    get computedStyles() {
        return [
            ['height', this.height],
            ['width', this.width],
        ]
            .filter(([type, bound]) => Boolean(bound))
            .reduce((currentValue, [type, bound]) => ({
                ...currentValue,
                [String(type)]: `${bound}px`,
            }), {});
    }
    protected source ?: VNode;

    public render(): VNode {
        return (
            <div
                class={{
                    [styles.icon]: true,
                }}
                style={this.computedStyles}
            >
                <this.Tag
                    class={{
                        [styles.source]: true,
                    }}
                />
            </div>
        );
    }
}

export default IconElement;
