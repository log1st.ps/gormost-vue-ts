import {availableIconTypes, availableIconTypesList} from '@cip/core/src/constants/icon/iconTypesConstants';

export const ICON_TYPE_MAGINIFIER = 'magnifier';
export const ICON_TYPE_SPINNER = 'spinner';
export const ICON_TYPE_BELL = 'bell';
export const ICON_TYPE_CLOSE = 'close';
export const ICON_TYPE_ANGLE = 'angle';
export const ICON_TYPE_CHECK = 'check';
export const ICON_TYPE_CALENDAR = 'calendar';
export const ICON_TYPE_BUILDING = 'building';
export const ICON_TYPE_TREE = 'tree';
export const ICON_TYPE_USER_SETTINGS = 'user-settings';
export const ICON_TYPE_GEAR = 'gear';
export const ICON_TYPE_DOWNLOAD = 'download';
export const ICON_TYPE_PRINT = 'print';
export const ICON_TYPE_FILTER = 'filter';
export const ICON_TYPE_ARROW = 'arrow';

export type availableExtendedIconTypes =
    availableIconTypes
    | typeof ICON_TYPE_MAGINIFIER
    | typeof ICON_TYPE_SPINNER
    | typeof ICON_TYPE_BELL
    | typeof ICON_TYPE_CLOSE
    | typeof ICON_TYPE_ANGLE
    | typeof ICON_TYPE_CHECK
    | typeof ICON_TYPE_CALENDAR
    | typeof ICON_TYPE_BUILDING
    | typeof ICON_TYPE_TREE
    | typeof ICON_TYPE_USER_SETTINGS
    | typeof ICON_TYPE_GEAR
    | typeof ICON_TYPE_DOWNLOAD
    | typeof ICON_TYPE_PRINT
    | typeof ICON_TYPE_FILTER
    | typeof ICON_TYPE_ARROW
    ;

export const availableExtendedIconTypesList = [
    ...availableIconTypesList,
    ICON_TYPE_MAGINIFIER,
    ICON_TYPE_SPINNER,
    ICON_TYPE_BELL,
    ICON_TYPE_CLOSE,
    ICON_TYPE_ANGLE,
    ICON_TYPE_CHECK,
    ICON_TYPE_CALENDAR,
    ICON_TYPE_BUILDING,
    ICON_TYPE_TREE,
    ICON_TYPE_USER_SETTINGS,
    ICON_TYPE_GEAR,
    ICON_TYPE_DOWNLOAD,
    ICON_TYPE_PRINT,
    ICON_TYPE_FILTER,
    ICON_TYPE_ARROW,
];
