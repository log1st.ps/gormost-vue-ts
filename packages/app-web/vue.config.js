const common = require("../../vue.config.common");

module.exports = {
    ...common,
    devServer: {
        proxy: {
            "^/api": {
                target: process.env.VUE_APP_API_URL,
                pathRewrite: {
                    "^/api": `/gm-rest/api/v1`,
                },
                logLevel: "debug",
            },
            "^/nsi": {
                target: process.env.VUE_APP_API_URL,
                pathRewrite: {
                    "^/nsi": `/gm-nsi-rest/api/v1`,
                },
                logLevel: "debug",
            },
        },
    },
};
