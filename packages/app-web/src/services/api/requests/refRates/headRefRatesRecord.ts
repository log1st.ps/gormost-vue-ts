import {headRequest} from '@/services/api/api';

// Определяет наличие элемента по id
export default async (
    // id
    id: number,
) => await headRequest(`/api/ref-rates/${id}`);
