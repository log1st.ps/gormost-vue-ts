import {getRequest} from '@/services/api/api';

// Возвращает список элементов в соответстивии с параметрами
export default async (
   query: {
       // Optional String isActiveFilter values (y,all)
        isActiveFilter?: string,
       // Optional Integer limit
        limit?: string,
       // Optional String search (example evId==541)
        search?: string,
       // Optional String sort (example atr-,atr2,atr3)
        sort?: string,
       // Optional Integer start
        start?: string,
    } = {},
) => await getRequest(`/nsi/accounts/list`, { params: query });
