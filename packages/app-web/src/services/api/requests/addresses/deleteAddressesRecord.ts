import {deleteRequest} from '@/services/api/api';

// Удаляет элемент по id
export default async (
    // id
    id: number,
) => await deleteRequest(`/nsi/addresses/${id}`);
