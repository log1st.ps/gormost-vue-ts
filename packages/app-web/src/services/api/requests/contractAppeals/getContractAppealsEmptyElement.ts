import {getRequest} from '@/services/api/api';

// Возвращает пустое заявление (структуру)
export default async (

) => await getRequest(`/api/contract-appeals/empty-element`);
