import {postRequest} from '@/services/api/api';

// Создает элемент
export default async (
   body: {
       // entity
        entity?: any,
    } = {},
) => await postRequest(`/nsi/duls/`, body);
