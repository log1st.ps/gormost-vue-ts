import {optionsRequest} from '@/services/api/api';

// Выводит перечень доступных http-методов
export default async (

) => await optionsRequest(`/api/contract-statuses`);
