import {postRequest} from '@/services/api/api';

// Создает элемент
export default async (
   body: {
       // entity
        entity?: any,
    } = {},
) => await postRequest(`/api/ref-axle-loads/`, body);
