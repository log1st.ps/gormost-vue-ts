import {getRequest} from '@/services/api/api';

// Возвращает элемент по id
export default async (
    // Long Id
    id: number,
) => await getRequest(`/api/ref-settlement-periods/${id}`);
