import {putRequest} from '@/services/api/api';

// Обновляет существующее заявление
export default async (
   body: {
       // Обновлённое заявление
        entity?: any,
    } = {},
) => await putRequest(`/api/requests/update`, body);
