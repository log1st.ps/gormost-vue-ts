import {putRequest} from '@/services/api/api';

// Обновляет элемент
export default async (
   body: {
       // entity
        entity: any,
    },
) => await putRequest(`/api/ref-target-appeals/`, body);
