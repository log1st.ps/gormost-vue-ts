import {getRequest} from '@/services/api/api';

// Для тестирования доступности приложения
export default async (

) => await getRequest(`/nsi/tests/`);
