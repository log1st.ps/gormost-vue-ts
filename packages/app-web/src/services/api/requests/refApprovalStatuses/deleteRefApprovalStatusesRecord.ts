import {deleteRequest} from '@/services/api/api';

// Удаляет элемент по id
export default async (
    // id
    id: number,
) => await deleteRequest(`/api/ref-approval-statuses/${id}`);
