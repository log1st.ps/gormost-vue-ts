import {getRequest} from '@/services/api/api';

// Возвращает элемент по id
export default async (
    // Long Id
    id: number,
) => await getRequest(`/api/ref-approval-statuses/${id}`);
