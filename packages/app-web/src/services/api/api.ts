import axios from 'axios';

export const getRequest = axios.get;
export const postRequest = axios.post;
export const deleteRequest = axios.delete;
export const headRequest = axios.head;
export const putRequest = axios.put;
export const optionsRequest = (url: string) => axios({
    url,
    method: 'options',
});
