const fs = require('fs')
const axios = require('axios');

[
    ['api', 'http://192.168.40.235:8112/gm-rest/api/v1/v2/api-docs?group=/api/v1'],
    ['nsi', 'http://192.168.40.235:8112/gm-nsi-rest/api/v1/v2/api-docs?group=/api/v1'],
]
    .forEach(async([namespace, url]) => {

        const requestsByDomains = {};

        const {data: {paths}} = await axios.get(url);

        const requests = Object.entries(paths)
            .filter(([key]) => key.split('/').filter(Boolean).length)
            .map(([url, info]) => {
                const domainMatches = url.split('/').filter(Boolean)
                const domain = convertKebabToCamelCase(domainMatches[0])

                if (!(domain in requestsByDomains)) {
                    requestsByDomains[domain] = []
                }

                Object.entries(info).map(([method, {description, parameters}]) => {
                    const requestName = convertKebabToCamelCase(
                        [
                            method,
                            domain,
                            ...(1 in domainMatches ? [convertKebabToCamelCase(domainMatches[1])] : [])
                        ]
                            .join('-')
                            .replace('{id}', 'Record')
                    )

                    requestsByDomains[domain].push({
                        url,
                        description,
                        method,
                        requestName,
                        parameters,
                    })
                })
            })


        Object.entries(requestsByDomains).forEach(([domain, requests]) => {
            const path = `requests/${domain}`
            if (!fs.existsSync(path)) {
                fs.mkdirSync(path, {recursive: true})
            }

            requests.forEach(
                ({url, description, method, requestName, parameters}) => {
                    const filePath = `${path}/${requestName}.ts`;
                    if (!fs.existsSync(filePath)) {

                        const params = {};

                        (parameters || []).forEach(
                            ({
                                 name,
                                 in: type,
                                 description: paramDescription,
                                 required,
                                 allowEmptyValue,
                                 type: argType,
                             }) => {
                                if (!(type in params)) {
                                    params[type] = []
                                }

                                params[type].push({
                                    name,
                                    paramDescription,
                                    allowEmpty: !!allowEmptyValue,
                                    isRequired: !!required,
                                    argType,
                                })
                            }
                        )

                        const onlyPath = (
                            Object.keys(params).length === 1  && Object.keys(params)[0] === 'path'
                        )

                        if ('path' in params) {
                            url = url.replace(
                                /{(.*)}/g,
                                `\${${onlyPath
                                    ? ''
                                    : 'path.'}$1}`
                            )
                        }

                        const args = onlyPath
                            ? params
                                [Object.keys(params)[0]]
                                .sort(({isRequired: a}, {isRequired: b}) => (a ? 1 : 0) - (b ? 1 : 0))
                                .map(({name, paramDescription, argType, isRequired}) =>
                                    `    // ${paramDescription}
    ${name}${!isRequired ? '?:' : ':'} ${convertTypeToTypescriptType(argType)},`
                                ).join('\n')
                            : Object.entries(params).map(([category, params]) =>
                                `   ${category}: {
${params.map(({name, argType, isRequired, paramDescription}) =>
                                    `       // ${paramDescription}
        ${name}${!isRequired ? '?:' : ':'} ${convertTypeToTypescriptType(argType)},`).join('\n')}
    }${params.filter(({isRequired}) => isRequired).length ? '' : ' = {}' },`
                            ).join('\n\n')

                        const axiosArguments = [
                            `\`${`/${namespace}${url}`}\``,
                            'body' in params && 'body',
                            'query' in params && `{ params: query }`
                            // 'query' in params && `{ params: ${params.query.map(({name}) => name).join(',\n')} }`
                        ].filter(Boolean).join(', ')

                        fs.writeFileSync(filePath,
                            `import {${method}Request} from '@/services/api/api';

// ${description}
export default async (
${args}
) => await ${method}Request(${axiosArguments});
`)
                    }
                }
            )
        })
    })

const convertKebabToCamelCase = (key) =>
    key
        .split('-')
        .map(
            (item, index) => index > 0
                ? item[0].toUpperCase() + item.substr(1)
                : item
        )
        .join('')

const convertTypeToTypescriptType = (type) => ({
    "string": 'string',
    "integer": 'number'
}[type] || 'any')