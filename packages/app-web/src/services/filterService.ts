import {IConfiguredApplyingFilter} from '@/containers/helpers/FilterModal';
import {
    IComputedFilter,
    IFilterType, OPERATOR_BETWEEN, OPERATOR_BOOLEAN,
    OPERATOR_EQUAL, OPERATOR_IN, OPERATOR_LESS,
    OPERATOR_LIKE, OPERATOR_MORE,
    OPERATOR_NOT_EQUAL,
} from '@/interfaces/filterInterfaces';
import {INPUT_TYPE_NUMBER} from '@cip/core/src/constants/form/input/formInputTypesConstants';

export const escapeFilterValue = (value) => {
    return String(value).replace(/(["])/g, '\\$1');
};

export const buildRSQLFromFilterBuilder = ({
    operator,
    value,
    config: {
        key,
        ...config
    },
}: IConfiguredApplyingFilter) => {
    if (!operator) {
        return;
    }
    if (
        [OPERATOR_EQUAL, OPERATOR_NOT_EQUAL].indexOf(operator) > -1
    ) {
        const val =
            config.fieldConfig.options.field.type === INPUT_TYPE_NUMBER
                ? value
                : `"${escapeFilterValue(value)}"`;
        if (operator === OPERATOR_EQUAL) {
            return `${key}==${val}`;
        }
        if (operator === OPERATOR_NOT_EQUAL) {
            return `${key}!=${val}`;
        }
    }

    if (operator === OPERATOR_LIKE) {
        return `${key}=="*${escapeFilterValue(value)}*"`;
    }

    if (operator === OPERATOR_BETWEEN) {
        const val = `"${escapeFilterValue(value)}"`;
        return `${key} > ${val[0]} and ${key} < ${val[1]}`;
    }

    if (operator === OPERATOR_BOOLEAN) {
        return `${key}=${!!value ? 1 : 0}`;
    }

    if (operator === OPERATOR_LESS) {
        return `${key} < "${escapeFilterValue(value)}"`;
    }

    if (operator === OPERATOR_MORE) {
        return `${key} > "${escapeFilterValue(value)}"`;
    }

    if (operator === OPERATOR_IN) {
        return `${key} =in= ${value.map(escapeFilterValue).join(',')}`;
    }
};

export const buildRSQLFromInlineFilter = ({
    key,
    value,
    config,
}: {
    key: string,
    value: any,
    config?: IFilterType,
}) => {
    return (!!config && config.options.filterString) ? (
        config.options.filterString(value)
    ) : `${key}=="*${escapeFilterValue(value)}*"`;
};
