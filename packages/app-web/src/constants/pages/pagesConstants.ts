export const REGISTRY_PAGE_INDEX = 'registry/index';

export const APPEALS_PAGE_CREATE = 'appeals/create';
export const APPEALS_PAGE_LIST = 'appeals/list';

export const REQUESTS_PAGE_LIST = 'requests/list';
