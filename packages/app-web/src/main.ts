import Vue from 'vue';
import App from './containers/app/App';
import router from './router';
import store from './store';
import './registerServiceWorker';
import i18n from './i18n';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
} as any).$mount('#app');
