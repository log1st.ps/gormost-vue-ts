import {VueComponent} from 'vue-tsx-helper';
import {Component, Prop} from 'vue-property-decorator';

@Component
class EGIPMap extends VueComponent<{
    height?: number;
}> {

    // @ts-ignore
    public $refs: {
        map: HTMLDivElement,
    };

    @Prop()
    public height?: number;

    protected mapInstance = null;

    // @ts-ignore
    protected egip: any = null;

    public render() {
        return (
            <div
                style={{
                    height: `${this.height}px`,
                }}
                ref={'map'}
            >

            </div>
        );
    }

    public async created(): Promise<void> {
        await this.loadScripts();
        await this.initMap();
    }

    protected async loadScripts() {
        const scripts = [
            'http://egiptest.mos.ru/jsapi/lib/ol-5.2.0.css',
            'http://egiptest.mos.ru/jsapi/lib/ol-ext-3.0.1.css',
            'http://egiptest.mos.ru/jsapi/lib/proj4-2.4.4.js',
            'http://egiptest.mos.ru/jsapi/lib/ol-5.2.0.js',
            'http://egiptest.mos.ru/jsapi/lib/ol-ext-3.0.1.js',
            'http://egiptest.mos.ru/jsapi/dist/egip.js',
        ];

        await scripts.reduce(
            (lastPromise: Promise<any>, url: string, index: number) => lastPromise.then(
                () => new Promise((resolve) => {
                    const isCss = url.match(/.*\.css/);
                    const node = document.createElement(isCss ? 'link' : 'script');

                    node.addEventListener('load', resolve);

                    if (node instanceof HTMLLinkElement) {
                        node.rel = 'stylesheet';
                        node.type = 'text/css';
                        node.href = url;
                    }   else {
                        node.src = url;
                    }

                    document.head.appendChild(node);
                }),
            ),
            Promise.resolve(),
        );

        // @ts-ignore
        this.egip = egip;

        // @ts-ignore
        egip.setConfig({
            baseURL: 'http://egiptest.mos.ru/',
            withCredentials: true,
        });

        await this.login();
    }

    protected async login() {
        await this.egip.api.login({
            login: 'user_api',
            password: '-5V2wJO"!"X4',
        });
    }

    protected async initMap() {
        this.mapInstance = this.egip.layers.createMap({
            target: this.$refs.map,
            layers: [
                this.egip.layers.createTiles2GIS(),
            ],
            view: this.egip.layers.createViewWGS({
                zoom: 12,
            }),
        });
    }
}

export default EGIPMap;
