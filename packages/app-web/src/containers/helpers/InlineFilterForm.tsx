import {VueComponent} from 'vue-tsx-helper';
import {Component, Emit, InjectReactive} from 'vue-property-decorator';
import Row from '@cip/gormost-core-web/src/components/grid/Row';
import Column from '@cip/gormost-core-web/src/components/grid/Column';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import {ICON_TYPE_FILTER} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import {
    SecondaryTypographyComponent,
    TertiaryTypographyComponent,
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import Relative from '@cip/gormost-core-web/src/components/grid/Relative';
import CardComponent from '@cip/gormost-core-web/src/components/card/CardComponent';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import {
    BUTTON_STATE_FONT_BOLD,
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_TERTIARY,
} from '@cip/core/src/constants/button/buttonStatesConstants';
import {BUTTON_SIZE_XS} from '@cip/core/src/constants/button/buttonSizesConstants';
import {ACTION_LIST_DIRECTION_COLUMN} from '@cip/core/src/constants/actionsList/actionsListDirectionContstants';
import {ROW_ALIGN_END, ROW_ALIGN_START} from '@cip/core/src/constants/grid/rowAlignsConstants';
import {
    FIELD_TYPE_CHECKBOX,
} from '@cip/core/src/mixins/components/form/elements/checkbox/CheckboxFormElementMixin';
import {
    FIELD_TYPE_DATEPICKER,
} from '@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin';
import {
    FIELD_TYPE_INPUT,
} from '@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin';
import CheckboxFormElement from '@cip/gormost-core-web/src/components/form/elements/checkbox/CheckboxFormElement';
import DatepickerFormElement from '@cip/gormost-core-web/src/components/form/elements/datepicker/DatepickerFormElement';
import FieldFormElement from '@cip/gormost-core-web/src/components/form/elements/field/FieldFormElement';
import Grid from '@cip/gormost-core-web/src/components/grid/Grid';
import InputFormElement from '@cip/gormost-core-web/src/components/form/elements/input/InputFormElement';
import {IComputedFilter, IFilterType} from '@/interfaces/filterInterfaces';
import {FIELD_TYPE_SELECT} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin';
import SelectFormElement from '@cip/gormost-core-web/src/components/form/elements/select/SelectFormElement';
import {FIELD_TYPE_FILE} from '@cip/core/src/mixins/components/form/elements/file/FileFormElementMixin';

@Component
class InlineFilterForm extends VueComponent<{
    onUpdateFilter?({key, value}: {key: string, value: any}): void,
    onClear?(): void,
    onApply?(): void,
}> {

    get computedFields(): IComputedFilter[] {
        return this.fields ? this.fields.map(({type, ...field}) => ({
            ...field,
            component: {
                [FIELD_TYPE_CHECKBOX]: CheckboxFormElement,
                [FIELD_TYPE_DATEPICKER]: DatepickerFormElement,
                [FIELD_TYPE_INPUT]: InputFormElement,
                [FIELD_TYPE_SELECT]: SelectFormElement,
                [FIELD_TYPE_FILE]: undefined,
            }[type],
            type,
        })) : [];
    }

    @InjectReactive('filterFields')
    public fields?: IFilterType[];

    public render() {
        return (
            <CardComponent>
                <Row align={ROW_ALIGN_START} gap={24}>
                    <Column gap={24}>
                        <Relative offset={[2, 0, 0, 0]}>
                            <Row gap={12}>
                                <Column gap={12}>
                                    <IconElement type={ICON_TYPE_FILTER} height={12} width={18}/>
                                </Column>
                                <Column gap={12} isGrow>
                                    <TertiaryTypographyComponent text={this.$t('filter.title') as string}/>
                                </Column>
                            </Row>
                        </Relative>
                    </Column>
                    <Column gap={24} isGrow isShrink>
                        <Grid
                            columns={this.computedFields.length > 4 ? 4 : this.computedFields.length}
                            gap={9}
                        >
                            {this.computedFields.map((field) => (
                                field.options.meta ? (
                                    <FieldFormElement {...{attrs: field.options.meta}}>
                                        <field.component
                                            {...{attrs: field.options.field}}
                                            onInput={this.updateModel(field)}
                                        />
                                    </FieldFormElement>
                                ) : (
                                    <Column align={ROW_ALIGN_END}>
                                        <field.component
                                            {...{attrs: field.options.field}}
                                            onInput={this.updateModel(field)}
                                        />
                                    </Column>
                                )
                            ))}
                        </Grid>
                    </Column>
                    <Column gap={24} isShrink={false} align={ROW_ALIGN_END}>
                        <ActionsListComponent
                            gap={23}
                            direction={ACTION_LIST_DIRECTION_COLUMN}
                            actions={[
                                {
                                    key: 'clear',
                                    state: [BUTTON_STATE_TERTIARY, BUTTON_STATE_FONT_BOLD],
                                    text: this.$t('filter.clearAll') as string,
                                    size: BUTTON_SIZE_XS,
                                    isBlock: true,
                                    onClick: () => {
                                        this.onClearButtonClick();
                                    },
                                },
                                {
                                    key: 'apply',
                                    state: BUTTON_STATE_PRIMARY,
                                    text: this.$t('filter.apply') as string,
                                    size: BUTTON_SIZE_XS,
                                    isBlock: true,
                                    onClick: () => {
                                        this.onApplyButtonClick();
                                    },
                                },
                            ]}
                        />
                    </Column>
                </Row>
            </CardComponent>
        );
    }

    @Emit('clear')
    protected onClearButtonClick() {
        //
    }

    @Emit('apply')
    protected onApplyButtonClick() {
        //
    }

    protected updateModel(filter: IComputedFilter) {
        return async (value: any) => {
            filter.options.field.value = value;
            if ('onUpdate' in filter && filter.onUpdate) {
                filter.onUpdate(filter.key, value);
            }
        };
    }
}

export default InlineFilterForm;
