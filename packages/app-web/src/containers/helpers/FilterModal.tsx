import {VueComponent} from 'vue-tsx-helper';
import {Component, Emit, InjectReactive, Prop} from 'vue-property-decorator';
import ModalComponent from '@cip/gormost-core-web/src/components/modal/ModalComponent';
import DialogComponent from '@cip/gormost-core-web/src/components/dialog/DialogComponent';
import {
    availableOperators,
    IComputedFilter,
    IFilterType,
    OPERATOR_BETWEEN, OPERATOR_IN,
    operatorsByFieldType,
} from '@/interfaces/filterInterfaces';
import {
    SecondaryTypographyComponent,
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import Row from '@cip/gormost-core-web/src/components/grid/Row';
import Column from '@cip/gormost-core-web/src/components/grid/Column';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import {ICON_TYPE_CLOSE} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import {BUTTON_SIZE_XS} from '@cip/core/src/constants/button/buttonSizesConstants';
import Grid from '@cip/gormost-core-web/src/components/grid/Grid';
import {ICON_TYPE_PLUS} from '@cip/core/src/constants/icon/iconTypesConstants';
import {
    ACTIONS_LIST_ALIGN_END,
    ACTIONS_LIST_ALIGN_START,
} from '@cip/core/src/constants/actionsList/actionsListAlignsConstants';
import {
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_QUINARY,
    BUTTON_STATE_TERTIARY,
} from '@cip/core/src/constants/button/buttonStatesConstants';
import {ROW_ALIGN_START} from '@cip/core/src/constants/grid/rowAlignsConstants';
import IconElement from '@cip/gormost-core-web/src/elements/icon/IconElement';
import Relative from '@cip/gormost-core-web/src/components/grid/Relative';
import {FIELD_TYPE_CHECKBOX} from '@cip/core/src/mixins/components/form/elements/checkbox/CheckboxFormElementMixin';
import CheckboxFormElement from '@cip/gormost-core-web/src/components/form/elements/checkbox/CheckboxFormElement';
import {
    FIELD_TYPE_DATEPICKER,
} from '@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin';
import DatepickerFormElement from '@cip/gormost-core-web/src/components/form/elements/datepicker/DatepickerFormElement';
import {FIELD_TYPE_INPUT} from '@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin';
import InputFormElement from '@cip/gormost-core-web/src/components/form/elements/input/InputFormElement';
import {
    FIELD_TYPE_SELECT,
} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin';
import {
    ISelectOption,
} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin.d';
import SelectFormElement from '@cip/gormost-core-web/src/components/form/elements/select/SelectFormElement';
import {FIELD_TYPE_FILE} from '@cip/core/src/mixins/components/form/elements/file/FileFormElementMixin';
import Fragment from 'vue-fragment/src/component';

interface IFilterConfig {
    availableOperators: availableOperators[];
    fieldConfig: IComputedFilter;
    key: string;
}

export interface IApplyingFilter {
    key: number | null;
    operator: availableOperators | null;
    value: any;
}

export interface IConfiguredApplyingFilter extends IApplyingFilter {
    config: IFilterConfig;
}

@Component
class FilterModal extends VueComponent<{
    isActive?: boolean,
    onClose?(): void;
    onApply?(): void;
}> {

    get computedFields(): IComputedFilter[] {
        return this.filters ? this.filters.map(({type, ...field}) => ({
            ...field,
            component: {
                [FIELD_TYPE_CHECKBOX]: CheckboxFormElement,
                [FIELD_TYPE_DATEPICKER]: DatepickerFormElement,
                [FIELD_TYPE_INPUT]: InputFormElement,
                [FIELD_TYPE_SELECT]: SelectFormElement,
                [FIELD_TYPE_FILE]: undefined,
            }[type],
            type,
        })) : [];
    }

    get availableFields(): ISelectOption[] {
        return (this.filters || []).map((field, index) => ({
            key: index,
            text:
                (field.options.meta || {}).title
                || (field.options.field || {}).placeholder,
        })).filter(({text}) => !!text);
    }

    get filterConfigs(): {
        [key: string]: IFilterConfig,
    } {
        return this.computedFields
            .reduce<{ [key: string]: IFilterConfig }>(
                (currentValue, {type, options, key, ...other}, index) => ({
                    ...currentValue,
                    [index]: {
                        availableOperators: operatorsByFieldType[type]
                            .filter((operator) => [OPERATOR_BETWEEN, OPERATOR_IN].indexOf(operator) === -1)
                            .map((operator) => ({
                                key: operator,
                                text: this.$t(`filter.operators.${operator}`),
                            })),
                        fieldConfig: {
                            type,
                            key,
                            options,
                            ...other,
                        },
                        key,
                    },
                }),
                {},
            );
    }

    @Prop()
    public isActive?: boolean;

    @InjectReactive('filterFields')
    protected filters?: IFilterType[];

    @InjectReactive('filterTitle')
    protected title?: string;

    @InjectReactive('filterBuilderModel')
    // @ts-ignore
    protected filterBuilderModel: IConfiguredApplyingFilter[];

    public render() {
        return (
            <ModalComponent
                value={this.isActive}
                onInput={this.close}
                isBackdropClosable
                maxWidth={860}
            >
                <DialogComponent
                    onClose={this.close}
                    scopedSlots={{
                        header: () => (
                            <SecondaryTypographyComponent text={this.title}/>
                        ),
                    }}
                >
                    <Grid gap={16}>
                        <Row gap={16} align={ROW_ALIGN_START}>
                            <Column gap={16} isGrow={false} isShrink={false}>
                                <Relative offset={[8, 0, 0, 0]}>
                                    {this.$t('filter.fieldTitle')}
                                </Relative>
                            </Column>
                            <Column gap={16} isGrow isShrink>
                                <Grid gap={16}>
                                    {this.renderModel()}
                                    <Relative offset={[8, 0, 0, 0]}>
                                        <ActionsListComponent
                                            align={ACTIONS_LIST_ALIGN_START}
                                            actions={[{
                                                key: 'add',
                                                leftIcon: () => (
                                                    <IconElement
                                                        type={ICON_TYPE_PLUS}
                                                        height={14}
                                                        width={14}
                                                    />
                                                ),
                                                state: BUTTON_STATE_QUINARY,
                                                text: this.$t('filter.add') as string,
                                                onClick: () => {
                                                    this.addFilter();
                                                },
                                            }]}
                                        />
                                    </Relative>
                                </Grid>
                            </Column>
                        </Row>
                        <ActionsListComponent
                            gap={16}
                            align={ACTIONS_LIST_ALIGN_END}
                            actions={[
                                {
                                    key: 'clear',
                                    size: BUTTON_SIZE_XS,
                                    state: BUTTON_STATE_TERTIARY,
                                    text: this.$t('filter.clearAll') as string,
                                    onClick: () => {
                                        this.clearModel();
                                    },
                                },
                                {
                                    key: 'apply',
                                    size: BUTTON_SIZE_XS,
                                    state: BUTTON_STATE_PRIMARY,
                                    text: this.$t('filter.apply') as string,
                                    onClick: () => {
                                        this.applyFilter();
                                    },
                                },
                            ]}
                        />
                    </Grid>
                </DialogComponent>
            </ModalComponent>
        );
    }

    protected clearModel() {
        this.filterBuilderModel.splice(0);
        this.applyFilter();
    }

    @Emit('apply')
    protected applyFilter() {
        // something
    }

    protected renderModel() {
        return (
            <Fragment>
                {this.filterBuilderModel.map((filter, index) => (
                    <Row gap={8}>
                        <Column isGrow gap={8}>
                            <Grid
                                columns={[5, 4, 6]}
                                gap={16}
                            >
                                <Column>
                                    <SelectFormElement
                                        placeholder={this.$t('filter.field') as string}
                                        options={this.availableFields}
                                        value={filter.key}
                                        onInput={this.updateFilterField(index)}
                                    />
                                </Column>
                                {(filter.config && filter.key !== null) && (
                                    <Fragment>
                                        <Column>
                                            <SelectFormElement
                                                placeholder={this.$t('filter.operator') as string}
                                                options={
                                                    filter.config.availableOperators
                                                }
                                                value={filter.operator}
                                                onInput={this.updateFilterOperator(index)}
                                            />
                                        </Column>
                                        {filter.operator && (
                                            <Column>
                                                <InputFormElement
                                                    placeholder={this.$t('filter.value') as string}
                                                    value={filter.value}
                                                    onInput={this.updateFilterValue(index)}
                                                />
                                            </Column>
                                        )}
                                    </Fragment>
                                )}
                            </Grid>
                        </Column>
                        <Column gap={8}>
                            <ActionsListComponent
                                actions={[{
                                    key: 'remove',
                                    leftIcon: () => (
                                        <IconElement
                                            type={ICON_TYPE_CLOSE}
                                            height={11}
                                            width={11}
                                        />
                                    ),
                                    isSquare: true,
                                    onClick: () => {
                                        this.removeFilter(index);
                                    },
                                }]}
                            />
                        </Column>
                    </Row>
                ))}
            </Fragment>
        );
    }

    protected updateFilterField(index) {
        return (value) => {
            this.filterBuilderModel[index].key = value;
            this.filterBuilderModel[index].operator = null;
            this.filterBuilderModel[index].value = null;
            this.filterBuilderModel[index].config = this.filterConfigs[value] || {};
        };
    }

    protected updateFilterOperator(index) {
        return (value) => {
            this.filterBuilderModel[index].operator = value;
        };
    }

    protected updateFilterValue(index) {
        return (value) => {
            this.filterBuilderModel[index].value = value;
        };
    }

    protected removeFilter(index) {
        this.filterBuilderModel.splice(index, 1);
    }

    protected addFilter() {
        this.filterBuilderModel.push({
            key: null,
            operator: null,
            value: null,
            // @ts-ignore
            config: {},
        });
    }

    @Emit('close')
    protected close() {
        // something
    }
}

export default FilterModal;
