import {VueComponent} from 'vue-tsx-helper';
import {Component, InjectReactive, Prop} from 'vue-property-decorator';
import {IActionsList} from '@cip/core/src/mixins/components/actionsList/ActionsListComponentMixin.d';
import APIProp, {PROP_TYPE_ARRAY} from '@cip/core/src/helpers/documentation/APIProp';
import APIContainer, {IContainerSlot} from '@cip/core/src/helpers/documentation/APIContainer';
import Row from '@cip/gormost-core-web/src/components/grid/Row';
import {ROW_ALIGN_CENTER} from '@cip/core/src/constants/grid/rowAlignsConstants';
import Column from '@cip/gormost-core-web/src/components/grid/Column';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import {ROW_JUSTIFY_END} from '@cip/core/src/constants/grid/rowJustifiesConstants';
import {
    PrimaryTypographyComponent, SecondaryTypographyComponent,
    TertiaryTypographyComponent
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';

interface IToolbar {
    title?: string;
    controls?: IActionsList;
}

@APIContainer({
    slots: [
        {
            name: 'title',
        },
        ...['tabs', 'actions'].map((name: string): IContainerSlot => ({
            name,
            params: {
                [name]: {
                    type: PROP_TYPE_ARRAY,
                    description: `List of buttons for ${name}`,
                },
            },
        })),
    ],
})
@Component
class Toolbar extends VueComponent<IToolbar> implements IToolbar {

    @Prop()
    public title ?: string;

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'ActionsList config for controls',
    })
    @Prop()
    public controls ?: IActionsList;

    @InjectReactive('toolbarTabs')
    protected tabs?: IActionsList;

    public render() {
        return (
            <Row align={ROW_ALIGN_CENTER}>
                <Column isShrink>
                    <Row gap={16} align={ROW_ALIGN_CENTER}>
                        <Column gap={16}>
                            <SecondaryTypographyComponent text={this.title}/>
                        </Column>
                        <Column gap={16} isGrow>
                            <ActionsListComponent {...{attrs: this.tabs} as any}/>
                        </Column>
                    </Row>
                </Column>
                <Column isGrow>
                    <Row justify={ROW_JUSTIFY_END}>
                        <ActionsListComponent {...{attrs: this.controls} as any}/>
                    </Row>
                </Column>
            </Row>
        );
    }
}

export default Toolbar;
