import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';
import HeaderComponent from '@cip/gormost-core-web/src/components/header/HeaderComponent';
import {IHeader} from '@cip/core/src/mixins/components/header/HeaderComponentMixin.d';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';

@APIContainer({

})
@Component
class Header extends VueComponent<{}> {
    get user(): IHeader['user'] {
        return this.$store.state.user;
    }

    public render() {
        return (
            // @ts-ignore
            <HeaderComponent
                onLogoClick={this.onLogoClick}
                logoPath={'/img/logo.png'}
                withNotifications
                user={this.user}
            />
        );
    }

    protected onLogoClick() {
        this.$router.push('/');
    }
}

export default Header;
