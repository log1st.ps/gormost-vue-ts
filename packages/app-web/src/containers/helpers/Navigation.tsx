import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';
import NavigationComponent from '@cip/gormost-core-web/src/components/navigation/NavigationComponent';
import {INavigationItem} from '@cip/core/src/mixins/components/navigation/NavigationComponentMixin.d';
import {
    ICON_TYPE_BUILDING,
    ICON_TYPE_TREE,
    ICON_TYPE_USER_SETTINGS,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';
import {REGISTRY_PAGE_INDEX} from '@/constants/pages/pagesConstants';
import {Route} from 'vue-router';

@APIContainer({

})
@Component
class Navigation extends VueComponent<{}> {

    get items(): INavigationItem[] {
        return [
            {
                key: REGISTRY_PAGE_INDEX,
                text: this.$t('navigation.requests') as string,
                isDisabled: false,
                icon: ICON_TYPE_BUILDING as any,
                route: {name: REGISTRY_PAGE_INDEX} as Route,
            },
            {
                key: 'facilities',
                text: this.$t('navigation.buildings') as string,
                isDisabled: true,
                icon: ICON_TYPE_BUILDING as any,
            },
            {
                key: 'nsi',
                text: this.$t('navigation.nsi') as string,
                isDisabled: true,
                icon: ICON_TYPE_TREE as any,
            },
            {
                key: 'administration',
                text: this.$t('navigation.administration') as string,
                isDisabled: true,
                icon: ICON_TYPE_USER_SETTINGS as any,
            },
            {
                key: 'analytics',
                text: this.$t('navigation.analytics') as string,
                isDisabled: true,
                icon: ICON_TYPE_USER_SETTINGS as any,
            },
        ];
    }

    protected value: string | null = null;

    public render() {
        return (
            <NavigationComponent items={this.items}/>
        );
    }
}

export default Navigation;
