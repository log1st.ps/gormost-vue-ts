import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';

@Component
class AppealAccordionPreview extends VueComponent<{}> {
    public render() {
        return (
            <div>
                {
                    // @ts-ignore
                    <router-view/>
                }
            </div>
        );
    }
}

export default AppealAccordionPreview;
