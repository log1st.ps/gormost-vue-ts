import {VueComponent} from 'vue-tsx-helper';
import {Component, Emit, Prop} from 'vue-property-decorator';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';
import Toolbar from '@/containers/helpers/Toolbar';
import {IActionsList} from '@cip/core/src/mixins/components/actionsList/ActionsListComponentMixin.d';
import {
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_TERTIARY,
} from '@cip/core/src/constants/button/buttonStatesConstants';
import {BUTTON_SIZE_XS} from '@cip/core/src/constants/button/buttonSizesConstants';
import {
    ICON_TYPE_DOWNLOAD,
    ICON_TYPE_GEAR,
    ICON_TYPE_MAGINIFIER,
    ICON_TYPE_PRINT,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import Grid from '@cip/gormost-core-web/src/components/grid/Grid';
import InlineFilterForm from '@/containers/helpers/InlineFilterForm';
import {IFilterType} from '@/interfaces/filterInterfaces';
import TableComponent from '@cip/gormost-core-web/src/components/table/TableComponent';
import {ITableColumn, ITableDataRow} from '@cip/core/src/mixins/components/table/TableComponentMixin.d';
import FilterModal from '@/containers/helpers/FilterModal';
import Absolute from '@cip/gormost-core-web/src/components/grid/Absolute';
import Fragment from 'vue-fragment/src/component';
import Styler from '@cip/gormost-core-web/src/components/styler/Styler';
import {COLOR_SEPTENARY_LIGHTER} from '@cip/core/src/constants/styler/availableStylerColorsConstants';

export interface ITableViewConfig {
    keyField: string;
    columns: ITableColumn[];
    data: ITableDataRow[];
}

@APIContainer({})
@Component
class TableView extends VueComponent<{
    title?: string;
    filterFields?: IFilterType[];
    tableConfig?: ITableViewConfig | null;
    onUpdateFilter?({key, value}: { key: string, value: any }): void,
    onInlineFilterClear?(): void,
    onInlineFilterApply?(): void,
    onFilterModalApply?(): void,
}> {

    get toolbarControls(): IActionsList {
        return {
            actions: [
                {
                    key: 'tableControl',
                    text: this.$t('table.controls.table') as string,
                    state: BUTTON_STATE_TERTIARY,
                    size: BUTTON_SIZE_XS,
                    leftIcon: ICON_TYPE_GEAR,
                    isDisabled: true,
                },
                {
                    key: 'export',
                    text: this.$t('table.controls.export') as string,
                    state: BUTTON_STATE_TERTIARY,
                    size: BUTTON_SIZE_XS,
                    leftIcon: ICON_TYPE_DOWNLOAD,
                    isDisabled: true,
                },
                {
                    key: 'print',
                    text: this.$t('table.controls.print') as string,
                    state: BUTTON_STATE_TERTIARY,
                    size: BUTTON_SIZE_XS,
                    leftIcon: ICON_TYPE_PRINT,
                    isDisabled: true,
                },
                {
                    key: 'search',
                    text: this.$t('table.controls.search') as string,
                    state: BUTTON_STATE_PRIMARY,
                    size: BUTTON_SIZE_XS,
                    leftIcon: ICON_TYPE_MAGINIFIER,
                    onClick: () => {
                        this.showFilterModal();
                    },
                },
            ],
            gap: 16,
        };
    }

    @Prop()
    public title?: string;

    @Prop()
    public filterFields?: IFilterType[];

    @Prop()
    public tableConfig?: ITableViewConfig;

    // @ts-ignore
    public $refs: {
        anchor: HTMLElement,
    };

    protected isFilterModalActive: boolean = false;

    public render() {
        return (
            <Fragment>
                <Absolute
                    isSticky
                    offset={[0, null, null]}
                    zIndex={1}
                >
                    <Styler background={COLOR_SEPTENARY_LIGHTER}>
                        <Styler height={16}/>
                        <Grid gap={12}>
                            <Toolbar
                                title={this.title}
                                controls={this.toolbarControls}
                            />
                            <InlineFilterForm
                                onClear={this.onInlineFilterClear}
                                onApply={this.onInlineFilterApply}
                            />
                        </Grid>
                    </Styler>
                </Absolute>
                <FilterModal
                    isActive={this.isFilterModalActive}
                    onClose={this.closeFilterModal}
                    onApply={this.onFilterModelApply}
                />
                {
                    // @ts-ignore
                    <TableComponent
                        isVerticallyStriped
                        isBordered
                        isCondensed
                        {...{attrs: this.tableConfig}}
                    />
                }
            </Fragment>
        );
    }

    @Emit('inlineFilterClear')
    protected onInlineFilterClear() {
        //
    }

    @Emit('inlineFilterApply')
    protected onInlineFilterApply() {
        //
    }

    @Emit('filterModalApply')
    protected onFilterModelApply() {
        //
    }

    protected showFilterModal() {
        this.isFilterModalActive = true;
    }

    protected closeFilterModal() {
        this.isFilterModalActive = false;
    }
}

export default TableView;
