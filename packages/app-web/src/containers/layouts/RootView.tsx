import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';

@APIContainer({

})
@Component
class RootView extends VueComponent<{}> {
    public render() {
        return (
            <div id={'root-view'}>
                {
                    // @ts-ignore
                    (<router-view/>)
                }
            </div>
        );
    }
}

export default RootView;
