import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';

@APIContainer({

})
@Component
class RequestsView extends VueComponent<{}> {
    public render() {
        return (
            <div id={'requests-view'}>
                {
                    // @ts-ignore
                    (<router-view/>)
                }
            </div>
        );
    }
}

export default RequestsView;
