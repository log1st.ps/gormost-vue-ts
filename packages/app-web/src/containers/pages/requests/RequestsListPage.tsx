import {Component, Watch} from 'vue-property-decorator';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';
import AbstractListPage from '@/containers/abstract/AbstractListPage';
import {IInlineFilterConfig} from '@/interfaces/filterInterfaces';
import {BUTTON_STATE_PRIMARY} from '@cip/core/src/constants/button/buttonStatesConstants';
import {BUTTON_SIZE_SM} from '@cip/core/src/constants/button/buttonSizesConstants';
import {ICON_TYPE_PLUS} from '@cip/core/src/constants/icon/iconTypesConstants';
import {BUTTON_TYPE_LINK} from '@cip/core/src/constants/button/buttonTypesConstants';
import {FIELD_TYPE_INPUT} from '@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin';
import {
    FIELD_TYPE_SELECT,
} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin';
import {
    ISelectOption,
} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin.d';
import {
    FIELD_TYPE_DATEPICKER,
} from '@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin';
import {escapeFilterValue} from '@/services/filterService';
import RequestsModel from '@/interfaces/models/RequestsModel';
import {
    ACTION_FETCH_REQUESTS_LIST,
    GETTER_REQUESTS_STATUSES_LIST,
    MODULE_REQUESTS,
} from '@/store/modules/requests/requestsStoreConstants';
import RequestStatusesModel from '@/interfaces/models/RequestStatusesModel';

@APIContainer({

})
@Component
class RequestsListPage
    extends AbstractListPage {

    get availableStatuses(): ISelectOption[] {
        return this.$store.getters[`${MODULE_REQUESTS}/${GETTER_REQUESTS_STATUSES_LIST}`]
            .map((request: RequestStatusesModel) => ({
                key: request.requestStatusId,
                text: request.requestStatusName,
            }));
    }

    get availableStatusesMap(): {[key: string]: string} {
        return this.availableStatuses.reduce((currentValue, {key, text}) => ({
            ...currentValue,
            [key]: text,
        }), {});
    }


    protected filterModel: IInlineFilterConfig['model'] = {
        requestNumber: null,
        requestStatusByStatusId: null,
        startDateFrom: null,
        startDateTo: null,
    };

    protected data: RequestsModel[] = [];

    public created(): void {
        this.primaryActions = {
            actions: [{
                key: 'create',
                state: BUTTON_STATE_PRIMARY,
                size: BUTTON_SIZE_SM,
                isRounded: true,
                leftIcon: ICON_TYPE_PLUS,
                text: 'Операции',
                type: BUTTON_TYPE_LINK,
            }],
        };

        this.title = this.$t('registry.title') as string;
        this.filterTitle = this.$t('requests.page.list.search') as string;
        this.pageId = 'requests-list-page';

        this.fetchRequestsList();
    }

    @Watch('availableStatuses', {
        immediate: true,
    })
    public async setup() {
        this.tableConfig = {
            keyField: 'requestId',
            columns: [
                {
                    key: 'appealId',
                    title: 'Обращение',
                    subTitle: 'Дата подачи',
                },
                {
                    key: 'requestNumber',
                    title: 'Заявление',
                    subTitle: '№ и дата действия разрешения',
                },
                {
                    key: 'applicantId',
                    title: 'Заявитель',
                },
                {
                    key: 'routeId',
                    title: 'Маршрут',
                },
                {
                    key: '_contractId',
                    title: 'Договор',
                    subTitle: '№ и Дата',
                },
                {
                    key: '_contractValue',
                    title: this.$t('appeals.field._contractValue') as string,
                },
                {
                    key: 'requestStatusByStatusId',
                    title: 'Статус заявления',
                },
            ],
            data: [],
        };

        this.filtersConfig = ([
            {
                key: 'requestNumber',
                type: FIELD_TYPE_INPUT,
                options: {
                    meta: {
                        title: 'Номер заявления',
                    },
                    filterString(value: any): string {
                        return `requestNumber == "*${escapeFilterValue(value)}*"`;
                    },
                },
            },
            {
                key: 'requestStatusByStatusId',
                type: FIELD_TYPE_SELECT,
                options: {
                    meta: {
                        title: 'Статус заявления',
                    },
                    field: {
                        options: this.availableStatuses,
                        isMultiple: true,
                    },
                    filterString(value: any): string {
                        return `requestStatusByStatusId.requestStatusId =in= (${value.join(',')})`;
                    },
                },
            },
            {
                key: 'startDateFrom',
                type: FIELD_TYPE_DATEPICKER,
                options: {
                    meta: {
                        title: 'Дата разрешения',
                    },
                    field: {
                        format: 'd.m.Y',
                        before: this.$t('fields.datepicker.from') as string,
                    },
                    filterString(value: any): string {
                        return `startDate >= "${escapeFilterValue(value)}T00:00:00"`;
                    },
                },
            },
            {
                key: 'startDateTo',
                type: FIELD_TYPE_DATEPICKER,
                options: {
                    field: {
                        format: 'd.m.Y',
                        before: this.$t('fields.datepicker.to') as string,
                    },
                    filterString(value: any): string {
                        return `startDate <= "${escapeFilterValue(value)}T00:00:00"`;
                    },
                },
            },
        ] as IInlineFilterConfig['filters']).map((filter) => ({
            ...filter,
            options: {
                ...filter.options,
                field: {
                    ...(filter.options.field || {}),
                    isClearable: true,
                },
            },
        }));

        this.buildData();
    }

    protected async fetchRequestsList() {
        this.data = await this.$store.dispatch(
            `${MODULE_REQUESTS}/${ACTION_FETCH_REQUESTS_LIST}`,
            this.computedPayload,
        );

        this.buildData();
    }

    protected buildData() {
        if (!this.tableConfig) {
            return;
        }

        this.tableConfig.data = this.data.map((
            request: (RequestsModel & {
                _requestsLength?: number;
                _requestsIds?: string;
                _contractId?: number;
                _contractDate?: string;
                _contractValue?: string;
            }),
        ) => ({
            ...request,
            requestNumber: {
                value: request.requestNumber,
                hint: [request.startDate, request.endDate].filter(Boolean).join(' - '),
            },
            requestStatusByStatusId:
                request.requestStatusByStatusId ?
                    (this.availableStatusesMap[request.requestStatusByStatusId.requestStatusId] || '')
                    : '',
        }));
    }

    protected applyInlineFilter() {
        this.fetchRequestsList();
    }

    protected applyFilterModal() {
        this.fetchRequestsList();
    }
}

export default RequestsListPage;
