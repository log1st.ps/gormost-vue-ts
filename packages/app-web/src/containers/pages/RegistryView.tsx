import {VueComponent} from 'vue-tsx-helper';
import {Component, ProvideReactive, Watch} from 'vue-property-decorator';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';
import {BUTTON_SIZE_SM, BUTTON_SIZE_XS} from '@cip/core/src/constants/button/buttonSizesConstants';
import {
    BUTTON_STATE_FONT_REGULAR,
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_SECONDARY, BUTTON_STATE_TERTIARY,
} from '@cip/core/src/constants/button/buttonStatesConstants';
import {IActionsList} from '@cip/core/src/mixins/components/actionsList/ActionsListComponentMixin.d';
import {APPEALS_PAGE_LIST, REGISTRY_PAGE_INDEX, REQUESTS_PAGE_LIST} from '@/constants/pages/pagesConstants';
import {BUTTON_TYPE_LINK} from '@cip/core/src/constants/button/buttonTypesConstants';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';

@APIContainer({

})
@Component
class RegistryView extends VueComponent<{}> {
    get currentRoute(): string | undefined {
        return this.$route ? this.$route.name : undefined;
    }

    @ProvideReactive('toolbarTabs')
    protected toolbarTabs: IActionsList | null = null;

    @Watch('$route', {
        immediate: true,
    })
    public async handleRoute() {
        await waitForTicks(this);

        if (this.$route.name === REGISTRY_PAGE_INDEX) {
            this.$router.push({name: APPEALS_PAGE_LIST});
        }
    }

    public render() {
        return (
            <div id={'registry-view'}>
                {
                    // @ts-ignore
                    (<router-view/>)
                }
            </div>
        );
    }

    @Watch('$route', {
        immediate: true,
    })
    protected async computeToolbarTabs() {
        await waitForTicks(this);
        this.toolbarTabs = {
            actions: [
                {
                    key: 'appeals/list',
                    text: this.$t('registry.tabs.appeals') as string,
                    size: BUTTON_SIZE_XS,
                    type: BUTTON_TYPE_LINK as any,
                    url: {name: APPEALS_PAGE_LIST},
                },
                {
                    key: 'requests/list',
                    text: this.$t('registry.tabs.requests') as string,
                    size: BUTTON_SIZE_XS,
                    type: BUTTON_TYPE_LINK as any,
                    url: {name: REQUESTS_PAGE_LIST},
                },
            ].map((action) => ({
                ...action,
                state: this.currentRoute === action.key
                    ? BUTTON_STATE_PRIMARY
                    : [BUTTON_STATE_TERTIARY, BUTTON_STATE_FONT_REGULAR],
            })),
        };
    }
}

export default RegistryView;
