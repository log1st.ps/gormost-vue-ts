import {Component, Watch} from 'vue-property-decorator';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';
import AbstractListPage from '@/containers/abstract/AbstractListPage';
import {IInlineFilterConfig} from '@/interfaces/filterInterfaces';
import {FIELD_TYPE_INPUT} from '@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin';
import {FIELD_TYPE_SELECT} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin';
import {ISelectOption} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin.d';
import {FIELD_TYPE_DATEPICKER} from '@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin';
import {BUTTON_STATE_PRIMARY} from '@cip/core/src/constants/button/buttonStatesConstants';
import {BUTTON_SIZE_SM} from '@cip/core/src/constants/button/buttonSizesConstants';
import {ICON_TYPE_PLUS} from '@cip/core/src/constants/icon/iconTypesConstants';
import {BUTTON_TYPE_LINK} from '@cip/core/src/constants/button/buttonTypesConstants';
import {APPEALS_PAGE_CREATE} from '@/constants/pages/pagesConstants';
import {GETTER_ROUTES_LIST, MODULE_ROUTES} from '@/store/modules/routes/routesStoreConstants';
import RouteModel from '@/interfaces/models/RoutesModel';
import AppealsModel from '@/interfaces/models/AppealsModel';
import {
    ACTION_FETCH_APPEALS_LIST,
    GETTER_APPEAL_STATUSES_LIST,
    GETTER_APPLICANTS_LIST,
    GETTER_EMPLOYEES_LIST,
    MODULE_APPEALS,
} from '@/store/modules/appeals/appealsStoreConstants';
import {escapeFilterValue} from '@/services/filterService';
import RefAppealStatusesModel from '@/interfaces/models/RefAppealStatusesModel';
import ApplicantsModel from '@/interfaces/models/ApplicantsModel';
import EmployeesModel from '@/interfaces/models/EmployeesModel';
import {INPUT_TYPE_NUMBER} from '@cip/core/src/constants/form/input/formInputTypesConstants';

@APIContainer({})
@Component
class AppealsListPage
    extends AbstractListPage {

    get availableRoutes(): ISelectOption[] {
        return this.$store.getters[`${MODULE_ROUTES}/${GETTER_ROUTES_LIST}`]
            .map((route: RouteModel) => ({
                key: route.routeId,
                text: route.routeDescription,
            }));
    }

    get availableRoutesMap(): { [key: string]: string } {
        return this.availableRoutes.reduce((currentValue, {key, text}) => ({
            ...currentValue,
            [key]: text,
        }), {});
    }

    get availableStatuses(): ISelectOption[] {
        return this.$store.getters[`${MODULE_APPEALS}/${GETTER_APPEAL_STATUSES_LIST}`]
            .map((appealStatus: RefAppealStatusesModel) => ({
                key: appealStatus.appealStatusId,
                text: appealStatus.appealStatusName,
            }));
    }

    get availableStatusesMap(): { [key: string]: string } {
        return this.availableStatuses.reduce((currentValue, {key, text}) => ({
            ...currentValue,
            [key]: text,
        }), {});
    }

    get availableApplicants(): ISelectOption[] {
        return this.$store.getters[`${MODULE_APPEALS}/${GETTER_APPLICANTS_LIST}`]
            .map((appealStatus: ApplicantsModel) => ({
                key: appealStatus.applicantId,
                text: appealStatus.fullName,
            }));
    }

    get availableApplicantsMap(): { [key: string]: string } {
        return this.availableApplicants.reduce((currentValue, {key, text}) => ({
            ...currentValue,
            [key]: text,
        }), {});
    }

    get availableEmployees(): ISelectOption[] {
        return this.$store.getters[`${MODULE_APPEALS}/${GETTER_EMPLOYEES_LIST}`]
            .map((employee: EmployeesModel) => ({
                key: employee.employeeId,
                text: employee.fio,
            }));
    }

    protected filterModel: IInlineFilterConfig['model'] = {
        appealId: null,
        appealNumber: null,
        routeId: null,
        appealStatusId: null,
        applicationDateFrom: null,
        applicationDateTo: null,
        applicantId: null,
        executorId: null,
    };

    protected data: AppealsModel[] = [];

    public created(): void {
        this.primaryActions = {
            actions: [{
                key: 'create',
                state: BUTTON_STATE_PRIMARY,
                size: BUTTON_SIZE_SM,
                isRounded: true,
                leftIcon: ICON_TYPE_PLUS,
                text: this.$t('appeals.page.list.add') as string,
                type: BUTTON_TYPE_LINK,
                url: {name: APPEALS_PAGE_CREATE},
            }],
        };

        this.title = this.$t('registry.title') as string;
        this.filterTitle = this.$t('appeals.page.list.search') as string;
        this.pageId = 'appeals-list-page';

        this.fetchAppealsList();
    }

    @Watch('availableRoutes')
    @Watch('availableApplicants')
    @Watch('availableEmployees')
    @Watch('availableStatuses', {
        immediate: true,
    })
    public async setup() {
        this.tableConfig = {
            keyField: 'appealId',
            columns: [
                {
                    key: 'appealNumber',
                    title: 'Обращение',
                    subTitle: '№ и Дата',
                },
                {
                    key: '_requestsLength',
                    title: 'Заявления',
                    subTitle: '№№ и количество',
                },
                {
                    key: 'applicantId',
                    title: 'Заявитель',
                },
                {
                    key: 'routeId',
                    title: 'Маршрут',
                },
                {
                    key: '_contractId',
                    title: 'Договор',
                    subTitle: '№ и Дата',
                },
                {
                    key: '_contractValue',
                    title: this.$t('appeals.field._contractValue') as string,
                },
                {
                    key: 'appealStatusId',
                    title: 'Статус обращения',
                },
            ],
            data: [],
        };

        this.filtersConfig = ([
            {
                key: 'appealNumber',
                type: FIELD_TYPE_INPUT,
                options: {
                    meta: {
                        title: this.$t('appeals.field.appealNumber') as string,
                    },
                },
            },
            {
                key: 'appealId',
                type: FIELD_TYPE_INPUT,
                options: {
                    meta: {
                        title: this.$t('appeals.field.appealId') as string,
                    },
                    field: {
                        type: INPUT_TYPE_NUMBER,
                    },
                    filterString(value: any): string {
                        return `appealId == ${value}`;
                    },
                },
            },
            {
                key: 'routeId',
                type: FIELD_TYPE_SELECT,
                options: {
                    meta: {
                        title: 'Маршрут',
                    },
                    field: {
                        options: this.availableRoutes,
                        isMultiple: true,
                    },
                    filterString(value: any): string {
                        return `routeId =in= (${value.join(',')})`;
                    },
                },
            },
            {
                key: 'appealStatusId',
                type: FIELD_TYPE_SELECT,
                options: {
                    meta: {
                        title: 'Статус обращения',
                    },
                    field: {
                        options: this.availableStatuses,
                        isMultiple: true,
                    },
                    filterString(value: any): string {
                        return `appealStatusId =in= (${value.join(',')})`;
                    },
                },
            },
            {
                key: 'applicationDateFrom',
                type: FIELD_TYPE_DATEPICKER,
                options: {
                    meta: {
                        title: this.$t('appeals.field.applicationDate') as string,
                    },
                    field: {
                        format: 'd.m.Y',
                        before: this.$t('fields.datepicker.from') as string,
                    },
                    filterString(value: any): string {
                        return `applicationDate >= "${escapeFilterValue(value)}T00:00:00"`;
                    },
                },
            },
            {
                key: 'applicationDateTo',
                type: FIELD_TYPE_DATEPICKER,
                options: {
                    field: {
                        format: 'd.m.Y',
                        before: this.$t('fields.datepicker.to') as string,
                    },
                    filterString(value: any): string {
                        return `applicationDate <= "${escapeFilterValue(value)}T00:00:00"`;
                    },
                },
            },
            {
                key: 'applicantId',
                type: FIELD_TYPE_SELECT,
                options: {
                    meta: {
                        title: 'Заявитель',
                    },
                    field: {
                        options: this.availableApplicants,
                        isMultiple: true,
                    },
                    filterString(value: any): string {
                        return `applicantId =in= (${value.join(',')})`;
                    },
                },
            },
            {
                key: 'executorId',
                type: FIELD_TYPE_SELECT,
                options: {
                    meta: {
                        title: 'Исполнитель',
                    },
                    field: {
                        options: this.availableEmployees,
                        isMultiple: true,
                    },
                    filterString(value: any): string {
                        return `executorId =in= (${value})`;
                    },
                },
            },
        ] as IInlineFilterConfig['filters']).map((filter) => ({
            ...filter,
            options: {
                ...filter.options,
                field: {
                    ...(filter.options.field || {}),
                    isClearable: true,
                },
            },
        }));

        this.buildData();
    }

    protected async fetchAppealsList() {
        this.data = await this.$store.dispatch(
            `${MODULE_APPEALS}/${ACTION_FETCH_APPEALS_LIST}`,
            this.computedPayload,
        );

        this.buildData();
    }

    protected buildData() {
        if (!this.tableConfig) {
            return;
        }

        this.tableConfig.data = this.data.map((
            appeal: (AppealsModel & {
                _requestsLength?: number;
                _requestsIds?: string;
                _contractId?: number;
                _contractDate?: string;
                _contractValue?: string;
            }),
        ) => ({
            ...appeal,

            appealNumber: {
                value: appeal.appealNumber,
                hint: appeal.appealRegDate,
            },

            _requestsLength: {
                value: String(appeal._requestsLength),
                hint: appeal._requestsIds,
            },

            appealStatusId: this.availableStatusesMap[appeal.appealStatusId],
            applicantId: this.availableApplicantsMap[appeal.applicantId],
            routeId: this.availableRoutesMap[appeal.routeId] || '',

            _contractId: {
                value: String(appeal._contractId),
                hint: appeal._contractDate,
            },
            _contractValue: appeal._contractValue,
        }));
    }

    protected applyInlineFilter() {
        this.fetchAppealsList();
    }

    protected applyFilterModal() {
        this.fetchAppealsList();
    }
}

export default AppealsListPage;
