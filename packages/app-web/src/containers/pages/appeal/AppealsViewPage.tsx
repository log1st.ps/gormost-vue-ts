import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';
import Row from '@cip/gormost-core-web/src/components/grid/Row';
import Column from '@cip/gormost-core-web/src/components/grid/Column';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import {BUTTON_SIZE_XS} from '@cip/core/src/constants/button/buttonSizesConstants';
import {BUTTON_STATE_FONT_REGULAR, BUTTON_STATE_SECONDARY} from '@cip/core/src/constants/button/buttonStatesConstants';
import {
    ICON_TYPE_ARROW,
    ICON_TYPE_DOWNLOAD,
    ICON_TYPE_PRINT,
} from '@cip/gormost-core-web/src/elements/icon/availableIconTypes';
import Styler from '@cip/gormost-core-web/src/components/styler/Styler';
import {
    PrimaryTypographyComponent, SenaryTypographyComponent, TextTypographyComponent,
} from '@cip/gormost-core-web/src/components/typography/TypographyComponent';
import {ROW_ALIGN_END} from '@cip/core/src/constants/grid/rowAlignsConstants';
import Grid from '@cip/gormost-core-web/src/components/grid/Grid';
import {COLOR_PRIMARY} from '@cip/core/src/constants/styler/availableStylerColorsConstants';
import TabsComponent from '@cip/gormost-core-web/src/components/tabs/TabsComponent';
import TabComponent from '@cip/gormost-core-web/src/components/tabs/TabComponent';
import {VNode} from 'vue';
import AppealsViewGeneralTab from '@/containers/pages/appeal/tabs/AppealsViewGeneralTab';

const TAB_GENERAL = 'general';
const TAB_REQUESTS = 'requests';
const TAB_ROUTE = 'route';
const TAB_MB = 'mb';
const TAB_CALCULATIONS_AND_DOCS = 'calculationsAndDocs';

type availableTabs =
    typeof TAB_GENERAL
    | typeof TAB_REQUESTS
    | typeof TAB_ROUTE
    | typeof TAB_MB
    | typeof TAB_CALCULATIONS_AND_DOCS;

const availableTabsList = [
    TAB_GENERAL,
    TAB_REQUESTS,
    TAB_ROUTE,
    TAB_MB,
    TAB_CALCULATIONS_AND_DOCS,
];

@APIContainer({

})
@Component
class AppealsViewPage extends VueComponent<{}> {

    get baseInfo(): Array<{
        key: string,
        value: any,
    }> {
        return [
            {
                key: 'id',
                value: this.appeal.id,
            },
            {
                key: 'dateCreate',
                value: this.appeal.dateCreate,
            },
            {
                key: 'sender',
                value: this.appeal.sender,
            },
            {
                key: 'status',
                value: this.$t(`appeals.page.view.info.statuses.${this.appeal.status}`) as string,
            },
        ];
    }

    protected appeal: {
        id?: string,
        dateCreate?: string,
        sender?: string,
        status?: string,
    } = {};

    protected activeTab: availableTabs = TAB_GENERAL;

    public created(): void {
        this.$set(this, 'appeal', {
            id: '0016067/19',
            dateCreate: '20.05.2019',
            sender: 'АО "МОСВОДОКАНАЛ"',
            status: 'registered',
        });
    }

    get ComponentTab(): VNode | any {
        return {
            [TAB_GENERAL]: AppealsViewGeneralTab,
            [TAB_REQUESTS]: null,
            [TAB_ROUTE]: null,
            [TAB_MB]: null,
            [TAB_CALCULATIONS_AND_DOCS]: null,
        }[this.activeTab];
    }

    public render() {
        return (
            <div class={'appeal-new-page'}>
                <Styler height={24}/>
                <Grid gap={24}>
                    {this.renderToolbar()}
                    {this.renderTabs()}
                </Grid>
                <Styler height={8}/>
                <this.ComponentTab/>
            </div>
        );
    }

    public renderTabs() {
        return (
            <TabsComponent
                value={this.activeTab}
                onInput={this.setActiveTab as any}
            >
                {availableTabsList.map((tab) => (
                    <TabComponent
                        name={tab}
                        label={this.$t(`appeals.page.view.tab.${tab}`) as string}
                    />
                ))}
            </TabsComponent>
        );
    }

    protected setActiveTab(tab: availableTabs) {
        this.activeTab = tab;
    }

    protected renderToolbar() {
        return (
            <Row gap={16}>
                <Column gap={16}>
                    <ActionsListComponent
                        actions={[{
                            key: 'back',
                            size: BUTTON_SIZE_XS,
                            state: BUTTON_STATE_SECONDARY,
                            leftIcon: ICON_TYPE_ARROW,
                            isSquare: true,
                            onClick: () => {
                                this.onBackClick();
                            },
                        }]}
                    />
                </Column>
                <Column gap={16} align={ROW_ALIGN_END}>
                    <PrimaryTypographyComponent text={this.$t('appeals.page.view.title') as string}/>
                </Column>
                <Column isShrink isGrow gap={16}>
                    <Row gap={24}>
                        {this.baseInfo.map(({key, value}) => (
                            <Column gap={24}>
                                <Grid
                                    key={key}
                                    gap={4}
                                >
                                    <SenaryTypographyComponent
                                        text={this.$t(`appeals.page.view.info.${key}`) as string}
                                    />
                                    <Styler color={['id', 'status'].indexOf(key) > -1 ? COLOR_PRIMARY : undefined}>
                                        <TextTypographyComponent text={value}/>
                                    </Styler>
                                </Grid>
                            </Column>
                        ))}
                    </Row>
                </Column>
                <Column gap={16}>
                    <ActionsListComponent
                        gap={16}
                        actions={[
                            {
                                key: 'export',
                                state: [BUTTON_STATE_SECONDARY, BUTTON_STATE_FONT_REGULAR],
                                size: BUTTON_SIZE_XS,
                                leftIcon: ICON_TYPE_DOWNLOAD,
                                text: this.$t('appeals.page.view.action.export') as string,
                            },
                            {
                                key: 'print',
                                state: [BUTTON_STATE_SECONDARY, BUTTON_STATE_FONT_REGULAR],
                                size: BUTTON_SIZE_XS,
                                leftIcon: ICON_TYPE_PRINT,
                                text: this.$t('appeals.page.view.action.print') as string,
                            },
                        ]}
                    />
                </Column>
            </Row>
        );
    }

    protected onBackClick() {
        this.$router.back();
    }
}

export default AppealsViewPage;
