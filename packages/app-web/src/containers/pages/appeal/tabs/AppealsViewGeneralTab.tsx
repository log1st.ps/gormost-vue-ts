import {VueComponent} from 'vue-tsx-helper';
import {Component, ProvideReactive} from 'vue-property-decorator';
import Row from '@cip/gormost-core-web/src/components/grid/Row';
import Column from '@cip/gormost-core-web/src/components/grid/Column';
import CardComponent from '@cip/gormost-core-web/src/components/card/CardComponent';
import AccordionComponent from '@cip/gormost-core-web/src/components/accordion/AccordionComponent';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import {ACTION_LIST_DIRECTION_COLUMN} from '@cip/core/src/constants/actionsList/actionsListDirectionContstants';
import {IActionsList} from '@cip/core/src/mixins/components/actionsList/ActionsListComponentMixin.d';
import {BUTTON_STATE_QUATERNARY} from '@cip/core/src/constants/button/buttonStatesConstants';
import {BUTTON_SIZE_XS} from '@cip/core/src/constants/button/buttonSizesConstants';
import {ROW_ALIGN_START} from '@cip/core/src/constants/grid/rowAlignsConstants';
import {
    FIELD_TYPE_INPUT,
    IInputFormElement
} from "@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin";
import {
    FIELD_TYPE_SELECT,
    ISelectFormElement
} from "@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin";
import {
    FIELD_TYPE_DATEPICKER,
    IDatepickerFormElement
} from "@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin";
import {IFieldType} from "@cip/core/src/mixins/components/form/elements/CommonFormElementMixin.d";
import {
    FIELD_TYPE_CHECKBOX,
    ICheckboxFormElement
} from "@cip/core/src/mixins/components/form/elements/checkbox/CheckboxFormElementMixin";
import {IFieldFormElement} from "@cip/core/src/mixins/components/form/elements/field/FieldFormElementMixin";
import {
    FIELD_TYPE_FILE,
    IFileFormElement
} from "@cip/core/src/mixins/components/form/elements/file/FileFormElementMixin";
import {IInlineFilterConfig} from "@/interfaces/filterInterfaces";
import {BUTTON_STATE_PRIMARY} from "../../../../../../core/src/constants/button/buttonStatesConstants";
import {BUTTON_SIZE_MD} from "../../../../../../core/src/constants/button/buttonSizesConstants";
import {ICON_TYPE_PLUS} from "../../../../../../core/src/constants/icon/iconTypesConstants";
import {BUTTON_TYPE_LINK} from "../../../../../../core/src/constants/button/buttonTypesConstants";
import {APPEALS_PAGE_CREATE} from "@/constants/pages/pagesConstants";

export interface IAccordionViewConfig {
    data: IAccordionConfig[];
}


export interface IAccordionConfig {
    title: string,
    data: IAccordionGroupConfig[];
}

export interface IAccordionGroupConfig {
    key: string,
    type: IFormType
}

export type IFormType = ({
    type: typeof FIELD_TYPE_CHECKBOX;
    options: {
        meta?: IFieldFormElement,
        field?: ICheckboxFormElement,
    };
} | {
    type: typeof FIELD_TYPE_DATEPICKER;
    options: {
        meta?: IFieldFormElement,
        field?: IDatepickerFormElement,
    }
} | {
    type: typeof FIELD_TYPE_FILE;
    options: {
        meta?: IFieldFormElement,
        field?: IFileFormElement,
    };
} | {
    type: typeof FIELD_TYPE_INPUT;
    options: {
        meta?: IFieldFormElement,
        field?: IInputFormElement,
    };
} | {
    type: typeof FIELD_TYPE_SELECT;
    options: {
        meta?: IFieldFormElement,
        field?: ISelectFormElement,
    };
});

interface ICommonFilterType {
    value?: any;
    options: {
        field?: {
            [key: string]: any,
            value?: any,
        },
        meta?: IFieldFormElement,
    };
    key: any;
    onUpdate?(field: string, value: any): void | Promise<void>;
}

@Component
class AppealsViewGeneralTab extends VueComponent<{}> {
    @ProvideReactive('accordionConfig')
    protected accordionConfig?: IAccordionViewConfig | null = null;


    public created(): void {
        this.setup()
    }

    get actions(): IActionsList['actions'] {
        return [
            {
                key: 'collapse',
                state: BUTTON_STATE_QUATERNARY,
                size: BUTTON_SIZE_XS,
                text: this.$t('appeals.page.view.general.actions.collapse') as string,
                isBlock: true,
            },
            {
                key: 'expand',
                state: BUTTON_STATE_QUATERNARY,
                size: BUTTON_SIZE_XS,
                text: this.$t('appeals.page.view.general.actions.expand') as string,
                isBlock: true,
                isDisabled: true,
            },
            {
                key: 'edit',
                state: BUTTON_STATE_QUATERNARY,
                size: BUTTON_SIZE_XS,
                text: this.$t('appeals.page.view.general.actions.edit') as string,
                isBlock: true,
            },
            {
                key: 'save',
                state: BUTTON_STATE_QUATERNARY,
                size: BUTTON_SIZE_XS,
                text: this.$t('appeals.page.view.general.actions.save') as string,
                isBlock: true,
                isDisabled: true,
            },
        ];
    }

    public render() {
        return (
            <Row gap={8} align={ROW_ALIGN_START}>
                <Column gap={8} isShrink={false}>
                    <CardComponent>
                        Steps here
                    </CardComponent>
                </Column>
                <Column gap={8} isGrow isShrink>
                    <CardComponent>
                        {this.accordionRender()}
                    </CardComponent>
                </Column>
                <Column gap={8} isShrink={false}>
                    <CardComponent>
                        <ActionsListComponent
                            actions={this.actions}
                            direction={ACTION_LIST_DIRECTION_COLUMN}
                            gap={8}
                        />
                    </CardComponent>
                </Column>
            </Row>
        );
    }

    public async setup() {

        this.accordionConfig = {
            data: [
                {
                    title: 'test',
                    data: [
                        {
                            key: 'appealNumber',
                            type: {
                                type: FIELD_TYPE_INPUT,
                                options: {
                                    meta: {
                                        title: this.$t('appeals.field.appealNumber') as string,
                                    },
                                },
                            }
                        }
                    ]
                },
            ]
        }
    }

    public async accordionRender () {
        return (
            this.accordionConfig != null ? this.accordionConfig.data.map(item =>
                console.log(item)
            ) : ''
        )
    }
}

export default AppealsViewGeneralTab;
