import {VueComponent} from 'vue-tsx-helper';
import {Component} from 'vue-property-decorator';
import EGIPMap from '@/containers/helpers/EGIPMap';

@Component
class AppealsViewRouteTab extends VueComponent<{}> {
    public render() {
        return (
            <div>
                <EGIPMap
                    height={400}
                />
            </div>
        );
    }
}

export default AppealsViewRouteTab;
