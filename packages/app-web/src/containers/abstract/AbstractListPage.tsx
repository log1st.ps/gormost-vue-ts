import {VueComponent} from 'vue-tsx-helper';
import {Component, InjectReactive, ProvideReactive, Watch} from 'vue-property-decorator';
import {IFilterType, IInlineFilterConfig, mergeFilterConfigWithModel} from '@/interfaces/filterInterfaces';
import TableView, {ITableViewConfig} from '@/containers/layouts/TableView';
import waitForTicks from '@cip/core/src/helpers/waitForTicks';
import APIContainer from '@cip/core/src/helpers/documentation/APIContainer';
import Absolute from '@cip/gormost-core-web/src/components/grid/Absolute';
import ActionsListComponent from '@cip/gormost-core-web/src/components/actionsList/ActionsListComponent';
import {IActionsList} from '@cip/core/src/mixins/components/actionsList/ActionsListComponentMixin.d';
import {
    IConfiguredApplyingFilter,
} from '@/containers/helpers/FilterModal';
import {buildRSQLFromFilterBuilder, buildRSQLFromInlineFilter} from '@/services/filterService';

@APIContainer({
    description: `
        This page allows you to incapsulate table page logic.
        How to use:
        1. set
            filterModel
            filtersConfig
            filterBuilderModel
        2. render
    `,
})
@Component
class AbstractListPage extends VueComponent<{}> {

    get nonEmptyInlineFilters(): Array<[string, IFilterType]> {
        return Object.entries(this.filterModel)
            .filter(([key, value]) => ['', null, undefined].indexOf(value) === -1)
            .filter(([key, value]) => Array.isArray(value) ? value.length > 0 : true);
    }

    get nonEmptyBuilderFilters(): IConfiguredApplyingFilter[] {
        return this.filterBuilderModel
            .filter(({value}) => ['', null, undefined].indexOf(value) === -1);
    }

    get computedFilter(): string {
        return [
            ...this.nonEmptyInlineFilters
                .map(([key, value]) => buildRSQLFromInlineFilter({
                    key,
                    value,
                    config: this
                        .filtersConfig
                        .find(({key: filterKey}) => filterKey === key),
                })),
            ...this.nonEmptyBuilderFilters
                .map(buildRSQLFromFilterBuilder),
        ]
            .join(' and ');
    }

    get computedPayload(): object {
        return {
            ...(this.computedFilter ? {
                isActiveFilter: 'y',
                search: this.computedFilter,
            } : {}),
        };
    }

    @ProvideReactive('filterFields')
    protected filterFields: IFilterType[] = [];

    @ProvideReactive('toolbarTabs')
    @InjectReactive('toolbarTabs')
    protected toolbarTabs?: IFilterType[];

    @ProvideReactive('filterBuilderModel')
    protected filterBuilderModel: IConfiguredApplyingFilter[] = [];

    protected filterModel: IInlineFilterConfig['model'] = {};
    protected filtersConfig: IInlineFilterConfig['filters'] = [];
    protected tableConfig?: ITableViewConfig | null = null;
    protected primaryActions?: IActionsList | null = null;
    protected pageId?: string = '';
    protected title?: string = '';

    @ProvideReactive('filterTitle')
    protected filterTitle?: string;

    private droppedModel?: IInlineFilterConfig['model'] = {};

    public render() {
        return (
            <div id={this.pageId}>
                <TableView
                    title={this.title}
                    onUpdateFilter={this.onUpdateFilter}
                    tableConfig={this.tableConfig}
                    onInlineFilterClear={this.dropModel}
                    onInlineFilterApply={this.applyInlineFilter}
                    onFilterModalApply={this.applyFilterModal}
                />
                {this.primaryActions && (
                    <Absolute
                        isFixed
                        offset={[null, 22, 22, null]}
                        zIndex={1}
                    >
                        {
                            // @ts-ignore
                            <ActionsListComponent
                                {...{attrs: this.primaryActions}}
                            />
                        }
                    </Absolute>
                )}
            </div>
        );
    }

    public created(): void {
        this.droppedModel = {...this.filterModel};
    }

    protected applyInlineFilter() {
        // something
    }

    protected applyFilterModal() {
        // something
    }

    protected dropModel() {
        this.$set(this, 'filterModel', this.droppedModel);
    }

    protected computeFilterFields() {
        this.filterFields = mergeFilterConfigWithModel(
            this.filtersConfig
                .map((filter) => ({
                    ...filter,
                    onUpdate: (key: string, value: any): void | Promise<void> => {
                        this.filterModel[key] = value;
                    },
                })),
            this.filterModel,
        );
    }

    protected onUpdateFilter({key, value}: { key: string, value: any }) {
        this.filterModel[key] = value;
    }

    @Watch('filterModel', {
        immediate: true,
    })
    @Watch('filtersConfig', {
        deep: true,
    })
    protected async watchConfig() {
        await waitForTicks(this);

        this.computeFilterFields();
    }
}

export default AbstractListPage;
