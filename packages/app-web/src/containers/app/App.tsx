import {Component} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';

import './App.scss';
import Header from '@/containers/helpers/Header';
import MainLayoutComponent from '@cip/gormost-core-web/src/layouts/main/MainLayoutComponent';
import Navigation from '@/containers/helpers/Navigation';
import {PORTAL_DATEPICKER, PORTAL_FIXED, PORTAL_MODAL, PORTAL_SELECT} from '@cip/core/src/constants/portal/portalNames';
import {PortalTarget} from 'portal-vue';
import {ACTION_FETCH_ROUTES_LIST, MODULE_ROUTES} from '@/store/modules/routes/routesStoreConstants';
import {
    ACTION_FETCH_APPEAL_STATUSES_LIST,
    ACTION_FETCH_APPLICANTS_LIST, ACTION_FETCH_EMPLOYEES_LIST,
    MODULE_APPEALS,
} from '@/store/modules/appeals/appealsStoreConstants';
import {ACTION_FETCH_REQUESTS_STATUSES_LIST, MODULE_REQUESTS} from '@/store/modules/requests/requestsStoreConstants';

@Component
class App extends VueComponent<{}> {

    public async created(): Promise<void> {
        await this.$store.dispatch(`${MODULE_ROUTES}/${ACTION_FETCH_ROUTES_LIST}`);
        await this.$store.dispatch(`${MODULE_APPEALS}/${ACTION_FETCH_APPEAL_STATUSES_LIST}`);
        await this.$store.dispatch(`${MODULE_APPEALS}/${ACTION_FETCH_APPLICANTS_LIST}`);
        await this.$store.dispatch(`${MODULE_APPEALS}/${ACTION_FETCH_EMPLOYEES_LIST}`);
        await this.$store.dispatch(`${MODULE_REQUESTS}/${ACTION_FETCH_REQUESTS_STATUSES_LIST}`);
    }

    public render() {
        return (
            <MainLayoutComponent
                scopedSlots={{
                    header: () => (
                        <Header/>
                    ),
                    navigation: () => (
                        <Navigation/>
                    ),
                }}
            >
                {
                    // @ts-ignore
                    (<router-view/>)
                }
                {[
                    PORTAL_DATEPICKER,
                    PORTAL_SELECT,
                    PORTAL_MODAL,
                    PORTAL_FIXED,
                ].map((portalName) => (
                    // @ts-ignore
                    <PortalTarget
                        multiple
                        name={portalName}
                    />
                ))}
            </MainLayoutComponent>
        );
    }
}

export default App;
