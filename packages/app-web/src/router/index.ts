import Vue from 'vue';
import Router from 'vue-router';
import {
  APPEALS_PAGE_CREATE, APPEALS_PAGE_LIST, REGISTRY_PAGE_INDEX, REQUESTS_PAGE_LIST,
} from '@/constants/pages/pagesConstants';
import RootView from '@/containers/layouts/RootView';
import AppealView from '@/containers/pages/AppealView';
import RegistryView from '@/containers/pages/RegistryView';
import AppealsViewPage from '@/containers/pages/appeal/AppealsViewPage';
import RequestsListPage from '@/containers/pages/requests/RequestsListPage';
import RequestsView from '@/containers/pages/RequestsView';
import AppealsListPage from '@/containers/pages/appeal/AppealsListPage';
import TemporaryRoute from '@/containers/pages/TemporaryRoute';
import AppealAccordionPreview from '@/containers/tmp/appealAccordion/AppealAccordionPreview';
import TmpView from '@/containers/tmp/TmpView';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'tmp-route',
      path: '/tmp-route',
      component: TemporaryRoute,
    },
    {
      name: 'tmp',
      path: '/tmp',
      component: TmpView,
      children: [
        {
          name: 'appeal-accordion',
          path: 'appeal-accordion',
          component: AppealAccordionPreview,
          children: [
              // Здесь роуты для контейнеров
          ],
        },
      ],
    },
    {
      name: 'root',
      path: '/',
      component: RootView,
      children: [
        {
          path: 'registry',
          name: REGISTRY_PAGE_INDEX,
          component: RegistryView,
          children: [
            {
              path: 'appeals',
              component: AppealView,
              children: [
                {
                  name: APPEALS_PAGE_LIST,
                  path: 'list',
                  component: AppealsListPage,
                },
                {
                  name: APPEALS_PAGE_CREATE,
                  path: 'new',
                  component: AppealsViewPage,
                },
              ],
            },
            {
              path: 'requests',
              component: RequestsView,
              children: [
                {
                  name: REQUESTS_PAGE_LIST,
                  path: 'list',
                  component: RequestsListPage,
                },
              ],
            },
          ],
        },
      ],
    },
  ],
});
