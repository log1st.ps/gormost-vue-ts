import VueI18N from 'vue-i18n';
import Vue from 'vue';
import messages from '@/i18n/messages';

Vue.use(VueI18N);

export default new VueI18N({
    locale: 'ru',
    messages,
});
