import appeals from '@/i18n/ru/domains/appealsDomain';

export default {
    registry: {
        title: 'Реестр',
        tabs: {
            requests: 'Заявлений',
            appeals: 'Обращений',
        },
    },
    table: {
        controls: {
            table: 'Настройки таблицы',
            export: 'Экспорт в Excel',
            print: 'Печать',
            search: 'Поиск',
        },
    },
    filter: {
        title: 'Фильтр',
        clearAll: 'Очистить всё',
        apply: 'Применить',
        fieldTitle: 'По полю',
        field: 'Поле',
        add: 'Добавить условие',
        operator: 'Оператор',
        value: 'Значение',
        operators: {
            equal: 'Равно',
            notEqual: 'Не равно',
            like: 'Содержит',
            between: 'Между',
            less: 'Менее',
            more: 'Более',
            boolean: 'Логически равно',
            in: 'В списке',
        },
    },
    fields: {
        datepicker: {
            from: 'с',
            to: 'по',
        },
    },
    navigation: {
        requests: 'Обработка заявлений',
        buildings: 'Инженерные сооружения',
        nsi: 'НСИ',
        administration: 'Администрирование',
        analytics: 'Аналитика',
    },
    appeals,
};
