const fs = require("fs");
const axios = require("axios");

[
    "http://192.168.40.235:8112/gm-rest/api/v1/v2/api-docs?group=/api/v1",
    "http://192.168.40.235:8112/gm-nsi-rest/api/v1/v2/api-docs?group=/api/v1",
]
    .forEach(async (url) => {

        const {data: {paths, definitions}} = await axios.get(url);

        Object.entries(paths)
            .filter(([key]) => key.split("/").filter(Boolean).length)
            .map(([uri, info]) => {
                const domainMatches = uri.split("/").filter(Boolean);
                const domain = convertKebabToCamelCase(domainMatches[0]);

                if (domainMatches[1] === "{id}" && "get" in info) {
                    const ref = info.get.responses[200].schema.$ref.replace(/#\/definitions\//, "");
                    const definition = definitions[ref];

                    const path = "models";
                    const interfaceName = `${domain[0].toUpperCase() + domain.substr(1)}Model`;
                    const filePath = `${path}/${interfaceName}.ts`;
                    if (!fs.existsSync(path)) {
                        fs.mkdirSync(path, {recursive: true});
                    }

                    const params = Object.entries(definition.properties)
                        .filter(([key]) => key !== "_links")
                        .map(
                            ([key, config]) =>
                                [
                                    key,
                                    (definition.required || []).indexOf(key) > -1 ? ":" : "?:",
                                    ` ${convertTypeToTypescriptType(config.type)}`,
                                ].join(""),
                        );

                    fs.writeFileSync(filePath,
                        `import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ${interfaceName} extends AbstractModel {
    ${params.join([
                            `;\n`,
                            "    ",
                        ].join(""))};
}
`,
                    );
                }
            });
    });

const convertKebabToCamelCase = (key) =>
    key
        .split("-")
        .map(
            (item, index) => index > 0
                ? item[0].toUpperCase() + item.substr(1)
                : item,
        )
        .join("");

const convertTypeToTypescriptType = (type) => ({
    "integer": "number",
    "number": "number",
    "string": "string",
}[type] || "any");
