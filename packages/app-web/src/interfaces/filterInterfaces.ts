import {
    FIELD_TYPE_CHECKBOX,
} from '@cip/core/src/mixins/components/form/elements/checkbox/CheckboxFormElementMixin';
import {
    ICheckboxFormElement,
} from '@cip/core/src/mixins/components/form/elements/checkbox/CheckboxFormElementMixin.d';
import {IFieldFormElement} from '@cip/core/src/mixins/components/form/elements/field/FieldFormElementMixin.d';
import {
    FIELD_TYPE_DATEPICKER,
} from '@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin';
import {
    IDatepickerFormElement,
} from '@cip/core/src/mixins/components/form/elements/datepicker/DatepickerFormElementMixin.d';
import {
    FIELD_TYPE_FILE,
} from '@cip/core/src/mixins/components/form/elements/file/FileFormElementMixin';
import {
    IFileFormElement,
} from '@cip/core/src/mixins/components/form/elements/file/FileFormElementMixin.d';
import {
    FIELD_TYPE_INPUT,
} from '@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin';
import {
    IInputFormElement,
} from '@cip/core/src/mixins/components/form/elements/input/InputFormElementMixin.d';
import {
    FIELD_TYPE_SELECT,
} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin';
import {
    ISelectFormElement,
} from '@cip/core/src/mixins/components/form/elements/select/SelectFormElementMixin.d';
import {IFieldType} from '@cip/core/src/mixins/components/form/elements/CommonFormElementMixin.d';


export const FILTER_TYPE_STRING = 'String';
export const FILTER_TYPE_NUMBER = 'Number';
export const FILTER_TYPE_BOOLEAN = 'Boolean';
export const FILTER_TYPE_ARRAY = 'Array';
export const FILTER_TYPE_OBJECT = 'Object';
export const FILTER_TYPE_DATE = 'Date';

interface ICommonFilterType {
    value?: any;
    options: {
        field?: {
            [key: string]: any,
            value?: any,
        },
        meta?: IFieldFormElement,
        filterString?(value: any): string,
    };
    key: any;
    onUpdate?(field: string, value: any): void | Promise<void>;
}

export type IFilterType = ({
    type: typeof FIELD_TYPE_CHECKBOX;
    options: {
        meta?: IFieldFormElement,
        field?: ICheckboxFormElement,
    };
} | {
    type: typeof FIELD_TYPE_DATEPICKER;
    options: {
        meta?: IFieldFormElement,
        field?: IDatepickerFormElement,
    }
} | {
    type: typeof FIELD_TYPE_FILE;
    options: {
        meta?: IFieldFormElement,
        field?: IFileFormElement,
    };
} | {
    type: typeof FIELD_TYPE_INPUT;
    options: {
        meta?: IFieldFormElement,
        field?: IInputFormElement,
    };
} | {
    type: typeof FIELD_TYPE_SELECT;
    options: {
        meta?: IFieldFormElement,
        field?: ISelectFormElement,
    };
}) & ICommonFilterType;

export const mergeFilterConfigWithModel
    = (
    filter: IFilterType[],
    model: {[key: string]: any},
) => filter.map((item) => ({
    ...item,
    options: {
        ...item.options,
        field: {
            ...item.options.field,
            value: model[item.key],
        },
    },
}));

export interface IComputedFilter {
    component: any;
    key: string;
    type: IFieldType;
    options: {
        meta?: IFieldFormElement,
        field?: IFilterType | any,
    };
    onUpdate?(field: string, value: any): void | Promise<void>;
}

export interface IInlineFilterConfig {
    model: {
        [key: string]: any,
    };
    filters: IFilterType[];
}

export const OPERATOR_EQUAL = 'equal';
export const OPERATOR_NOT_EQUAL = 'notEqual';
export const OPERATOR_LIKE = 'like';
export const OPERATOR_BETWEEN = 'between';
export const OPERATOR_LESS = 'less';
export const OPERATOR_MORE = 'more';
export const OPERATOR_BOOLEAN = 'boolean';
export const OPERATOR_IN = 'in';

export type availableOperators =
    typeof OPERATOR_EQUAL
    | typeof OPERATOR_NOT_EQUAL
    | typeof OPERATOR_LIKE
    | typeof OPERATOR_BETWEEN
    | typeof OPERATOR_BOOLEAN
    | typeof OPERATOR_LESS
    | typeof OPERATOR_IN
    | typeof OPERATOR_MORE;

export const operatorsByFieldType: {
    [key: string]: availableOperators[],
} = {
    [FIELD_TYPE_INPUT]: [
        OPERATOR_EQUAL,
        OPERATOR_NOT_EQUAL,
        OPERATOR_BETWEEN,
        OPERATOR_LESS,
        OPERATOR_IN,
        OPERATOR_MORE,
        OPERATOR_LIKE,
    ],
    [FIELD_TYPE_DATEPICKER]: [
        OPERATOR_EQUAL,
        OPERATOR_NOT_EQUAL,
        OPERATOR_BETWEEN,
        OPERATOR_LESS,
        OPERATOR_MORE,
    ],
    [FIELD_TYPE_SELECT]: [
        OPERATOR_EQUAL,
        OPERATOR_NOT_EQUAL,
        OPERATOR_IN,
    ],
    [FIELD_TYPE_CHECKBOX]: [
        OPERATOR_BOOLEAN,
    ],
};
