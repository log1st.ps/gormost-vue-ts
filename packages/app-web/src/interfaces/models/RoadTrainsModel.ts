import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RoadTrainsModel extends AbstractModel {
    maxSpeed: number;
    minTurnRad: number;
    requestId: number;
    roadTrainHeight: number;
    roadTrainId: number;
    roadTrainLength: number;
    roadTrainWeight: number;
    roadTrainWeightWithCargo: number;
    roadTrainWidth: number;
    ver: number;
}
