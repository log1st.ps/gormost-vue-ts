import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefTypicalAxleLoadsModel extends AbstractModel {
    typicalAxleLoadName: string;
    ver: number;
    typicalAxleLoadId: number;
}
