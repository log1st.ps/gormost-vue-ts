import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAxleLoadExcessesModel extends AbstractModel {
    description: string;
    maxExcessValue: number;
    minExcessValue: number;
    ver: number;
    axleLoadExcessId: number;
}
