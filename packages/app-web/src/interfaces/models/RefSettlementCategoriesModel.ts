import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefSettlementCategoriesModel extends AbstractModel {
    settlementCategoryName: string;
    ver: number;
    settlementCategoryId: number;
}
