import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAddressRouteTypesModel extends AbstractModel {
    addressRouteTypeName?: string;
    ver: number;
    addressRouteTypeId: number;
}
