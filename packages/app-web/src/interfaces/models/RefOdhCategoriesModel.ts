import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefOdhCategoriesModel extends AbstractModel {
    odhCategoryId?: number;
    odhCategoryName?: string;
    ver: number;
}
