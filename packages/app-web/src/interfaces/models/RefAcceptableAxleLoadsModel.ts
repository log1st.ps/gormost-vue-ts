import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAcceptableAxleLoadsModel extends AbstractModel {
    acceptableAxleLoadValue?: number;
    airSuspension?: any;
    axleLoadId: number;
    axleLocationId: number;
    axleSpacingId: number;
    endDate?: string;
    startDate: string;
    ver: number;
    wheelsAmountId: number;
    acceptableAxleLoadId: number;
}
