import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefDocTypesModel extends AbstractModel {
    divIssuerVisible: string;
    docCommentVisible: string;
    docDateIssVisible: string;
    docDateRegVisible: string;
    docIssuerVisible: string;
    docNumVisible: string;
    docSeriesVisible: string;
    docTypeName: string;
    etpCode: number;
    groupId: number;
    initDisplay: string;
    interdepRequest: string;
    isActive: string;
    orderNum: number;
    typeCode: string;
    ver: number;
    versionNumber: number;
    warehouseDocType: string;
    docTypeId: number;
}
