import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface BanksModel extends AbstractModel {
    bankId: number;
    bankName: string;
    bik: string;
    correspondentAccount: string;
    ver: number;
}
