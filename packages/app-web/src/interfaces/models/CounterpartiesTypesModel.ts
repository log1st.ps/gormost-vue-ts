import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface CounterpartiesTypesModel extends AbstractModel {
    counterpartiesTypeId: number;
    counterpartiesTypeName: string;
    ver: number;
}
