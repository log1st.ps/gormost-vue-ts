import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ContractAppealsModel extends AbstractModel {
    appealId?: number;
    contractId?: number;
    ver: number;
    contractAppealId?: number;
}
