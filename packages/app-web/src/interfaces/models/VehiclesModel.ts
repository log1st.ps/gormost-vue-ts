import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface VehiclesModel extends AbstractModel {
    requestId: number;
    roadTrainNumber?: number;
    stateRegistrationNumber: string;
    vehicleAxlesAmount?: number;
    vehicleId: number;
    vehicleModel: string;
    vehicleType: string;
    vehicleTypeId?: number;
    vehicleWeight: number;
    ver: number;
}
