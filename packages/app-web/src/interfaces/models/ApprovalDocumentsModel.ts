import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ApprovalDocumentsModel extends AbstractModel {
    approvalDocumentLocalDate?: string;
    attachedDocId?: number;
    docTypeName?: string;
    externalApprovalId?: number;
    number?: string;
    url?: string;
    ver: number;
    approvalDocumentId?: number;
}
