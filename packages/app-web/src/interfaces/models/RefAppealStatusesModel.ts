import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAppealStatusesModel extends AbstractModel {
    appealStatusName?: string;
    ver: number;
    appealStatusId: number;
}
