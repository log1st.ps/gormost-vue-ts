import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefSettlementPeriodsModel extends AbstractModel {
    settlementPeriodName: string;
    ver: number;
    settlementPeriodId: number;
}
