import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefSolutionMethodsModel extends AbstractModel {
    solutionMethodId: number;
    solutionMethodName: string;
    ver: number;
}
