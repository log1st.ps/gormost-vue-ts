import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefVehicleTypesModel extends AbstractModel {
    vehicleTypeId: number;
    vehicleTypeName: string;
    ver: number;
}
