import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefApplicantTypesModel extends AbstractModel {
    applicantTypeName: string;
    ver: number;
    applicantTypeId: number;
}
