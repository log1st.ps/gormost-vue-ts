import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefContractTypesModel extends AbstractModel {
    contractTypeName: string;
    ver: number;
    contractTypeId: number;
}
