import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAxleLocationsModel extends AbstractModel {
    description: string;
    ver: number;
    axleLocationId: number;
}
