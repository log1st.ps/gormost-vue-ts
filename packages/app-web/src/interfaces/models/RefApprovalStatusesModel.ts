import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefApprovalStatusesModel extends AbstractModel {
    approvalStatusId?: number;
    approvalStatusName?: string;
    ver: number;
}
