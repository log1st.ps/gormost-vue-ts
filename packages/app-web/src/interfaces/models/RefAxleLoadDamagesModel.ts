import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAxleLoadDamagesModel extends AbstractModel {
    axleLoadDamageValue: number;
    axleLoadExcessId: number;
    axleLoadId: number;
    endDate?: string;
    startDate: string;
    ver: number;
    axleLoadDamageId: number;
}
