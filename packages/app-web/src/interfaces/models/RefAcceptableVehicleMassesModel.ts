import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAcceptableVehicleMassesModel extends AbstractModel {
    acceptableVehicleMassValue: number;
    axlesAmount: number;
    description: string;
    roadTrain?: any;
    ver: number;
    acceptableVehicleMassId: number;
}
