import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefPricesModel extends AbstractModel {
    amountVat: number;
    price: number;
    priceWithVat: number;
    rateId: number;
    settlementCategoryId: number;
    settlementPeriodId: number;
    ver: number;
    priceId: number;
}
