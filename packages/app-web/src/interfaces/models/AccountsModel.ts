import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface AccountsModel extends AbstractModel {
    ver: number;
    accountId: number;
    accountNumber: string;
    bankId: number;
    counterpartiesId: number;
}
