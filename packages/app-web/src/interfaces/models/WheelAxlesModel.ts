import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface WheelAxlesModel extends AbstractModel {
    airSuspensionFact?: any;
    axleId: number;
    axleLoadFact?: number;
    axleNumber: number;
    discriptionWheels?: string;
    distanceToNextAxle?: number;
    vehicleId: number;
    ver: number;
    wheelsAmountId: number;
}
