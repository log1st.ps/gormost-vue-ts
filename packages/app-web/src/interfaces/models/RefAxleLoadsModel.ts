import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAxleLoadsModel extends AbstractModel {
    axleLoadValue: number;
    ver: number;
    axleLoadId: number;
}
