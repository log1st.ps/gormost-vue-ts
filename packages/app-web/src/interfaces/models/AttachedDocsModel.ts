import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface AttachedDocsModel extends AbstractModel {
    appealId: number;
    docTypeId: number;
    docTypeName: string;
    fileName: string;
    fileUrl: string;
    requestId: number;
    ver: number;
    attachedDocId: number;
}
