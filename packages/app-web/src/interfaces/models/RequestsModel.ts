import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RequestsModel extends AbstractModel {
    amountVisit: number;
    endDate: string;
    loadDivisible?: any;
    requestId: number;
    requestNumber: string;
    requestStatusByStatusId?: any;
    startDate: string;
    ver: number;
}
