import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefDamageCoefficientsModel extends AbstractModel {
    axleLoadId?: number;
    damageCoefficientName: string;
    damageCoefficientValue: number;
    description: string;
    endDate?: string;
    startDate: string;
    ver: number;
    damageCoefficientId: number;
}
