import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RepresentOrgsModel extends AbstractModel {
    contactNumber: string;
    counterpartiesId: number;
    emailAddress: string;
    firstName?: string;
    fullName: string;
    inn: string;
    lastName?: string;
    middleName?: string;
    ogrn: string;
    powersEmployeeId: number;
    powersEmployeeName: string;
    representOrgId: number;
    representOrgName: string;
    shortName: string;
    ver: number;
}
