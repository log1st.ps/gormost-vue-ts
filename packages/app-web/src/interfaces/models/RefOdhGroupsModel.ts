import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefOdhGroupsModel extends AbstractModel {
    odhGroupId?: number;
    odhGroupName?: string;
    ver: number;
}
