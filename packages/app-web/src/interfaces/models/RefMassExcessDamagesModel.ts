import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefMassExcessDamagesModel extends AbstractModel {
    axleLoadExcessId: number;
    endDate?: string;
    massExcessDamageId: number;
    massExcessDamageValue: number;
    startDate: string;
    ver: number;
}
