import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface AddressTypesModel extends AbstractModel {
    addressTypeId: number;
    addressTypeName: string;
    ver: number;
}
