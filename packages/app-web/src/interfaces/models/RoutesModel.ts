import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RoutesModel extends AbstractModel {
    departureAddressLandmark?: string;
    departureCityAoid?: string;
    departureHouseId?: string;
    departureSettlementAoid?: string;
    departureStreetAoid?: string;
    destinationAddressLandmark?: string;
    destinationCityAoid?: string;
    destinationHouseId?: string;
    destinationSettlementAoid?: string;
    destinationStreetAoid?: string;
    mapObjectId?: number;
    reverseRouteRequired?: any;
    routeDescription?: string;
    routeExtraInfo?: string;
    routeId: number;
    totalRouteLength?: number;
    ver: number;
}
