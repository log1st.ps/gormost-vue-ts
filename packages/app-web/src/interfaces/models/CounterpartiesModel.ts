import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface CounterpartiesModel extends AbstractModel {
    addressTypeId: number;
    birthDate: string;
    counterpartiesId: number;
    counterpartiesKindId: number;
    counterpartiesTypeId: number;
    email: string;
    firstName: string;
    fullName: string;
    inn: string;
    kpp: string;
    lastName: string;
    middleName: string;
    ogrn: string;
    phone1: string;
    sex: string;
    shortName: string;
    ver: number;
}
