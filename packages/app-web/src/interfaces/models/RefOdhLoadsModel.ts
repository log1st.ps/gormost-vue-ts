import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefOdhLoadsModel extends AbstractModel {
    axleLoadId: number;
    odhCategoryId: number;
    odhGroupId: number;
    odhLoadId: number;
    ver: number;
}
