import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ApplicantsModel extends AbstractModel {
    birthDate: string;
    contactNumber: string;
    counterpartiesId: number;
    documentType: string;
    emailAddress: string;
    firstName: string;
    fullName?: string;
    gender?: string;
    inn?: string;
    issuedBy: string;
    issuedDate: string;
    kpp?: string;
    lastName: string;
    middleName?: string;
    ogrn?: string;
    powerEmployeeId: number;
    powerEmployeeName: string;
    seriesNumber: string;
    shortName?: string;
    ver: number;
    applicantId: number;
}
