import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefStructureTypesModel extends AbstractModel {
    structureTypeId: number;
    structureTypeName: string;
    ver: number;
}
