import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RouteSectionsModel extends AbstractModel {
    routeId: number;
    routeSectionContent: string;
    routeSectionId: number;
    routeSectionLength?: number;
    serialNumber: number;
    ver: number;
}
