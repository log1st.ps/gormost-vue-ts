import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefAxleSpacingsModel extends AbstractModel {
    description: string;
    maxSpacingValue: number;
    minSpacingValue: number;
    ver: number;
    axleSpacingId: number;
}
