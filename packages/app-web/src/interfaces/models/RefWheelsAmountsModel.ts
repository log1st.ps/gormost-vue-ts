import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefWheelsAmountsModel extends AbstractModel {
    description: string;
    ver: number;
    wheelsAmountId: number;
}
