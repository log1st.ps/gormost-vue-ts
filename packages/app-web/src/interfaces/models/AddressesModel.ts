import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface AddressesModel extends AbstractModel {
    addressId: number;
    addressIndex: string;
    addressString: string;
    addressTypeId: number;
    apartment: string;
    areaName: string;
    buildnum: string;
    cityName: string;
    counterpartiesId: number;
    estate: string;
    fias?: any;
    houseName: string;
    placeName: string;
    regionName: string;
    streetName: string;
    structure: string;
    ver: number;
}
