import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RequestStatusesModel extends AbstractModel {
    requestStatusId: number;
    requestStatusName: string;
    ver: number;
}
