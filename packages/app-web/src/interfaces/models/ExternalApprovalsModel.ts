import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ExternalApprovalsModel extends AbstractModel {
    approvalDateFact?: string;
    approvalDatePlan?: string;
    approvalResultId?: number;
    approvalStatusId?: number;
    counterpartiesId?: number;
    externalApprovalId?: number;
    inputOperator?: string;
    inputOperatorId?: number;
    requestId?: number;
    shortName?: string;
    startDate?: string;
    ver: number;
}
