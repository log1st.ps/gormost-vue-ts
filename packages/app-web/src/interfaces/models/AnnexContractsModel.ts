import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface AnnexContractsModel extends AbstractModel {
    amount?: number;
    annexContractDate?: string;
    appealNumber?: string;
    contractSettlementId?: number;
    fullAmount?: number;
    serialNumber?: number;
    vat?: number;
    ver: number;
    annexContractId?: number;
}
