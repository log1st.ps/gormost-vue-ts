import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface PositionsModel extends AbstractModel {
    counterpartiesId: number;
    endDate: string;
    positionId: number;
    positionName: string;
    startDate: string;
    ver: number;
}
