import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface CargoesModel extends AbstractModel {
    cargoHeight: number;
    cargoLength: number;
    cargoName: string;
    cargoWeight: number;
    cargoWidth: number;
    requestId: number;
    ver: number;
    cargoId: number;
}
