import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface DepartmentsModel extends AbstractModel {
    counterpartiesId: number;
    departmentId: number;
    departmentName: string;
    endDate: string;
    startDate: string;
    ver: number;
}
