import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface DulsModel extends AbstractModel {
    counterpartiesId: number;
    dulId: number;
    dulTypeId: number;
    issuedBy: string;
    issuedDate: string;
    seriesNumber: string;
    ver: number;
}
