import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface StructuresModel extends AbstractModel {
    latitude: number;
    longitude: number;
    mapObjectId: number;
    maxLoadCapacity?: number;
    structureHeight?: number;
    structureId: number;
    structureLength?: number;
    structureName: string;
    structureNumber: string;
    structureProperties?: string;
    structureTypeId: number;
    structureWidth?: number;
    typicalAxleLoadId?: number;
    ver: number;
}
