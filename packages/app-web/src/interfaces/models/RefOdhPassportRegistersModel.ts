import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefOdhPassportRegistersModel extends AbstractModel {
    axisLength?: number;
    balanceHolder?: string;
    bicycleLaneArea?: number;
    bjectCategory?: string;
    btiRegistrationNumber?: string;
    cleaningArea?: number;
    customerName?: string;
    odhCategoryName?: string;
    odhGroupName?: string;
    odhId?: number;
    odhName?: string;
    picketAxesBicycleLength?: number;
    roadwayArea?: number;
    sidewalkArea?: number;
    techPassportNumber?: string;
    totalAreaInVehicle?: number;
    totalAreaWithBord?: number;
    tpuArea?: number;
    ver: number;
}
