import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ContractStatusesModel extends AbstractModel {
    contractStatusName?: string;
    ver: number;
    contractStatusId?: number;
}
