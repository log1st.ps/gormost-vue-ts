import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface CounterpartiesKindsModel extends AbstractModel {
    counterpartiesKindId: number;
    counterpartiesKindName: string;
    ver: number;
}
