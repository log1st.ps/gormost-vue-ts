import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface DulTypesModel extends AbstractModel {
    code: number;
    dulTypeId: number;
    dulTypeName: string;
    ver: number;
}
