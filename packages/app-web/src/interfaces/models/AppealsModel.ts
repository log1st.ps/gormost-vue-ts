import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface AppealsModel extends AbstractModel {
    accountNumber: string;
    apartment?: string;
    appealNumber: string;
    appealRegDate: string;
    appealStatusId: number;
    applicantId: number;
    applicationDate?: string;
    areaName: string;
    bankName: string;
    bic: string;
    buildnum?: string;
    cityName: string;
    contracts?: any;
    corespondentAccount: string;
    counterpartiesTypeName: string;
    echo?: string;
    estate?: string;
    executorId: number;
    houseName: string;
    houseid: string;
    inputOperatorId: number;
    organizationTypeId?: number;
    placeName: string;
    placeSupply: string;
    plannedDate: string;
    receiptAppealDate?: string;
    regNumMpgu?: string;
    regionName: string;
    representOrgId: number;
    representTypeId: number;
    requests?: any;
    routeId: number;
    solutionMethodId: number;
    solutionMethodName: string;
    streetName: string;
    structure?: string;
    targetAppealId: number;
    targetAppealName?: string;
    ver: number;
    appealId: number;
}
