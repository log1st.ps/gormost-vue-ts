import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefApprovalResultsModel extends AbstractModel {
    approvalResultId?: number;
    approvalResultName?: string;
    ver: number;
}
