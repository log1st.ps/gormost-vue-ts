import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface EmployeesModel extends AbstractModel {
    departmentId: number;
    description: string;
    employeeId: number;
    endDate: string;
    executor?: any;
    fio: string;
    fired?: any;
    positionId: number;
    startDate: string;
    ver: number;
}
