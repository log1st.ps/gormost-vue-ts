import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefTargetAppealsModel extends AbstractModel {
    targetAppealId: number;
    targetAppealName: string;
    ver: number;
}
