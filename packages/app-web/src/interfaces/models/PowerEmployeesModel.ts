import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface PowerEmployeesModel extends AbstractModel {
    powerEmployeeName: string;
    ver: number;
    powerEmployeeId: number;
}
