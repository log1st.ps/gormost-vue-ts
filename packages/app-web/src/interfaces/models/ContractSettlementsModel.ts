import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ContractSettlementsModel extends AbstractModel {
    amount?: number;
    fullAmount?: number;
    price?: number;
    rateId?: number;
    routeNumber?: number;
    settlementCategoryId?: number;
    settlementCategoryName?: string;
    settlementDate?: string;
    settlementNumber?: string;
    settlementPeriodId?: number;
    settlementPeriodName?: string;
    vat?: number;
    ver: number;
    contractSettlementId?: number;
}
