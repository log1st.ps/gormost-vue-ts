import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefRateNamesModel extends AbstractModel {
    rateName: string;
    ver: number;
    workName: string;
    rateNameId: number;
}
