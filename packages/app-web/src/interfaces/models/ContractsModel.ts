import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface ContractsModel extends AbstractModel {
    contractDate?: string;
    contractNumber?: string;
    contractSettlements?: any;
    contractStatusId?: number;
    contractTypeId?: number;
    counterpartiesId?: number;
    endDate?: string;
    ver: number;
    contractId?: number;
}
