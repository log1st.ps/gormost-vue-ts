import {AbstractModel} from '@/interfaces/AbstractModel';

export default interface RefRatesModel extends AbstractModel {
    endLocalDate: string;
    rateNameId: number;
    startLocalDate: string;
    ver: number;
    workName: string;
    rateId: number;
}
