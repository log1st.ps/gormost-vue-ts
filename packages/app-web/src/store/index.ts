import Vue from 'vue';
import Vuex from 'vuex';

import user from './modules/user';
import routes from './modules/routes';
import appeals from './modules/appeals';
import requests from './modules/requests';

Vue.use(Vuex);

export default new Vuex.Store({
    strict: true,
    modules: {
        user,
        routes,
        appeals,
        requests,
    },
    state: {
        requestStatuses: [
            {id: 'new', name: 'New'},
            {id: 'checking', name: 'Checking'},
            {id: 'reverted', name: 'Reverted'},
            {id: 'declined', name: 'Declined'},
            {id: 'archived', name: 'Archived'},
        ],
    } as any,
});
