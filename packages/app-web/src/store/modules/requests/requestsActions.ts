import {ActionContext, ActionTree} from 'vuex';
import {
    ACTION_FETCH_REQUESTS_LIST,
    ACTION_FETCH_REQUESTS_STATUSES_LIST, MUTATION_SET_REQUESTS_STATUSES_LIST,
} from '@/store/modules/requests/requestsStoreConstants';
import {IRequestsState} from '@/store/modules/requests/IRequestsState';
import {IStore} from '@/store/IStore';
import RequestsModel from '@/interfaces/models/RequestsModel';
import getRequestsList from '@/services/api/requests/requests/getRequestsList';
import getRequestStatusesList from '@/services/api/requests/requestStatuses/getRequestStatusesList';

export default {
    [ACTION_FETCH_REQUESTS_LIST]:
        async (context: ActionContext<IRequestsState, IStore>, payload): Promise<RequestsModel[]> => {
            const {data: {data: requests}} =
                await getRequestsList(payload);

            return requests;
        },
    [ACTION_FETCH_REQUESTS_STATUSES_LIST]:
        async ({commit}: ActionContext<IRequestsState, IStore>) => {
            const {data: {data: statuses}} =
                await getRequestStatusesList();

            commit(MUTATION_SET_REQUESTS_STATUSES_LIST, statuses);
        },
} as ActionTree<IRequestsState, IStore>;
