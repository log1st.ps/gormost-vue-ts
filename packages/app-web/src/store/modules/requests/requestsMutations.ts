import {MutationTree} from 'vuex';
import {IRequestsState} from '@/store/modules/requests/IRequestsState';
import {MUTATION_SET_REQUESTS_STATUSES_LIST} from '@/store/modules/requests/requestsStoreConstants';
import RequestStatusesModel from '@/interfaces/models/RequestStatusesModel';

export default {
    [MUTATION_SET_REQUESTS_STATUSES_LIST]: (state, statuses: RequestStatusesModel[]) => {
        state.refStatuses = statuses;
    },
} as MutationTree<IRequestsState>;
