export const MODULE_REQUESTS = 'requests';

export const ACTION_FETCH_REQUESTS_LIST = 'fetchRequestsList';

export const ACTION_FETCH_REQUESTS_STATUSES_LIST = 'fetchStatusesList';
export const MUTATION_SET_REQUESTS_STATUSES_LIST = 'setStatusesList';
export const GETTER_REQUESTS_STATUSES_LIST = 'getStatusesList';
