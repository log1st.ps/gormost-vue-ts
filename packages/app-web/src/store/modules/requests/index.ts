import {IRequestsState} from '@/store/modules/requests/IRequestsState';
import {IStore} from '@/store/IStore';
import {Module} from 'vuex';
import requestsGetters from '@/store/modules/requests/requestsGetters';
import requestsActions from '@/store/modules/requests/requestsActions';
import requestsState from '@/store/modules/requests/requestsState';
import requestsMutations from '@/store/modules/requests/requestsMutations';

export default {
    namespaced: true,
    state: requestsState,
    mutations: requestsMutations,
    actions: requestsActions,
    getters: requestsGetters,
} as Module<IRequestsState, IStore>;
