import {IRequestsState} from '@/store/modules/requests/IRequestsState';

export default {
    refStatuses: [],
    refApplicants: [],
    refEmployees: [],
} as IRequestsState;
