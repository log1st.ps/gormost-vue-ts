import RequestStatusesModel from '@/interfaces/models/RequestStatusesModel';

export interface IRequestsState {
    refStatuses: RequestStatusesModel[];
}
