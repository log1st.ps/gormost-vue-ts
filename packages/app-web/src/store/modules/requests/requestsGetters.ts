import {GetterTree} from 'vuex';
import {IRequestsState} from '@/store/modules/requests/IRequestsState';
import {IStore} from '@/store/IStore';
import {GETTER_REQUESTS_STATUSES_LIST} from '@/store/modules/requests/requestsStoreConstants';

export default {
    [GETTER_REQUESTS_STATUSES_LIST]: (store) => store.refStatuses,
} as GetterTree<IRequestsState, IStore>;
