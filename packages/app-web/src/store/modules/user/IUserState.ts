export interface IUserState {
    avatarPath: string;
    firstName: string;
    middleName: string;
    lastName: string;
}
