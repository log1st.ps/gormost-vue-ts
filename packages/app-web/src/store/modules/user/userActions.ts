import {ActionTree} from 'vuex';
import {IUserState} from '@/store/modules/user/IUserState';
import {IStore} from '@/store/IStore';

export default {

} as ActionTree<IUserState, IStore>;
