import {IUserState} from '@/store/modules/user/IUserState';

export default {
    avatarPath: 'https://pp.userapi.com/c638629/v638629605/515b9/3COFxxl26gM.jpg',
    firstName: 'Виктория',
    middleName: 'Алексеевна',
    lastName: 'Ильенок',
} as IUserState;
