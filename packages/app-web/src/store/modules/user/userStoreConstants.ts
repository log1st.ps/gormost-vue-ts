export const MODULE_USER = 'user';

export const GETTER_USER_GET_USER = 'getUser';

export const MUTATION_USER_SET_USER = 'setUser';

export const ACTION_USER_FETCH_USER = 'fetchUser';
