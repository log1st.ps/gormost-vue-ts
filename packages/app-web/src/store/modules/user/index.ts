import userState from '@/store/modules/user/userState';
import userMutations from '@/store/modules/user/userMutations';
import userActions from '@/store/modules/user/userActions';
import userGetters from '@/store/modules/user/userGetters';
import {Module} from 'vuex';
import {IUserState} from '@/store/modules/user/IUserState';
import {IStore} from '@/store/IStore';

export default {
    namespaced: true,
    state: userState,
    mutations: userMutations,
    actions: userActions,
    getters: userGetters,
} as Module<IUserState, IStore>;
