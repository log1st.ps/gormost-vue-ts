import {IAppealsState} from '@/store/modules/appeals/IAppealsState';
import {IStore} from '@/store/IStore';
import {Module} from 'vuex';
import appealsGetters from '@/store/modules/appeals/appealsGetters';
import appealsActions from '@/store/modules/appeals/appealsActions';
import appealsState from '@/store/modules/appeals/appealsState';
import appealsMutations from '@/store/modules/appeals/appealsMutations';

export default {
    namespaced: true,
    state: appealsState,
    mutations: appealsMutations,
    actions: appealsActions,
    getters: appealsGetters,
} as Module<IAppealsState, IStore>;
