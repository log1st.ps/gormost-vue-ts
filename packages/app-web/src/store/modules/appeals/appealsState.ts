import {IAppealsState} from '@/store/modules/appeals/IAppealsState';

export default {
    refStatuses: [],
    refApplicants: [],
    refEmployees: [],
} as IAppealsState;
