import {MutationTree} from 'vuex';
import {IAppealsState} from '@/store/modules/appeals/IAppealsState';
import {
    MUTATION_SET_APPEAL_STATUSES_LIST,
    MUTATION_SET_APPLICANTS_LIST, MUTATION_SET_EMPLOYEES_LIST,
} from '@/store/modules/appeals/appealsStoreConstants';
import RefAppealStatusesModel from '@/interfaces/models/RefAppealStatusesModel';
import ApplicantsModel from '@/interfaces/models/ApplicantsModel';
import EmployeesModel from '@/interfaces/models/EmployeesModel';

export default {
    [MUTATION_SET_APPEAL_STATUSES_LIST]: (state, list: RefAppealStatusesModel[]) => {
        state.refStatuses = list;
    },
    [MUTATION_SET_APPLICANTS_LIST]: (state, list: ApplicantsModel[]) => {
        state.refApplicants = list;
    },
    [MUTATION_SET_EMPLOYEES_LIST]: (state, list: EmployeesModel[]) => {
        state.refEmployees = list;
    },
} as MutationTree<IAppealsState>;
