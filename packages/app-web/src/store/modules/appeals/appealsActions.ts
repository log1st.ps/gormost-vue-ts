import {ActionContext, ActionTree} from 'vuex';
import {IAppealsState} from '@/store/modules/appeals/IAppealsState';
import {IStore} from '@/store/IStore';
import {
    ACTION_FETCH_APPEAL_STATUSES_LIST,
    ACTION_FETCH_APPEALS_LIST,
    ACTION_FETCH_APPLICANTS_LIST, ACTION_FETCH_EMPLOYEES_LIST,
    MUTATION_SET_APPEAL_STATUSES_LIST,
    MUTATION_SET_APPLICANTS_LIST, MUTATION_SET_EMPLOYEES_LIST,
} from '@/store/modules/appeals/appealsStoreConstants';
import { AxiosResponse } from 'axios';
import AppealsModel from '@/interfaces/models/AppealsModel';
import getAppealsList from '@/services/api/requests/appeals/getAppealsList';
import RefAppealStatusesModel from '@/interfaces/models/RefAppealStatusesModel';
import getRefAppealStatusesList from '@/services/api/requests/refAppealStatuses/getRefAppealStatusesList';
import ApplicantsModel from '@/interfaces/models/ApplicantsModel';
import getApplicantsList from '@/services/api/requests/applicants/getApplicantsList';
import EmployeesModel from '@/interfaces/models/EmployeesModel';
import getEmployeesList from '@/services/api/requests/employees/getEmployeesList';
import getRequestsList from '@/services/api/requests/requests/getRequestsList';
import getContractsRecord from '@/services/api/requests/contracts/getContractsRecord';
import getContractAppealsList from '@/services/api/requests/contractAppeals/getContractAppealsList';
import getContractSettlementsList from '@/services/api/requests/contractSettlements/getContractSettlementsList';

export default {
    [ACTION_FETCH_APPEALS_LIST]:
        async (context: ActionContext<IAppealsState, IStore>, payload): Promise<AppealsModel[]> => {
            const {data: {data: appealsList}}:
                AxiosResponse<{data: AppealsModel[]}>
                = await getAppealsList(payload);

            // @TODO: temporary
            const extendedList: any = [];

            const buildAppear = async (appeal: AppealsModel) => {
                const _requestsLength = appeal.requests.length;

                const orderedRequests = appeal.requests.sort(
                    ({requestNumber: a}, {requestNumber: b}) => a > b ? 1 : -1,
                );

                const _requestsIds = appeal.requests.length
                    ? [
                        orderedRequests[0].requestNumber,
                        orderedRequests[orderedRequests.length - 1].requestNumber,
                    ].join(' - ')
                    : '';

                const _contractId = appeal.contracts.length
                    ? appeal.contracts[0].contractNumber
                    : '';

                const _contractDate = appeal.contracts.length
                    ? appeal.contracts[0].contractDate
                    : '';

                const _contractValue = appeal.contracts.length && appeal.contracts[0].contractSettlements.length
                    ? appeal.contracts[0].contractSettlements[0].fullAmount
                    : '';

                extendedList.push({
                    ...appeal,
                    _requestsLength,
                    _requestsIds,
                    _contractId,
                    _contractDate,
                    _contractValue,
                });
            };

            await appealsList.reduce(async (previousPromise, appeal) => {
                await previousPromise;
                return buildAppear(appeal);
            }, Promise.resolve());

            return extendedList;
        },
    [ACTION_FETCH_APPEAL_STATUSES_LIST]:
        async ({commit}: ActionContext<IAppealsState, IStore>) => {
            const {data: {data: appealStatusesList}}:
                AxiosResponse<{data: RefAppealStatusesModel[]}>
                = await getRefAppealStatusesList();

            commit(
                MUTATION_SET_APPEAL_STATUSES_LIST,
                appealStatusesList.map(({links, ...appealStatus}) => appealStatus,
            ));
        },
    [ACTION_FETCH_APPLICANTS_LIST]:
        async ({commit}: ActionContext<IAppealsState, IStore>) => {
            const {data: {data: applicantsList}}:
                AxiosResponse<{data: ApplicantsModel[]}>
                = await getApplicantsList();

            commit(
                MUTATION_SET_APPLICANTS_LIST,
                applicantsList.map(({links, ...applicant}) => applicant,
            ));
        },
    [ACTION_FETCH_EMPLOYEES_LIST]:
        async ({commit}: ActionContext<IAppealsState, IStore>) => {
            const {data: {data: employeesList}}:
                AxiosResponse<{data: EmployeesModel[]}>
                = await getEmployeesList();

            commit(
                MUTATION_SET_EMPLOYEES_LIST,
                employeesList.map(({links, ...employee}) => employee,
            ));
        },
} as ActionTree<IAppealsState, IStore>;
