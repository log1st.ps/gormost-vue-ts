import {GetterTree} from 'vuex';
import {IAppealsState} from '@/store/modules/appeals/IAppealsState';
import {IStore} from '@/store/IStore';
import {
    GETTER_APPEAL_STATUSES_LIST,
    GETTER_APPLICANTS_LIST,
    GETTER_EMPLOYEES_LIST,
} from '@/store/modules/appeals/appealsStoreConstants';
import ApplicantsModel from '@/interfaces/models/ApplicantsModel';
import RefAppealStatusesModel from '@/interfaces/models/RefAppealStatusesModel';
import EmployeesModel from '@/interfaces/models/EmployeesModel';

export default {
    [GETTER_APPEAL_STATUSES_LIST]: (state): RefAppealStatusesModel[] => {
        return state.refStatuses;
    },
    [GETTER_APPLICANTS_LIST]: (state): ApplicantsModel[] => {
        return state.refApplicants;
    },
    [GETTER_EMPLOYEES_LIST]: (state): EmployeesModel[] => {
        return state.refEmployees;
    },
} as GetterTree<IAppealsState, IStore>;
