export const MODULE_APPEALS = 'appeals';

export const ACTION_FETCH_APPEALS_LIST = 'fetchList';

export const ACTION_FETCH_APPEAL_STATUSES_LIST = 'fetchStatusesList';
export const MUTATION_SET_APPEAL_STATUSES_LIST = 'setStatusesList';
export const GETTER_APPEAL_STATUSES_LIST = 'getStatusesList';

export const ACTION_FETCH_APPLICANTS_LIST = 'fetchApplicantsList';
export const MUTATION_SET_APPLICANTS_LIST = 'setApplicantsList';
export const GETTER_APPLICANTS_LIST = 'getApplicantsList';

export const ACTION_FETCH_EMPLOYEES_LIST = 'fetchEmployeesList';
export const MUTATION_SET_EMPLOYEES_LIST = 'setEmployeesList';
export const GETTER_EMPLOYEES_LIST = 'getEmployeesList';
