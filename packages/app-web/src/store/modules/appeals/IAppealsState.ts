import RefAppealStatusesModel from '@/interfaces/models/RefAppealStatusesModel';
import ApplicantsModel from '@/interfaces/models/ApplicantsModel';
import EmployeesModel from '@/interfaces/models/EmployeesModel';

export interface IAppealsState {
    refStatuses: RefAppealStatusesModel[];
    refApplicants: ApplicantsModel[];
    refEmployees: EmployeesModel[];
}
