import {MUTATION_SET_ROUTES_LIST} from '@/store/modules/routes/routesStoreConstants';
import {MutationTree} from 'vuex';
import {IRoutesState} from '@/store/modules/routes/IRoutesState';

export default {
    [MUTATION_SET_ROUTES_LIST]: (state, routesList) => {
        state.list = routesList;
    },
} as MutationTree<IRoutesState>;
