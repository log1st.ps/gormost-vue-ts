import {IRoutesState} from '@/store/modules/routes/IRoutesState';
import {IStore} from '@/store/IStore';
import {Module} from 'vuex';
import routesGetters from '@/store/modules/routes/routesGetters';
import routesActions from '@/store/modules/routes/routesActions';
import routesState from '@/store/modules/routes/routesState';
import routesMutations from '@/store/modules/routes/routesMutations';

export default {
    namespaced: true,
    state: routesState,
    mutations: routesMutations,
    actions: routesActions,
    getters: routesGetters,
} as Module<IRoutesState, IStore>;
