export const MODULE_ROUTES = 'routes';

export const ACTION_FETCH_ROUTES_LIST = 'fetchList';

export const MUTATION_SET_ROUTES_LIST = 'setList';

export const GETTER_ROUTES_LIST = 'getList';
