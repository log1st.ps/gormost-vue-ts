import {GetterTree} from 'vuex';
import {IRoutesState} from '@/store/modules/routes/IRoutesState';
import {IStore} from '@/store/IStore';
import {GETTER_ROUTES_LIST} from '@/store/modules/routes/routesStoreConstants';
import RoutesModel from '@/interfaces/models/RoutesModel';

export default {
    [GETTER_ROUTES_LIST]: (state): RoutesModel[] => state.list,
} as GetterTree<IRoutesState, IStore>;
