import {ActionTree} from 'vuex';
import {IRoutesState} from '@/store/modules/routes/IRoutesState';
import {IStore} from '@/store/IStore';
import {ACTION_FETCH_ROUTES_LIST, MUTATION_SET_ROUTES_LIST} from '@/store/modules/routes/routesStoreConstants';
import { AxiosResponse } from 'axios';
import getRoutesList from '@/services/api/requests/routes/getRoutesList';

export default {
    [ACTION_FETCH_ROUTES_LIST]:
        async ({commit}): Promise<IRoutesState['list']> => {
            const {data: {data: routesList}}:
                AxiosResponse<{data: IRoutesState['list']}>
                = await getRoutesList();

            commit(MUTATION_SET_ROUTES_LIST, routesList);

            return routesList;
        },
} as ActionTree<IRoutesState, IStore>;
