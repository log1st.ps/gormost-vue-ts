import RoutesModel from '@/interfaces/models/RoutesModel';

export interface IRoutesState {
    list: RoutesModel[];
}
