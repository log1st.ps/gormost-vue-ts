import {IUserState} from '@/store/modules/user/IUserState';
import {IRoutesState} from '@/store/modules/routes/IRoutesState';
import {IRequestsState} from '@/store/modules/requests/IRequestsState';
import {IAppealsState} from '@/store/modules/appeals/IAppealsState';

export interface IStore {
    user: IUserState;
    routes: IRoutesState;
    appeals: IAppealsState;
    requests: IRequestsState;
}
